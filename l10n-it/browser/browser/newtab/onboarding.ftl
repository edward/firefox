# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

onboarding-button-label-learn-more = Ulteriori informazioni
onboarding-button-label-try-now = Prova subito
onboarding-button-label-get-started = Inizia
onboarding-welcome-header = Benvenuto in { -brand-short-name }
onboarding-welcome-body = Adesso hai il browser.<br/>Esplora tutti gli altri prodotti della famiglia { -brand-product-name }.
onboarding-welcome-learn-more = Scopri tutti i vantaggi.
onboarding-join-form-header = Entra a far parte di { -brand-product-name }
onboarding-join-form-body = Inserisci il tuo indirizzo email per iniziare
onboarding-join-form-email =
    .placeholder = Inserisci email
onboarding-join-form-email-error = Inserisci un indirizzo email valido
onboarding-join-form-legal = Proseguendo, accetto le <a data-l10n-name="terms">condizioni di utilizzo del servizio</a> e l’<a data-l10n-name="privacy">informativa sulla privacy</a>.
onboarding-join-form-continue = Continua
onboarding-start-browsing-button-label = Inizia a navigare
onboarding-benefit-products-title = Prodotti al tuo servizio
onboarding-benefit-products-text = Una famiglia completa di strumenti che rispetta la tua privacy su tutti i tuoi dispositivi.
onboarding-benefit-knowledge-title = Conoscenze pratiche
onboarding-benefit-knowledge-text = Scopri tutto ciò che ti serve per essere online in modo più intelligente e sicuro.
onboarding-benefit-privacy-title = Privacy, davvero
onboarding-benefit-privacy-text = Ogni nostra azione avviene nel rispetto della nostra “Garanzia sui dati personali”: raccogli meno dati, mantienili al sicuro, nessun segreto.
onboarding-private-browsing-title = Navigazione anonima
onboarding-private-browsing-text = Naviga al riparo da occhi indiscreti. La Navigazione anonima con blocco contenuti mette uno stop a tutti gli elementi traccianti che cercano di pedinarti sul Web.
onboarding-screenshots-title = Screenshot
onboarding-screenshots-text = Cattura, salva e condividi schermate senza uscire da { -brand-short-name }. Cattura una sezione della pagina, o la pagina intera, mentre navighi. Poi salvala sul web per poterla visualizzare e condividere ancora più facilmente.
onboarding-addons-title = Componenti aggiuntivi
onboarding-addons-text = Aggiungi nuove caratteristiche in grado di rendere { -brand-short-name } ancora più adatto alle tue esigenze. Confronta prezzi, controlla il meteo o esprimi la tua personalità con un tema su misura.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Naviga in modo più veloce, intelligente e sicuro grazie a estensioni come Ghostery, in grado di bloccare tutte quelle fastidiose pubblicità.
onboarding-fxa-title = Sincronizzazione
onboarding-fxa-text = Crea un { -fxaccount-brand-name } e sincronizza segnalibri, password e schede aperte su tutti i dispositivi in cui utilizzi { -brand-short-name }.

onboarding-tracking-protection-title = Limita il tracciamento
onboarding-tracking-protection-text = Non è fastidioso quando le pubblicità ti seguono ovunque? { -brand-short-name } ti aiuta a controllare il modo in cui gli operatori pubblicitari tracciano la tua attività online.
onboarding-tracking-protection-button = { PLATFORM() ->
  [windows] Aggiorna opzioni
 *[other] Aggiorna preferenze
}

onboarding-data-sync-title = Porta le tue personalizzazioni sempre con te
onboarding-data-sync-text = Sincronizza i tuoi segnalibri e le tue password in tutti i dispositivi in cui usi { -brand-product-name }.
onboarding-data-sync-button = Attiva { -sync-brand-short-name }

onboarding-firefox-monitor-title = Tieni sotto controllo le violazioni di dati
onboarding-firefox-monitor-text = { -monitor-brand-name } verifica se la tua email è stata coinvolta in una violazione di dati e ti segnala se appare in una nuova violazione.
onboarding-firefox-monitor-button = Iscriviti agli avvisi

onboarding-browse-privately-title = Naviga in modo riservato
onboarding-browse-privately-text = La navigazione anonima non conserva la cronologia di ricerca e navigazione, mantenendo le tue abitudini di navigazione al sicuro da chiunque altro utilizzi il tuo computer.
onboarding-browse-privately-button = Apri una finestra anonima

onboarding-firefox-send-title = Mantieni al sicuro i file che condividi
onboarding-firefox-send-text = { -send-brand-name } protegge i file condivisi con crittografia end-to-end e un link che scade automaticamente.
onboarding-firefox-send-button = Prova { -send-brand-name }

onboarding-mobile-phone-title = Installa { -brand-product-name } sul tuo telefono
onboarding-mobile-phone-text = Scarica { -brand-product-name } per iOS e Android e sincronizza i tuoi dati con tutti i tuoi dispositivi.
onboarding-mobile-phone-button = Scarica il browser mobile

onboarding-send-tabs-title = Inviati una scheda
onboarding-send-tabs-text = “Invia scheda” permette di condividere immediatamente pagine con altri dispositivi senza bisogno di usare copia e incolla, o lasciare il browser.
onboarding-send-tabs-button = Inizia a utilizzare “Invia scheda”

onboarding-pocket-anywhere-title = Leggi e ascolta ovunque ti trovi
onboarding-pocket-anywhere-text = { -pocket-brand-name } salva le tue storie preferite, offrendoti la possibilità di leggerle, ascoltarle o guardarle nel tuo tempo libero, anche quando non sei connesso a Internet.
onboarding-pocket-anywhere-button = Prova { -pocket-brand-name }

onboarding-lockwise-passwords-title = Porta le tue password sempre con te
onboarding-lockwise-passwords-text = { -lockwise-brand-name } salva le tue password in modo sicuro, permettendoti di accedere in modo semplice e veloce ai tuoi account.
onboarding-lockwise-passwords-button = Ottieni { -lockwise-brand-name }

onboarding-facebook-container-title = Metti un limite a Facebook
onboarding-facebook-container-text = { -facebook-container-brand-name } mantiene la tua identità Facebook separata da tutto il resto. In questo modo, tracciarti attraverso il Web diventa più complicato.
onboarding-facebook-container-button = Aggiungi l’estensione

return-to-amo-sub-header = Ottimo, ora hai installato { -brand-short-name }
return-to-amo-addon-header = Perché adesso non provi <icon></icon><b>{ $addon-name }</b>?
return-to-amo-extension-button = Aggiungi l’estensione
return-to-amo-get-started-button = Inizia a usare { -brand-short-name }

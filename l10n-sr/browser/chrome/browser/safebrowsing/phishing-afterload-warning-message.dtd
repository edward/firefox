<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "Иди назад">
<!ENTITY safeb.palm.seedetails.label "Погледајте детаље">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "Ово није обманљив сајт…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "н">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "Саветодавно мишљење дато од <a id='advisory_provider'/>.">


<!ENTITY safeb.blocked.malwarePage.title2 "Посећивање овог веб сајта може нашкодити вашем рачунару">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "&brandShortName; је блокирао ову страницу зато што може покушати да инсталира злонамерни софтвер који може украсти или обрисати ваше личне информације на вашем рачунару.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "<span id='malware_sitename'/> је <a id='error_desc_link'>пријављен да садржи злонамерни софтвер</a>. Можете <a id='report_detection'>пријавити проблем</a> или <a id='ignore_warning_link'>игнорисати упозорење</a> и наставити настави на сајт.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "<span id='malware_sitename'/> је <a id='error_desc_link'>пријављен да садржи злонамерни софтвер</a>. Можете <a id='report_detection'>пријавити проблем</a>.">

<!ENTITY safeb.blocked.malwarePage.learnMore "Сазнајте више о штетном веб садржају укључујући и вирусе и друге злонамерне софтвере и како да заштитите ваш рачунар на <a id='learn_more_link'>StopBadware.org</a>. Сазнајте више о &brandShortName;-овој заштити од “пецања” и злонамерног софтвера на <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.unwantedPage.title2 "Овај сајт можда садржи штетне програме">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "&brandShortName; је блокирао ову страницу јер би можда покушала да вас наведе да инсталирате програме који би нашкодили вашем прегледачу (на пример, променио би вашу почетну страницу или би приказивао рекламе).">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "<span id='unwanted_sitename'/> је <a id='error_desc_link'>пријављен да садржи штетан софтвер</a>. Можете <a id='ignore_warning_link'>игнорисати ризик</a> и наставити на овај штетан сајт.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "<span id='unwanted_sitename'/> је <a id='error_desc_link'>пријављен да садржи штетан софтвер</a>.">

<!ENTITY safeb.blocked.unwantedPage.learnMore "Сазнајте више о штетном и нежељеном софтверу у <a id='learn_more_link'>полиси о нежељеном софтверу</a>. Сазнајте више о &brandShortName;-овој заштити од “пецања” и злонамерног софтвера на <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.phishingPage.title3 "Обманљив сајт испред">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "&brandShortName; је блокирао ову страницу јер би можда покушала да вас наведе на нешто опасно као што је инсталирање злонамерног софтвера или откривање личних информација као што су лозинке или кредитне картице.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "<span id='phishing_sitename'/> је <a id='error_desc_link'>пријављен као обманљив сајт</a>. Можете <a id='report_detection'>пријавити проблем</a> или <a id='ignore_warning_link'>га игнорисати</a> и отићи на овај небезбедан сајт.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "<span id='phishing_sitename'/> је <a id='error_desc_link'>пријављен као обманљив сајт</a>. Можете <a id='report_detection'>пријавити проблем</a>.">

<!ENTITY safeb.blocked.phishingPage.learnMore "Сазнајте више о обманљивим сајтовима и “пецању” на <a id='learn_more_link'>www.antiphishing.org</a>. Сазнајте више о &brandShortName;-овој заштити од “пецања” и злонамерног софтвера на <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.harmfulPage.title "Овај сајт можда садржи малициозни софтвер">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "&brandShortName; је блокирао ову страницу зато што може покушати да инсталира опасне програме који би украли или обрисали ваше податке (на пример, слике, лозинке, поруке и кредитне картице).">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "<span id='harmful_sitename'/> је <a id='error_desc_link'>пријављен да садржи штетне апликације</a>. Можете <a id='ignore_warning_link'>игнорисати упозорење</a> и наставити на овај небезбедан сајт.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "<span id='harmful_sitename'/> је <a id='error_desc_link'>пријављен да садржи штетне апликације</a>.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "Сазнајте више о &brandShortName;-овој заштити од “пецања” и злонамерног софтвера на <a id='firefox_support'>support.mozilla.org</a>.">

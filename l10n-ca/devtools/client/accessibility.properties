# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE These strings are used inside the Accessibility panel
# which is available from the Web Developer sub-menu -> 'Accessibility'.
# The correct localization of this file might be to keep it in
# English, or another language commonly spoken among web developers.
# You want to make that choice consistent across the developer tools.
# A good criteria is the language in which you'd find the best
# documentation on web development on the web.

# LOCALIZATION NOTE (accessibility.role): A title text used for Accessibility
# tree header column that represents accessible element role.
accessibility.role=Rol

# LOCALIZATION NOTE (accessibility.name): A title text used for Accessibility
# tree header column that represents accessible element name.
accessibility.name=Nom

# LOCALIZATION NOTE (accessibility.logo): A title text used for Accessibility
# logo used on the accessibility panel landing page.
accessibility.logo=Logotip d'accessibilitat

# LOCALIZATION NOTE (accessibility.properties): A title text used for header
# for Accessibility details sidebar.
accessibility.properties=Propietats

# LOCALIZATION NOTE (accessibility.treeName): A title text used for
# Accessibility tree (that represents accessible element name) container.
accessibility.treeName=Arbre d'accessibilitat

# LOCALIZATION NOTE (accessibility.accessible.notAvailable): A title text
# displayed when accessible sidebar panel does not have an accessible object to
# display.
accessibility.accessible.notAvailable=No hi ha informació d'accessibilitat

# LOCALIZATION NOTE (accessibility.enable): A title text for Enable
# accessibility button used to enable accessibility service.
accessibility.enable=Activa les funcions d'accessibilitat

# LOCALIZATION NOTE (accessibility.enabling): A title text for Enable
# accessibility button used when accessibility service is being enabled.
accessibility.enabling=S'estan activant les funcions d'accessibilitat…

# LOCALIZATION NOTE (accessibility.disable): A title text for Disable
# accessibility button used to disable accessibility service.
accessibility.disable=Desactiva les funcions d'accessibilitat

# LOCALIZATION NOTE (accessibility.disabling): A title text for Disable
# accessibility button used when accessibility service is being
# disabled.
accessibility.disabling=S'estan desactivant les funcions d'accessibilitat…

# LOCALIZATION NOTE (accessibility.pick): A title text for Picker button
# button used to pick accessible objects from the page.
accessibility.pick=Trieu un objecte accessible de la pàgina

# LOCALIZATION NOTE (accessibility.disable.disabledTitle): A title text used for
# a tooltip for Disable accessibility button when accessibility service can not
# be disabled. It is the case when a user is using a 3rd party accessibility
# tool such as screen reader.
accessibility.disable.disabledTitle=El servei d'accessibilitat no es pot desactivar. S'està utilitzant fora de les Eines per a desenvolupadors.

# LOCALIZATION NOTE (accessibility.disable.enabledTitle): A title text used for
# a tooltip for Disable accessibility button when accessibility service can be
# disabled.
accessibility.disable.enabledTitle=El servei d'accessibilitat es desactivarà per a totes les pestanyes i finestres.

# LOCALIZATION NOTE (accessibility.enable.disabledTitle): A title text used for
# a tooltip for Enabled accessibility button when accessibility service can not
# be enabled.
accessibility.enable.disabledTitle=El servei d'accessibilitat no es pot activar. S'ha desactivat mitjançant les preferències de privadesa dels serveis d'accessibilitat.

# LOCALIZATION NOTE (accessibility.enable.enabledTitle): A title text used for
# a tooltip for Enabled accessibility button when accessibility service can be
# enabled.
accessibility.enable.enabledTitle=El servei d'accessibilitat s'activarà per a totes les pestanyes i finestres.

# LOCALIZATION NOTE (accessibility.learnMore): A text that is used as is or as textual
# description in places that link to accessibility inspector documentation.
accessibility.learnMore=Més informació

# LOCALIZATION NOTE (accessibility.description.general): A title text used when
# accessibility service description is provided before accessibility inspector
# is enabled.
accessibility.description.general=Les funcions d'accessibilitat estan desactivades per defecte perquè afecten negativament el rendiment. És recomanable desactivar les funcions d'accessibilitat abans d'utilitzar altres subfinestres de les Eines per a desenvolupadors.

# LOCALIZATION NOTE (accessibility.description.general.p1): A title text for the first
# paragraph, used when accessibility service description is provided before accessibility
# inspector is enabled. %S in the content will be replaced by a link at run time
# with the accessibility.learnMore string.
accessibility.description.general.p1=L'inspector d'accessibilitat permet examinar l'arbre d'accessibilitat de la pàgina actual, que utilitzen els lectors de pantalla i altres tecnologies d'assistència. %S

# LOCALIZATION NOTE (accessibility.description.general.p2): A title text for the second
# paragraph, used when accessibility service description is provided before accessibility
# inspector is enabled.
accessibility.description.general.p2=Les funcions d'accessibilitat poden afectar el rendiment d'altres subfinestres de les eines per a desenvolupadors i s'haurien de desactivar quan no s'utilitzen.

# LOCALIZATION NOTE (accessibility.description.oldVersion): A title text used
# when accessibility service description is provided when a client is connected
# to an older version of accessibility actor.
accessibility.description.oldVersion=Esteu connectat a un servidor de depuració massa antic. Per usar la subfinestra d'accessibilitat, connecteu-vos a l'última versió del servidor de depuració.

# LOCALIZATION NOTE (accessibility.tree.menu.printToJSON): A title text used when a
# context menu item for printing an accessible tree to JSON is rendered after triggering a
# context menu for an accessible tree row.
accessibility.tree.menu.printToJSON=Imprimeix en JSON

<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "Vés enrere">
<!ENTITY safeb.palm.seedetails.label "Mostra detalls">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "No és cap lloc maliciós…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "m">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "Assessorament proporcionat per <a id='advisory_provider'/>.">


<!ENTITY safeb.blocked.malwarePage.title2 "Si visiteu aquest lloc web podríeu malmetre l'ordinador">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "El &brandShortName; ha blocat aquesta pàgina perquè podria intentar instal·lar programari maliciós que pot robar la vostra informació personal de l'ordinador o suprimir-la.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "S'ha informat que <span id='malware_sitename'/> <a id='error_desc_link'>conté programari maliciós</a>. Podeu <a id='report_detection'>informar d'un error en la detecció</a> o <a id='ignore_warning_link'>ignorar el risc</a> i visitar aquest lloc insegur.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "S'ha informat que <span id='malware_sitename'/> <a id='error_desc_link'>conté programari maliciós</a>. Podeu <a id='report_detection'>informar d'un error en la detecció</a>.">

<!ENTITY safeb.blocked.malwarePage.learnMore "Per obtenir més informació sobre el contingut web perillós, inclosos els virus i altre programari maliciós, i sobre com protegir el vostre ordinador, visiteu <a id='learn_more_link'>StopBadware.org</a>. Per obtenir més informació sobre la protecció contra la pesca electrònica i el programari maliciós del &brandShortName;, visiteu <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.unwantedPage.title2 "El lloc web que voleu visitar pot contenir programes maliciosos">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "El &brandShortName; ha blocat aquesta pàgina perquè podria intentar enganyar-vos perquè instal·leu programes que poden perjudicar la vostra experiència de navegació (per exemple, us poden canviar la pàgina d'inici o mostrar anuncis addicionals als llocs que visiteu).">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "S'ha informat que <span id='unwanted_sitename'/> <a id='error_desc_link'>conté programari perillós</a>. Podeu <a id='ignore_warning_link'>ignorar el risc</a> i visitar aquest lloc insegur.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "S'ha informat que <span id='unwanted_sitename'/> <a id='error_desc_link'>conté programari perillós</a>.">

<!ENTITY safeb.blocked.unwantedPage.learnMore "Per obtenir més informació sobre el programari perillós i indesitjable, vegeu la <a id='learn_more_link'>política de programari indesitjable</a>. Per obtenir més informació sobre la protecció contra la pesca electrònica i el programari maliciós del &brandShortName;, visiteu <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.phishingPage.title3 "Aquest lloc web és maliciós">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "El &brandShortName; ha blocat aquesta pàgina perquè podria intentar enganyar-vos perquè feu alguna acció perillosa, com instal·lar programari o revelar informació personal (per exemple, contrasenyes o targetes de crèdit).">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "S'ha informat que <span id='phishing_sitename'/> <a id='error_desc_link'>és un lloc maliciós</a>. Podeu <a id='report_detection'>informar d'un error en la detecció</a> o <a id='ignore_warning_link'>ignorar el risc</a> i visitar aquest lloc insegur.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "S'ha informat que <span id='phishing_sitename'/> <a id='error_desc_link'>és un lloc maliciós</a>. Podeu <a id='report_detection'>informar d'un error en la detecció</a>.">

<!ENTITY safeb.blocked.phishingPage.learnMore "Per obtenir més informació sobre els llocs maliciosos i la pesca electrònica (phishing), visiteu <a id='learn_more_link'>www.antiphishing.org</a>. Per obtenir més informació sobre la protecció contra la pesca electrònica i el programari maliciós del &brandShortName;, visiteu <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.harmfulPage.title "El lloc web que voleu visitar pot contenir programari maliciós">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "El &brandShortName; ha blocat aquesta pàgina perquè podria intentar instal·lar aplicacions perilloses que roben la vostra informació o l'eliminen (per exemple, fotos, contrasenyes, missatges i targetes de crèdit).">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "S'ha informat que <span id='harmful_sitename'/> <a id='error_desc_link'>conté una aplicació potencialment perillosa</a>. Podeu <a id='ignore_warning_link'>ignorar el risc</a> i visitar aquest lloc insegur.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "S'ha informat que <span id='harmful_sitename'/> <a id='error_desc_link'>conté una aplicació potencialment perillosa</a>.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "Per obtenir més informació sobre la protecció contra la pesca electrònica i el programari maliciós del &brandShortName;, visiteu <a id='firefox_support'>support.mozilla.org</a>.">

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = अभी डाउनलोड करें
onboarding-welcome-header = { -brand-short-name } में स्वागत है
onboarding-start-browsing-button-label = ब्राउज़िंग शुरू करें

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = निजी ब्राउजिंग
onboarding-screenshots-title = स्क्रीनशॉट
onboarding-addons-title = ऐड-ऑन
# Note: "Sync" in this case is a generic verb, as in "to synchronize"
onboarding-fxa-title = संकलित करें

## Message strings belonging to the Return to AMO flow


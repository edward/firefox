<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY itemFormatting.label             "עיצוב">
<!ENTITY itemTags.label                   "תוויות">
<!ENTITY itemAdvanced.label               "מתקדם">

<!ENTITY style.label                      "סגנון:">
<!ENTITY style.accesskey                  "ס">
<!ENTITY regularStyle.label               "רגיל">
<!ENTITY bold.label                       "מודגש">
<!ENTITY italic.label                     "נטוי">
<!ENTITY boldItalic.label                 "נטוי מודגש">
<!ENTITY size.label                       "גודל:">
<!ENTITY size.accesskey                   "ד">
<!ENTITY regularSize.label                "רגיל">
<!ENTITY bigger.label                     "גדול יותר">
<!ENTITY smaller.label                    "קטן יותר">
<!ENTITY quotedTextColor.label            "צבע:">
<!ENTITY quotedTextColor.accesskey        "ב">
<!ENTITY displayWidth.label               "הודעות טקסט פשוט">
<!ENTITY displayText.label                "בעת הצגת הודעות טקסט פשוט מצוטט:">

<!-- LOCALIZATION NOTE : (emoticonsAndStructs.label) 'Emoticons' are also known as 'Smileys', e.g. :-)   -->
<!ENTITY convertEmoticons.label        "הצג פרצופונים בצורה גרפית">
<!ENTITY convertEmoticons.accesskey    "ה">

<!-- labels -->
<!ENTITY displayTagsText.label     "תוויות משמשות לחלוקה לקטגוריות ותיעדוף ההודעות שלך.">
<!ENTITY newTagButton.label        "חדש…">
<!ENTITY newTagButton.accesskey    "ח">
<!ENTITY editTagButton1.label      "עריכה…">
<!ENTITY editTagButton1.accesskey  "ע">
<!ENTITY removeTagButton.label     "מחק">
<!ENTITY removeTagButton.accesskey "מ">

<!-- Fonts and Colors -->
<!ENTITY fontsAndColors1.label   "גופנים וצבעים">
<!ENTITY defaultFont.label       "גופן ברירת מחדל:">
<!ENTITY defaultFont.accesskey   "ב">
<!ENTITY defaultSize.label       "גודל:">
<!ENTITY defaultSize.accesskey   "ג">
<!ENTITY fontOptions.accesskey   "מ">
<!ENTITY fontOptions.label       "מתקדם…">
<!ENTITY colorButton.label       "צבעים…">
<!ENTITY colorButton.accesskey   "צ">

<!-- Advanced -->
<!ENTITY reading.caption                  "קריאה">
<!ENTITY display.caption                  "תצוגה">
<!ENTITY showCondensedAddresses.label     "הצגת שמות תצוגה בלבד עבור אנשים בפנקס הכתובת שלי">
<!ENTITY showCondensedAddresses.accesskey "צ">

<!ENTITY autoMarkAsRead.label             "סימון הודעות כנקראו אוטומטית">
<!ENTITY autoMarkAsRead.accesskey         "ק">
<!ENTITY markAsReadNoDelay.label          "מיד עם ההצגה">
<!ENTITY markAsReadNoDelay.accesskey      "מ">
<!-- LOCALIZATION NOTE (markAsReadDelay.label): This will concatenate to
     "After displaying for [___] seconds",
     using (markAsReadDelay.label) and a number (secondsLabel.label). -->
<!ENTITY markAsReadDelay.label            "לאחר הצגה למשך">
<!ENTITY markAsReadDelay.accesskey        "א">
<!ENTITY secondsLabel.label               "שניות">
<!ENTITY openMsgIn.label                  "פתיחת הודעות בתוך:">
<!ENTITY openMsgInNewTab.label            "לשונית חדשה">
<!ENTITY openMsgInNewTab.accesskey        "ל">
<!ENTITY reuseExpRadio0.label             "חלון הודעה חדש">
<!ENTITY reuseExpRadio0.accesskey         "ח">
<!ENTITY reuseExpRadio1.label             "חלון הודעה קיים">
<!ENTITY reuseExpRadio1.accesskey         "ק">
<!ENTITY closeMsgOnMoveOrDelete.label     "לסגור את חלון/לשונית ההודעה בעת העברה או מחיקה">
<!ENTITY closeMsgOnMoveOrDelete.accesskey "ס">

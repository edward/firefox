# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = ఇప్పుడే ప్రయత్నించండి
onboarding-button-label-get-started = మొదలుపెట్టండి
onboarding-welcome-header = { -brand-short-name }కు స్వాగతం
onboarding-start-browsing-button-label = విహరించడం మొదలుపెట్టండి

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = అంతరంగిక విహారణ
onboarding-screenshots-title = తెరపట్లు
onboarding-addons-title = పొడగింతలు

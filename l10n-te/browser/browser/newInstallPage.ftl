# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### For this feature, "installation" is used to mean "this discrete download of
### Firefox" and "version" is used to mean "the specific revision number of a
### given Firefox channel". These terms are not synonymous.

title = ముఖ్యమైన వార్తలు
changed-title = ఏమి మారింది?
options-title = నా ఎంపికలు ఏమిటి?
resources = వనరులు:
sync-label = మీ ఈమెయిలును ఇవ్వండి
sync-input =
    .placeholder = ఈమెయిలు
sync-button = కొనసాగించు
sync-learn = ఇంకా తెలుసుకోండి

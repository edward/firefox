<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "Firefox Sync">
<!ENTITY syncBrand.shortName.label "సింక్">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label '&syncBrand.shortName.label; కు అనుసంధానమవ్వు'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'మీ కొత్త పరికరంను క్రియాశీలపరచుటకు, “&syncBrand.shortName.label; అమర్చు” ను పరికరంపై ఎంపికచేయి.'>
<!ENTITY sync.subtitle.pair.label 'క్రియాశీలపరచుటకు, మీ యితర పరికరం పైన  “ఒక పరికరంతో జతకట్టు” ఎంపికచేయి.'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'ఆ పరికరం నా దగ్గర లేదు…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'ప్రవేశాలు'>
<!ENTITY sync.configure.engines.title.history 'చరిత్ర'>
<!ENTITY sync.configure.engines.title.tabs 'ట్యాబులు'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS2;పై &formatS1;'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'ఇష్టాంశాల మోనూ'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'ట్యాగులు'>
<!ENTITY bookmarks.folder.toolbar.label 'ఇష్టాంశాల పనిముట్లపట్టీ'>
<!ENTITY bookmarks.folder.other.label 'ఇతర ఇష్టాంశాలు'>
<!ENTITY bookmarks.folder.desktop.label 'డెస్క్‌టాప్ ఇష్టాంశాలు'>
<!ENTITY bookmarks.folder.mobile.label 'మొబైల్ ఇష్టాంశాలు'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'పిన్‌చేసినవి'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'తిరిగి విహరణకు'>

<!ENTITY fxaccount_getting_started_welcome_to_sync '&syncBrand.shortName.label;కి స్వాగతం'>
<!ENTITY fxaccount_getting_started_description2 'మీ ట్యాబులను, ఇష్టాంశాలను, సంకేతపదాలను, ఇంకా మరెన్నింటినో సింక్ చేసుకోడానికి ప్రవేశించండి.'>
<!ENTITY fxaccount_getting_started_get_started 'మొదలు పెట్టండి'>
<!ENTITY fxaccount_getting_started_old_firefox '&syncBrand.shortName.label; పాత వర్షన్ ఉపయోగిస్తున్నారా?'>

<!ENTITY fxaccount_status_auth_server 'ఖాతా సేవకి'>
<!ENTITY fxaccount_status_sync_now 'ఇప్పుడే సింక్ చేయి'>
<!ENTITY fxaccount_status_syncing2 'సింకవుతోంది…'>
<!ENTITY fxaccount_status_device_name 'పరికరం పేరు'>
<!ENTITY fxaccount_status_sync_server 'సింక్ సేవిక'>
<!ENTITY fxaccount_status_needs_verification2 'మీ ఖాతాను నిర్ధారించాలి. నిర్థారణ ఈమెయిలును మళ్ళీ పంపించడానికి తాకండి.'>
<!ENTITY fxaccount_status_needs_credentials 'అనుసంధానం కాలేదు. ప్రవేశించుటకు తట్టండి.'>
<!ENTITY fxaccount_status_needs_upgrade 'ప్రవేశించుటకు మీరు &brandShortName; కు నవీకరించాలి.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled '&syncBrand.shortName.label; అమర్పు పూర్తయ్యింది; కానీ ఆటోమెటిగ్గా సింక్రనించడంలేదు. Android అమరికలు &gt; డేటా వాడుక వద్ద “ఆటో-సింక్ డేటా”ను ఎంచుకోండి.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 '&syncBrand.shortName.label; అమర్పు పూర్తయ్యింది; కానీ ఆటోమెటిగ్గా సింక్రనించడంలేదు. Android అమరికలు &gt; ఖాతాలు వద్ద “ఆటో-సింక్ డేటా” ఎంచుకోండి.'>
<!ENTITY fxaccount_status_needs_finish_migrating 'మీ కొత్త Firefox ఖాతా లోనికి ప్రవేశించడానికి తాకండి.'>
<!ENTITY fxaccount_status_choose_what 'ఏమి సమకాలీకరించాలో ఎంచుకోండి'>
<!ENTITY fxaccount_status_bookmarks 'ఇష్టాంశాలు'>
<!ENTITY fxaccount_status_history 'చరిత్ర'>
<!ENTITY fxaccount_status_passwords2 'ప్రవేశాలు'>
<!ENTITY fxaccount_status_tabs 'తెరిచివున్న ట్యాబులు'>
<!ENTITY fxaccount_status_additional_settings 'అదనపు అమరికలు'>
<!ENTITY fxaccount_pref_sync_use_metered2 'వై-ఫై లో మాత్రమే సమకాలీకరించండి'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 'ఒక సెల్యులార్ లేదా మీటర్ నెట్వర్క్లో సమకాలీకరించకుండా &brandShortName;ను నిరోధించు'>
<!ENTITY fxaccount_status_legal 'చట్టబద్ధం' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'సేవా నియమాలు'>
<!ENTITY fxaccount_status_linkprivacy2 'అంతరంగికతా విధానం'>
<!ENTITY fxaccount_remove_account 'డిస్కనెక్ట్&ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 'Sync నుండి అనుసంధానం తెంచివేయాలా?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'మీ విహారణ దత్తాంశం ఈ పరికరంలో ఉంటుంది, కానీ దాన్ని మీరు మీ ఖాతాకు సింక్రనించుకోలేరు.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 'Firefox ఖాతా &formatS; అనుసంధానం తెంచివేయబడింది.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'అనుసంధానం తెంచివేయి'>

<!ENTITY fxaccount_enable_debug_mode 'డీబగ్ రీతి ప్రారంభించు'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title '&syncBrand.shortName.label; ఎంపికలు'>
<!ENTITY fxaccount_options_configure_title '&syncBrand.shortName.label; ఆకృతీకరించు'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 '&syncBrand.shortName.label; అనుసంధానమై లేదు'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 '&formatS; వలె ప్రవేశించడానికి తాకండి'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title '&syncBrand.shortName.label;‌ని నవీకరించడం పూర్తిచేయాలా?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text '&formatS; వలె ప్రవేశించడానికి తాకండి'>

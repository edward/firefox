# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Firefox Brand
##
## Firefox must be treated as a brand, and kept in English.
## It cannot be:
## - Declined to adapt to grammatical case.
## - Transliterated.
## - Translated.
##
## Reference: https://www.mozilla.org/styleguide/communications/translation/

-brand-shorter-name = Firefox
-brand-short-name = Firefox
-brand-full-name = Mozilla Firefox
# This brand name can be used in messages where the product name needs to
# remain unchanged across different versions (Nightly, Beta, etc.).
-brand-product-name = Firefox
-vendor-short-name = Mozilla
trademarkInfo = ഫയര്‍ഫോക്സും ഫയര്‍ഫോക്സ് ലോഗോകളും മോസില്ലാ ഫൌണ്ടേഷന്റെ ട്രേഡ്മാർക്കുകൾ ആണ്.

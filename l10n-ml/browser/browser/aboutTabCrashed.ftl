# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

crashed-title = ടാബ് ക്രാഷ് റിപോർട്ടർ
crashed-close-tab-button = ടാബ് അടയ്കുക
crashed-restore-tab-button = ഈ ടാബ് പുനഃസ്ഥാപിക്കുക
crashed-restore-all-button = എല്ലാ തകർന്ന ടാബുകളും പുനഃസ്ഥാപിക്കുക
crashed-header = നിങ്ങളുടെ ടാബ് ക്രാഷ് ചെയ്തു.
crashed-offer-help = ഞങ്ങൾ സഹായിക്കാം!
crashed-single-offer-help-message = താൾ വീണ്ടെടുക്കുന്നതിന് { crashed-restore-tab-button } തെരഞ്ഞെടുക്കുക.
crashed-multiple-offer-help-message = താളുകൾ വീണ്ടെടുക്കുന്നതിന് { crashed-restore-tab-button } അല്ലെങ്കിൽ { crashed-restore-all-button } തെരഞ്ഞെടുക്കുക.
crashed-request-help = ഞങ്ങളെ സഹായിക്കുമോ?
crashed-request-help-message = പ്രശ്നങ്ങൾ കണ്ടുപിടിച്ച് { -brand-short-name }-നെ മെച്ചപ്പെടുത്തുന്നതിന് ക്രാഷ് റിപോർട്ടുകൾ സഹായിക്കുന്നു.
crashed-request-report-title = ഈ ടാബ് റിപോർട്ട് ചെയ്യുക
crashed-send-report = ഇത്തരത്തിലുള്ള പ്രശ്നങ്ങൾ പരിഹരിക്കുന്നതിന് ഓട്ടോമേറ്റഡ് ക്രാഷ് റിപോർട്ട് അയയ്ക്കുക.
crashed-comment =
    .placeholder = നിർബന്ധമല്ലാത്ത കമന്റുകൾ (ഇവ എല്ലാവർക്കും കാണാം)
crashed-include-URL = { -brand-short-name } ക്രാഷ് ചെയ്തപ്പോൾ നിങ്ങൾ ബ്രൗസ് ചെയ്തുകൊണ്ടിരുന്ന സൈറ്റുകളുടെ യുആർഎൽ ഉൾപ്പെടുത്തുക.
crashed-email-placeholder = നിങ്ങളുടെ ഈമെയില്‍ വിലാസം ഇവിടെ നല്‍കുക
crashed-email-me = കുടുതല്‍ വിവരങ്ങള്‍ ലഭ്യമാകുമ്പോള്‍ എന്നെ ഇമെയില്‍ വഴി അറിയിയ്ക്കുക
crashed-report-sent = തകർച്ചാ റിപ്പോട്ട് മുമ്പേ സമർപ്പിച്ചതാണ്; { -brand-short-name } മികച്ചതാക്കാൻ സഹായിച്ചതിനു നന്ദി!
crashed-request-auto-submit-title = പശ്ചാത്തലത്തിലുള്ള ടാബുകൾ റിപോർട്ട് ചെയ്യുക
crashed-auto-submit-checkbox = { -brand-short-name } ക്രാഷ് ചെയ്യുമ്പോൾ ഓട്ടോമാറ്റിക്കായി റിപോർട്ടുകൾ രേഖപ്പെടുത്തുന്നതിന് മുൻഗണനകൾ സജ്ജമാക്കുക.

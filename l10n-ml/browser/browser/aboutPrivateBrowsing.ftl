# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-private-browsing-learn-more = കൂടുതൽ അറിയുക <a data-l10n-name="learn-more">സ്വകാര്യമായ ബ്രൗസിംഗ്</a>.
about-private-browsing-info-visited = സന്ദര്‍ശിച്ച താളുകള്‍
privatebrowsingpage-open-private-window-label = ഒരു സ്വകാര്യ ജാലകം തുറക്കുക
    .accesskey = ക
about-private-browsing-info-notsaved = നിങ്ങള്‍ സ്വകാര്യ ജാലകത്തില്‍ ബ്രൗസ് ചെയ്യുമ്പോള്‍, { -brand-short-name } ഇവയൊന്നും <strong>സൂക്ഷിക്കുന്നില്ല</strong>:
about-private-browsing-search-placeholder = വെബിൽ തിരയുക
about-private-browsing-info-bookmarks = അടയാളക്കുറിപ്പുകള്‍
about-private-browsing-info-title = നിങ്ങൾ ഒരു സ്വകാര്യ വിൻഡോയിലാണ്
about-private-browsing-info-searches = തെരയലുകള്‍
about-private-browsing-info-downloads = ഡൗണ്‍ലോഡുകള്‍
private-browsing-title = സ്വകാര്യ ബ്രൗസിങ്
about-private-browsing-info-saved = { -brand-short-name } ഇവയൊക്കെ <strong>സൂക്ഷിക്കും</strong>:
about-private-browsing-info-myths = സ്വകാര്യ ബ്രൌസിംഗിനെക്കുറിച്ചുള്ള പൊതുവായ മിഥ്യകൾ
about-private-browsing-info-clipboard = പകര്‍ത്തിയ വാക്യം
about-private-browsing-info-temporary-files = താല്‍കാലിക ഫയലുകള്‍
about-private-browsing-info-cookies = കുക്കികള്‍
tracking-protection-start-tour = എങ്ങനെ ഇത് പ്രവര്‍ത്തിക്കുന്നു എന്ന് കാണുക
about-private-browsing-note = സ്വകാര്യ ബ്രൗസിങ് <strong>നിങ്ങളെ അജ്ഞാതനാക്കുന്നില്ല</strong> ഇന്റര്‍നെറ്റില്‍. നിങ്ങളുടെ സ്ഥാപത്തിനോ അല്ലെങ്കില്‍ ഇന്റര്‍നെറ്റ് സേവന ദാതാവിനോ നിങ്ങള്‍ സന്ദര്‍ശിക്കുന്ന പേജുകള്‍ കാണാം എന്ന് ഓര്‍മ്മിക്കുക.
about-private-browsing =
    .title = ഇൻറർനെറ്റിൽ തിരയുക
about-private-browsing-not-private = നിങ്ങള്‍ നിലവില്‍ ഒരു സ്വകാര്യ ജാലകത്തിലല്ല.
content-blocking-title = ഉള്ളടക്ക തടയൽ

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### For this feature, "installation" is used to mean "this discrete download of
### Firefox" and "version" is used to mean "the specific revision number of a
### given Firefox channel". These terms are not synonymous.

title = പ്രധാന വാർത്തകൾ
heading = നിങ്ങളുടെ { -brand-short-name } പ്രൊഫൈലിലെ മാറ്റങ്ങൾ
changed-title = എന്തൊക്കെയാണ് മാറിയത്?
changed-desc-dedicated = ഫയർഫോക്സ്, ഫയർഫോക്സ് എസ്.ആർ.ആർ, ഫയർഫോക്സ് ബീറ്റ, ഫയർഫോക്സ് ഡവലപ്പർ എഡിഷൻ, ഫയർഫോക്സ് നൈറ്റ്ലി എന്നിവയുൾപ്പെടെ ഏത് പതിപ്പിലേയ്ക്കും സുരക്ഷിതവും എളുപ്പതിലും മാറാന്‍ ഈ ഡെഡിക്കേറ്റഡ് പ്രൊഫൈല്‍ സഹായിക്കുന്നു. നിങ്ങളുടെ സംരക്ഷിച്ച വിവരങ്ങൾ മറ്റ് ഫയർഫോക്സ് ഇൻസ്റ്റാളേഷനുകളുമായി ഇത് തനിയെ പങ്കിടുകയില്ല.
options-title = എന്റെ ഐച്ഛികങ്ങൾ എന്തെല്ലാമാണ്?
resources = വിഭവങ്ങൾ:
support-link = പ്രൊഫൈൽ മാനേജർ - പിന്തുണ ലേഖനം ഉപയോഗിച്ച്
sync-header = പ്രവേശിക്കുക അല്ലെങ്കിൽ ഒരു { -fxaccount-brand-name } സൃഷ്ടിക്കുക
sync-label = നിങ്ങളുടെ ഇമെയിൽ നൽകുക
sync-input =
    .placeholder = ഇമെയിൽ
sync-button = തുടരുക
sync-terms = തുടരുന്നതിലൂടെ നിങ്ങൾ <a data-l10n-name="terms"> സേവന നിബന്ധനകളും </a> <a data-l10n-name="privacy"> സ്വകാര്യത അറിയിപ്പും </a> അംഗീകരിക്കുന്നു.
sync-first = { -sync-brand-name } ആദ്യമായി ഉപയോഗിക്കുകയാണോ? നിങ്ങളുടെ വിവരങ്ങൾ സമന്വയിപ്പിക്കാൻ നിങ്ങളുടെ എല്ലാ ഫയർഫോക്സ് ഇൻസ്റ്റാളേഷനിലും പ്രവേശിക്കണം
sync-learn = കൂടുതൽ അറിയുക

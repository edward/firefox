# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

safe-mode-window =
    .title = { -brand-short-name } സേഫ്‌ മോഡ്‌
    .style = max-width: 400px
start-safe-mode =
    .label = സേഫ് മോഡില്‍ ആരംഭിയ്ക്കുക
refresh-profile =
    .label = { -brand-short-name } വീണ്ടും സജ്ജമാക്കുക
safe-mode-description = പ്രശ്നങ്ങള്‍ എന്താണെന്നു് കണ്ടുപിടിച്ചു് അവ പരിഹരിയ്ക്കുന്നതിനുള്ള { -brand-short-name } ന്റെ പ്രത്യേക മോഡാകുന്നു സേഫ് മോഡ്.
refresh-profile-instead = ട്രബിള്‍ഷൂട്ടിങ് ഉപേക്ഷിച്ചും { -brand-short-name } പുതുക്കാം.
# Shown on the safe mode dialog after multiple startup crashes. 
auto-safe-mode-description = ആരംഭിയ്ക്കുമ്പോള്‍ { -brand-short-name } അപ്രതീക്ഷിതമായി അടച്ചിരിയ്ക്കുന്നു. ഇതിനു് കാരണങ്ങള്‍  ആഡ്-ഓണുകള്‍ അല്ലെങ്കില്‍ മറ്റെന്തെങ്കിലുമാവാം. ഇതു് പരിഹരിയ്ക്കുന്നതിനായി, നിങ്ങള്‍ക്കു് അതിന്റെ സ്വതവേയുള്ള അവസ്ഥയിലേക്കു് വീണ്ടും സജ്ജമാക്കാം അല്ലെങ്കില്‍ സേഫ് മോഡില്‍ പരിഹാരങ്ങള്‍ കണ്ടുപിടിയ്ക്കാം.

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Shown while the disconnect is in progress
sync-disconnect-disconnecting = വിച്ഛേദിക്കുന്നു ...
sync-disconnect-cancel =
    .label = റദ്ദാക്കുക
    .accesskey = C

## Disconnect confirm Button
##
## The 2 labels which may be shown on the single "Disconnect" button, depending
## on the state of the checkboxes.

sync-disconnect-confirm-disconnect-delete =
    .label = വിച്ഛേദിക്കുക & ഇല്ലാതാക്കുക
    .accesskey = D

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = ഇപ്പോൾ ശ്രമിക്കുക
onboarding-button-label-get-started = ആരംഭിക്കുക
onboarding-welcome-header = { -brand-short-name } ലേക്ക് സ്വാഗതം
onboarding-start-browsing-button-label = ബ്രൗസിംഗ് ആരംഭിക്കുക

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = സ്വകാര്യ ബ്രൗസിംഗ്
onboarding-private-browsing-text = ഒറ്റയ്ക്ക് ബ്രൗസ് ചെയ്യുക. സ്വകാര്യ ബ്രൌസിംഗ് വെബിലുടനീളം നിങ്ങളെ പിന്തുടരുന്ന ഓൺലൈൻ ട്രാക്കറുകളെ തടയുന്നു.
onboarding-screenshots-title = സ്ക്രീൻഷോട്ടുകൾ
onboarding-screenshots-text = { -brand-short-name } വിട്ടുപോകാതെ തന്നെ സ്ക്രീൻഷോട്ടുകൾ എടുക്കുക, സംരക്ഷിക്കുക പങ്കിടുക. നിങ്ങൾ ബ്രൗസുചെയ്യുമ്പോൾ ഒരു പ്രദേശമോ മുഴുവൻ പേജുമോ ചിത്രീകരിക്കുക. ശേഷം എളുപ്പത്തിൽ ലഭ്യമാക്കുന്നതിനും പങ്കിടലിനും വെബിൽ സംരക്ഷിക്കുക.
onboarding-addons-title = ആഡ്-ഓണുകൾ
onboarding-addons-text = { -brand-short-name } കൂടുതൽ മികവോടെ നിങ്ങൾക്കായി പ്രവർത്തിക്കാൻ ഒട്ടനേകം സവിശേഷതകൾ ചേർക്കുക. വിലകൾ താരതമ്യം ചെയ്യുക, കാലാവസ്ഥ പരിശോധിക്കുക അല്ലെങ്കിൽ ഇഷ്ടപ്പെട്ട കെട്ടും മട്ടും ഉപയോഗിച്ച് നിങ്ങളുടെ സ്വഭാവം പ്രകടിപ്പിക്കുക.
onboarding-ghostery-title = ഗോസ്റ്ററി
# Note: "Sync" in this case is a generic verb, as in "to synchronize"
onboarding-fxa-title = സമന്വയം
onboarding-fxa-text = { -fxaccount-brand-name } എന്നതിനായി സൈൻ അപ്പ് ചെയ്യുക, ഒപ്പം നിങ്ങളുടെ ബുക്ക്മാർക്കുകൾ, രഹസ്യവാക്കുകള്‍, തുറന്ന ടാബുകൾ എന്നിവ { -brand-short-name } ഉപയോഗിച്ച് നിങ്ങൾ എല്ലായിടത്തും സമന്വയിപ്പിക്കുക.

## Message strings belonging to the Return to AMO flow

return-to-amo-sub-header = അടിപൊളി, നിങ്ങൾക്ക് { -brand-short-name } ലഭിച്ചു
return-to-amo-extension-button = വിപുലീകരണം ചേർക്കുക
return-to-amo-get-started-button = { -brand-short-name } ഉപയോഗിച്ചു് തുടങ്ങാം

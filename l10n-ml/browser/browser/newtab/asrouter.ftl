# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

cfr-doorhanger-extension-heading = ശുപാർശ ചെയ്യുന്ന വിപുലീകരണം
cfr-doorhanger-pintab-heading = ടാബ് പിൻ ചെയ്യുന്നതു പരീക്ഷിക്കുക
cfr-doorhanger-extension-sumo-link =
    .tooltiptext = എന്തുകൊണ്ട് ഞാനിത് കാണുന്നു
cfr-doorhanger-extension-cancel-button = ഇപ്പോൾ വേണ്ട
    .accesskey = N
cfr-doorhanger-extension-ok-button = ഇപ്പോൾ ചേർക്കൂ
    .accesskey = A
cfr-doorhanger-pintab-ok-button = ഈ ടാബ് പിൻ ചെയ്യുക
    .accesskey = P
cfr-doorhanger-extension-manage-settings-button = ശുപാർശ ക്രമീകരണം നിയന്ത്രിക്കുക
    .accesskey = M
cfr-doorhanger-extension-never-show-recommendation = ഈ ശുപാർശ എന്നെ കാണിക്കേണ്ടതില്ല
    .accesskey = S
cfr-doorhanger-extension-learn-more-link = കൂടുതല്‍ അറിയുക
# This string is used on a new line below the add-on name
# Variables:
#   $name (String) - Add-on author name
cfr-doorhanger-extension-author = { $name } നാല്‍
# This is a notification displayed in the address bar.
# When clicked it opens a panel with a message for the user.
cfr-doorhanger-extension-notification = ശുപാർശ

## Add-on statistics
## These strings are used to display the total number of
## users and rating for an add-on. They are shown next to each other.

# Variables:
#   $total (Number) - The rating of the add-on from 1 to 5
cfr-doorhanger-extension-rating =
    .tooltiptext =
        { $total ->
            [one] { $total } നക്ഷത്രം
           *[other] { $total } നക്ഷത്രങ്ങൾ
        }
# Variables:
#   $total (Number) - The total number of users using the add-on
cfr-doorhanger-extension-total-users =
    { $total ->
        [one] { $total } ഉപയോക്താവ്
       *[other] { $total } ഉപയോക്താക്കൾ
    }

## These messages are steps on how to use the feature and are shown together.

cfr-doorhanger-pintab-animation-pause = താൽക്കാലികമായി നിർത്തുക
cfr-doorhanger-pintab-animation-resume = തുടരുക

<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "ഫയര്‍ഫോക്സ് സിന്‍ക്">
<!ENTITY syncBrand.shortName.label "സിന്‍ക്">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label '&syncBrand.shortName.label;ലേക്ക് ബന്ധിപ്പിക്കുക'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'നിങ്ങളുടെ ഉപകരണം പ്രവർത്തിപ്പിക്കാൻ, “&syncBrand.shortName.label; സജ്ജീകരണം” എടുക്കുക.'>
<!ENTITY sync.subtitle.pair.label 'സജീവമാക്കുന്നതിനു്, നിങ്ങളുടെ മറ്റുപകരണത്തില്‍ \&quot;Pair a Device\&quot; തെരഞ്ഞെടുക്കുക.'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'ഡിവൈസ് ലഭ്യമല്ല...'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'പ്രവേശനങ്ങള്‍'>
<!ENTITY sync.configure.engines.title.history 'നാള്‍വഴി'>
<!ENTITY sync.configure.engines.title.tabs 'ടാബുകള്‍'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS2; എന്നതില്‍ &formatS1;'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'അടയാളക്കുറിപ്പുകള്‍ക്കുള്ള പട്ടിക'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'റ്റാഗുകള്‍'>
<!ENTITY bookmarks.folder.toolbar.label 'അടയാളക്കുറിപ്പുകള്‍ക്കുള്ള ഉപകരണപ്പട്ട'>
<!ENTITY bookmarks.folder.other.label 'മറ്റു് അടയാളക്കുറിപ്പുകള്‍'>
<!ENTITY bookmarks.folder.desktop.label 'പണിയിടത്തിലുള്ള അടയാളക്കുറിപ്പുകള്‍'>
<!ENTITY bookmarks.folder.mobile.label 'മൊബൈല്‍ അടയാളക്കുറിപ്പുകള്‍'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'ചേര്‍ത്ത സൈറ്റുകള്‍'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'ബ്രൗസിംഗിലേക്ക് തിരികെ'>

<!ENTITY fxaccount_getting_started_welcome_to_sync '&syncBrand.shortName.label;-ലേക്കു് സ്വാഗതം'>
<!ENTITY fxaccount_getting_started_description2 'നിങ്ങളുടെ ടാബുകള്‍, അടയാളകുറിപ്പുകള്‍, പ്രവേശനങ്ങള്‍ തുടങ്ങിയവ സിന്‍ക് ചെയ്യാനായി പ്രവേശിക്കൂ.'>
<!ENTITY fxaccount_getting_started_get_started 'ഉപയോഗിച്ചു് തൂടങ്ങൂ'>
<!ENTITY fxaccount_getting_started_old_firefox '&syncBrand.shortName.label;-ന്റെ പഴയ പതിപ്പു് ഉപയോഗിയ്ക്കുന്നോ?'>

<!ENTITY fxaccount_status_auth_server 'അക്കൌണ്ട് സര്‍വര്‍'>
<!ENTITY fxaccount_status_sync_now 'ഉടന്‍ സിന്‍ക് ചെയ്യുക'>
<!ENTITY fxaccount_status_syncing2 'സിന്‍ക് ചെയ്യുന്നു...'>
<!ENTITY fxaccount_status_device_name 'ഉപകരണത്തിന്റെ പേരു്'>
<!ENTITY fxaccount_status_sync_server 'സിന്‍ക് സര്‍വര്‍'>
<!ENTITY fxaccount_status_needs_verification2 'അക്കൌണ്ട് ഉറപ്പാക്കേണ്ടതുണ്ടു്. ഇതിനാവശ്യമായ മെയില്‍ അയയ്ക്കുന്നതിനു് സ്ക്രീനില്‍ തൊടുക.'>
<!ENTITY fxaccount_status_needs_credentials 'കണക്ട് ചെയ്യുവാനായില്ല. പ്രവേശിയ്ക്കുന്നതിനു് സ്ക്രീനില്‍ തൊടുക.'>
<!ENTITY fxaccount_status_needs_upgrade 'പ്രവേശിയ്ക്കന്നതിനു് &brandShortName; പരിഷ്കരിയ്ക്കണം.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled '&syncBrand.shortName.label; സജ്ജമാണു്, പക്ഷേ ഓട്ടോമാറ്റിയ്ക്കായി സിന്‍ക് ചെയ്യുന്നില്ല. ആന്‍ഡ്രോയിഡ് സജ്ജീകരണങ്ങള്‍ &gt; ഡേറ്റാ ഉപയോഗത്തിലുള്ള “Auto-sync data”മാറ്റുക.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 '&syncBrand.shortName.label; ശരിയാക്കി, പക്ഷേ തനിയെ സമന്വയിപ്പിക്കുന്നില്ല. ആണ്‍ഡ്രോയിഡിലെ സജ്ജികരണങ്ങള്‍ &gt; അക്കൌണ്ടുകള്‍ മെനുവില്‍ നിന്ന് “തനിയെ സമന്വയിപ്പിക്കുക” മാറ്റുക.'>
<!ENTITY fxaccount_status_needs_finish_migrating 'താങ്കളുടെ പുതിയ ഫയര്‍ഫോക്സ് അക്കൌണ്ടില്‍ പ്രവേശിക്കാന്‍ സ്ക്രീനില്‍ തൊടുക.'>
<!ENTITY fxaccount_status_choose_what 'എന്തൊക്കെ സമന്വയിപ്പിക്കണം എന്ന് തിരഞ്ഞെടുക്കുക'>
<!ENTITY fxaccount_status_bookmarks 'അടയാളക്കുറിപ്പുകള്‍'>
<!ENTITY fxaccount_status_history 'നാള്‍വഴി'>
<!ENTITY fxaccount_status_passwords2 'പ്രവേശനങ്ങള്‍'>
<!ENTITY fxaccount_status_tabs 'ടാബുകള്‍ തുറക്കുക'>
<!ENTITY fxaccount_status_additional_settings 'കൂടുതൽ സജ്ജീകരണങ്ങൾ'>
<!ENTITY fxaccount_pref_sync_use_metered2 'വൈഫൈ മുഖേന മാത്രം സമന്വയിപ്പിക്കുക'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 'ഒരു മൊബൈല്‍ അല്ലെങ്കില്‍ പരിധിയുള്ള നെറ്റ്‍വര്‍ക്ക് വഴി സമന്വയിപ്പിക്കുന്നതില്‍ നിന്ന് &brandShortName; നെ തടയുക'>
<!ENTITY fxaccount_status_legal 'നിയമപരം' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'സേവന നിബന്ധനകള്‍'>
<!ENTITY fxaccount_status_linkprivacy2 'സ്വകാര്യ വ്യവസ്ഥകള്‍'>
<!ENTITY fxaccount_remove_account 'വിച്ഛേദിക്കൂ&ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 'സമന്വയത്തിൽ നിന്നും വിച്ഛേദിക്കണോ?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'നിങ്ങളുടെ ബ്രൗസിംഗ് ഡാറ്റ ഈ ഉപകരണത്തിൽ ശേഷിക്കും, എന്നാൽ ഇത് നിങ്ങളുടെ അക്കൗണ്ടിലേക്ക് മേലിൽ സമന്വയിപ്പിക്കില്ല.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 'ഫയർഫോക്സ് അക്കൗണ്ട് &formatS; വിച്ഛേദിച്ചു.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'വിച്ഛേദിക്കുക'>

<!ENTITY fxaccount_enable_debug_mode 'ഡീബഗ് മോഡ് സജ്ജമാക്കൂ'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'ഫയര്‍ഫോക്സ്'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title '&syncBrand.shortName.label; ഐച്ഛികങ്ങള്‍'>
<!ENTITY fxaccount_options_configure_title '&syncBrand.shortName.label; ക്രമീകരിയ്ക്കുക'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 '&syncBrand.shortName.label; കണക്ട് ചെയ്തിട്ടില്ല'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 '&formatS; ആയി പ്രവേശിയ്ക്കാന്‍ സ്ക്രീനില്‍ തട്ടൂ'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title '&syncBrand.shortName.label; പുതുക്കല്‍ കഴിഞ്ഞോ?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text '&formatS; ആയി പ്രവേശിയ്ക്കാന്‍ സ്ക്രീനില്‍ തട്ടൂ'>

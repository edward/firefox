# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

title = നെറ്റ്‌വര്‍ക്കിങിനെപ്പറ്റി
warning = ഇതു് പരീക്ഷണമാണു്. നിങ്ങള്‍ക്കു് മേല്‍നോട്ടം ആവശ്യമുണ്ടു്.
show-next-time-checkbox = ഈ മുന്നറിയിപ്പു് അടുത്ത തവണ കാണിക്കുക
ok = ശരി
sockets = സോക്കറ്റുകള്‍
dns = ഡിഎന്‍എസ്
websockets = വെബ്സോക്കറ്റുകള്‍
refresh = പുതുക്കുക
auto-refresh = എല്ലാ 3 സെക്കന്‍ഡുകളില്‍ സ്വയമായി പുതുക്കുക
hostname = ഹോസ്റ്റ്നാമം
port = പോര്‍ട്ട്
ssl = എസ്എസ്എല്‍
active = സജീവമാക്കുക
idle = നിര്‍ജ്ജീവം
host = ഹോസ്റ്റ്‌
tcp = ടിസിപി
sent = അയച്ചതു്
received = ലഭിച്ചതു്
family = കുടുംബം
addresses = വിലാസങ്ങള്‍
expires = കാലാവധി കഴിയുന്നതു് (നിമിഷങ്ങള്‍)
messages-sent = അയച്ച സന്ദേശങ്ങള്‍
messages-received = ലഭിച്ച സന്ദേശങ്ങള്‍
bytes-sent = അയച്ച ബൈറ്റുകള്‍
bytes-received = ലഭിച്ച ബൈറ്റുകള്‍
log-tutorial = ഈ ടൂള്‍ ഉപയോഗിക്കുന്നത് എങ്ങനെ എന്നറിയാന്‍ <a data-l10n-name="logging">HTTP ലോഗിങ്ങ്</a> കാണുക.
current-log-file = നിലവിലെ ലോഗ് ഫയൽ:
current-log-modules = നിലവിലുള്ള ലോഗ് മൊഡ്യൂളുകൾ:
set-log-file = ലോഗ് ഫയൽ സജ്ജമാക്കുക
set-log-modules = ലോഗ് മൊഡ്യൂളുകൾ സജ്ജമാക്കുക
start-logging = ലോഗ് ചെയ്ത് തുടങ്ങുക
stop-logging = ലോഗ് ചെയ്യുന്നത് അവസാനിപ്പിക്കുക
dns-domain = ഡൊമെയിൻ:
total-network-requests = മൊത്തം നെറ്റ്‍വർക്ക് അഭ്യർത്ഥനയുടെ എണ്ണം
rcwn-perf-open = തുറന്നത്
rcwn-perf-read = വായിച്ചത്
rcwn-perf-write = എഴുതിയത്
rcwn-avg-short = ചെറിയ ശരാശരി
rcwn-avg-long = വലിയ ശരാശരി

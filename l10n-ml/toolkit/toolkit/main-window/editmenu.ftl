# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### This file contains the entities needed for the 'edit' menu
### It's currently only used for the Browser Console and Developer Toolbox

editmenu-undo =
    .label = വേണ്ട
    .accesskey = U
editmenu-redo =
    .label = ആവര്‍ത്തിക്കുക
    .accesskey = R
editmenu-cut =
    .label = മുറിക്കുക
    .accesskey = t
editmenu-copy =
    .label = പകര്‍ത്തുക
    .accesskey = C
editmenu-paste =
    .label = ഒട്ടിക്കുക
    .accesskey = P
editmenu-delete =
    .label = നീക്കം ചെയ്യുക
    .accesskey = D
editmenu-select-all =
    .label = എല്ലാം തെരഞ്ഞെടുക്കുക
    .accesskey = A

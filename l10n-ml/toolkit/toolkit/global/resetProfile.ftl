# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

refresh-profile-dialog =
    .title = { -brand-short-name } വീണ്ടും സജ്ജമാക്കുക
refresh-profile-dialog-button =
    .label = { -brand-short-name } വീണ്ടും സജ്ജമാക്കുക
refresh-profile-description = പ്രശ്നങ്ങള്‍ പരിഹരിച്ചു് പ്രവര്‍ത്തനം വീണ്ടെടുക്കുന്നതിനു് പുതുതായി തുടങ്ങുക.
refresh-profile-description-details = അത്:
refresh-profile-remove = നിങ്ങളുടെ ആഡ്-ഓണുകളും ഇഷ്ട്ടപ്പെട്ട മാറ്റങ്ങളും നീക്കം ചെയ്യുക
refresh-profile-restore = നിങ്ങളുടെ ബ്രൌസറിന്റെ സജ്ജീകരണങ്ങള്‍ സ്വതേയാക്കുക
refresh-profile = { -brand-short-name } മികച്ച രീതിയില്‍ സജ്ജമാക്കുക
refresh-profile-button = { -brand-short-name } പുതുക്കുക…

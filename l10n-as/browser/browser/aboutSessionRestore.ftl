# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restore-page-tab-title = অধিবেশন পুনৰুদ্ধাৰ কৰক
# When tabs are distributed across multiple windows, this message is used as a
# header above the group of tabs for each window.
#
# Variables:
#    $windowNumber: Progressive number associated to each window
restore-page-window-label = উইন্ডো { $windowNumber }
restore-page-restore-header =
    .label = পুনৰুদ্ধাৰ কৰক
restore-page-list-header =
    .label = উইন্ডো আৰু টেববোৰ

## The following strings are used in about:welcomeback

welcome-back-tab-title = সফল!
welcome-back-page-title = সফল!
welcome-back-page-info = { -brand-short-name } আৰম্ভ হবলৈ প্ৰস্তুত।
welcome-back-restore-button =
    .label = যাওঁ বোলক!
    .accesskey = L
welcome-back-restore-some-label = কেৱল আপুনি বিচৰাবোৰ পুনৰুদ্ধাৰ কৰক
welcome-back-page-info-link = আপোনাৰ এড-অনসমূহ আৰু স্বনিৰ্বাচনসমূহক আতৰোৱা হৈছে আৰু আপোনাৰ ব্ৰাউছাৰৰ সংহতিসমূহক তাৰ অবিকল্পিতলৈ পুনৰুদ্ধাৰ কৰা হৈছে। যদি ই আপোনাৰ সমস্যাৰ সমাধান কৰা নাই, <a data-l10n-name="link-more">আপুনি কি কৰিব পাৰিব তাৰ বিষয়ে অধিক জানক।</a>

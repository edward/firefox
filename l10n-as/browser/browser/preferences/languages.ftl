# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = ভাষা
    .style = width: 30em
languages-close-key =
    .key = w
languages-customize-moveup =
    .label = ওপৰলৈ নিয়ক
    .accesskey = U
languages-customize-movedown =
    .label = তললৈ নিয়ক
    .accesskey = D
languages-customize-remove =
    .label = আঁতৰাওক
    .accesskey = R
languages-customize-select-language =
    .placeholder = যোগ কৰিবলৈ ভাষা নিৰ্বাচন কৰক…
languages-customize-add =
    .label = যোগ কৰক
    .accesskey = A
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/. --

copy =
    .key = C
menu-copy =
    .label = কপি কৰক
    .accesskey = C
select-all =
    .key = A
menu-select-all =
    .label = সকলো বাছক
    .accesskey = A
close-window =
    .key = A
general-tab =
    .label = সাধাৰণ
    .accesskey = G
general-url =
    .value = ঠিকনা:
general-type =
    .value = ধৰণ:
general-mode =
    .value = প্ৰদৰ্শনৰ ধৰণ:
general-size =
    .value = আকাৰ:
general-referrer =
    .value = উল্লেখ কৰা URL:
general-modified =
    .value = সলনি কৰা হ'ল:
general-meta-name =
    .label = নাম
general-meta-content =
    .label = বিষয়বস্তু
media-tab =
    .label = মাধ্যম
    .accesskey = M
media-location =
    .value = স্থান:
media-text =
    .value = সম্বন্ধিত লিপি:
media-alt-header =
    .label = বৈকল্পিক আখৰ
media-address =
    .label = ঠিকনা
media-type =
    .label = ধৰণ
media-size =
    .label = আকাৰ
media-count =
    .label = গণনা কৰ
media-dimension =
    .value = আয়তন:
media-long-desc =
    .value = দীঘল বিৱৰণ:
media-save-as =
    .label = এই ধৰণে সংৰক্ষণ কৰক…
    .accesskey = A
media-save-image-as =
    .label = এই ধৰণে সংৰক্ষণ কৰক…
    .accesskey = e
media-preview =
    .value = মাধ্যমৰ পূৰ্ব প্ৰদৰ্শন:
perm-tab =
    .label = অনুমতিসমূহ
    .accesskey = P
permissions-for =
    .value = চিহ্নিত কাৰ্য্যৰ বাবে অনুমতি:
security-tab =
    .label = সুৰক্ষা
    .accesskey = S
security-view =
    .label = প্ৰমাণপত্ৰ চাওক
    .accesskey = V
security-view-unknown = অজ্ঞাত
    .value = অজ্ঞাত
security-view-identity =
    .value = ৱেব ছাইটৰ পৰিচয়
security-view-identity-owner =
    .value = গৰাকী:
security-view-identity-domain =
    .value = ৱেবছাইট:
security-view-identity-verifier =
    .value = চিহ্নিত ব্যক্তি অথবা সংঘঠন দ্বাৰা প্ৰমাণিত কৰা হৈছে:
security-view-privacy =
    .value = গোপনীয়তা আৰু ইতিহাস
security-view-privacy-history-value = আজিৰ পূৰ্বে মই এই ৱেব ছাইটত আহিছো নে?
security-view-privacy-passwords-value = এই ৱেব ছাইটৰ বাবে মই কোনো পাছৱৰ্ড সংৰক্ষণ কৰিছোঁ নে?
security-view-privacy-viewpasswords =
    .label = সংৰক্ষিত পাছৱৰ্ড চাওক
    .accesskey = w
security-view-technical =
    .value = কাৰিকৰী বিৱৰণসমূহ
help-button =
    .label = সহায়

## These strings are used to tell the user if the website is storing cookies
## and data on the users computer in the security tab of pageInfo
## Variables:
##   $value (number) - Amount of data being stored
##   $unit (string) - The unit of data being stored (Usually KB)

image-size-unknown = অজ্ঞাত
not-set-verified-by = নিৰ্ধাৰিত কৰা হোৱা নাই
not-set-alternative-text = নিৰ্ধাৰিত কৰা হোৱা নাই
not-set-date = নিৰ্ধাৰিত কৰা হোৱা নাই
media-img = ছবি
media-bg-img = পৃষ্ঠভূমি
media-border-img = বৰ্ডাৰ
media-list-img = বুলেট
media-cursor = কাৰ্চাৰ
media-object = বস্তু
media-embed = প্ৰোথিত কৰ
media-link = আইকন
media-input = নিবেশ
media-video = ভিডিঅ'
media-audio = অডিঅ'
saved-passwords-yes = হয়
saved-passwords-no = নহয়
no-page-title =
    .value = নামহীন পৃষ্ঠা:
general-quirks-mode =
    .value = Quirks অৱস্থা
general-strict-mode =
    .value = মান আজ্ঞা পালনৰ অৱস্থা
security-no-owner = এই ৱেব ছাইটে গৰাকীৰ তথ্য নিদিয়ে।
media-select-folder = ছবি সংৰক্ষণ কৰিবলৈ এটা ফোল্ডাৰ নিৰ্বাচন কৰক
media-unknown-not-cached =
    .value = অজ্ঞাত (ক্যাশ কৰা হোৱা নাই)
permissions-use-default =
    .label = অবিকল্পিত ব্যৱহাৰ কৰক
security-no-visits = নহয়
# This string is used to display the type of
# an image
# Variables:
#   $type (string) - The type of an image
media-image-type =
    .value = { $type } ছবি
# This string is used to display the size of a scaled image
# in both scaled and unscaled pixels
# Variables:
#   $dimx (number) - The horizontal size of an image
#   $dimy (number) - The vertical size of an image
#   $scaledx (number) - The scaled horizontal size of an image
#   $scaledy (number) - The scaled vertical size of an image
media-dimensions-scaled =
    .value = { $dimx }px × { $dimy }px ({ $scaledx }px × { $scaledy }px লে স্কেইল্ড)
# This string is used to display the size of an image in pixels
# Variables:
#   $dimx (number) - The horizontal size of an image
#   $dimy (number) - The vertical size of an image
media-dimensions =
    .value = { $dimx }px × { $dimy }px
# This string is used to display the size of a media
# file in kilobytes
# Variables:
#   $size (number) - The size of the media file in kilobytes
media-file-size = { $size } KB
# This string is used to display the website name next to the
# "Block Images" checkbox in the media tab
# Variables:
#   $website (string) - The website name
media-block-image =
    .label = { $website } ৰ পৰা ছবি প্ৰতিৰোধ কৰক
    .accesskey = B
# This string is used to display the URL of the website on top of the
# pageInfo dialog box
# Variables:
#   $website (string) - The url of the website pageInfo is getting info for
page-info-page =
    .title = পৃষ্ঠাৰ তথ্য - { $website }
page-info-frame =
    .title = ফ্ৰেমৰ তথ্য - { $website }

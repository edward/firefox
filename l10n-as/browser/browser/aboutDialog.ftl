# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

aboutDialog-title =
    .title = { -brand-full-name } ৰ বিষয়ে
update-checkForUpdatesButton =
    .label = আপডেইটসমূহৰ বাবে নিৰীক্ষণ কৰক
    .accesskey = C
update-checkingForUpdates = আপডেইটসমূহৰ বাবে নীৰিক্ষণ কৰা হৈছে…
update-downloading = <img data-l10n-name="icon"/>আপডেইট ডাউনল'ড কৰা হৈছে — <label data-l10n-name="download-status"/>
update-applying = আপডেইট প্ৰয়োগ কৰা হৈছে…
update-failed = আপডেইট ব্যৰ্থ। <label data-l10n-name="failed-link">শেহতীয়া সংস্কৰণ ডাউনল'ড কৰক</label>
update-failed-main = আপডেইট ব্যৰ্থ। <a data-l10n-name="failed-link-main">শেহতীয়া সংস্কৰণ ডাউনল'ড কৰক</a>
update-adminDisabled = আপডেইটসমূহ আপোনাৰ চিস্টেম ব্যৱস্থাপকৰ দ্বাৰা অসামৰ্থবান কৰি থোৱা হৈছে
update-noUpdatesFound = { -brand-short-name } বৰ্তমান তাৰিখলে কাৰ্য্যত
update-otherInstanceHandlingUpdates = { -brand-short-name } ক অন্য এটা উদাহৰণ দ্বাৰা আপডেইট কৰা হৈ আছে
update-manual = আপডেইটসমূহ চিহ্নিত স্থানত উপলব্ধ <label data-l10n-name="manual-link"/>
update-unsupported = আপুনি এই চিস্টেমত ততোধিক আপডেইট পৰিৱেশন কৰিব নোৱাৰিব। <label data-l10n-name="unsupported-link">অধিক জানক</label>
channel-description = আপুনি বৰ্তমানে <label data-l10n-name="current-channel"></label> আপডেইট চেনেলত আছে।{ " " }
warningDesc-version = { -brand-short-name } পৰিক্ষামূলক আৰু অস্থিৰ হব পাৰে।
community-exp = <label data-l10n-name="community-exp-mozillaLink">{ -vendor-short-name }</label> এটা <label data-l10n-name="community-exp-creditsLink">বিশ্বব্যাপী সম্প্ৰদায়</label> যি ৱেবক মুক্ত, ৰাজহুৱা আৰু সকলোৱে অভিগম কৰিব পৰা বনাবলে একেলগে কাম কৰি আছে।
community-2 = { -brand-short-name } ৰূপাংকণ কৰিছে <label data-l10n-name="community-mozillaLink">{ -vendor-short-name }</label>, এটা <label data-l10n-name="community-creditsLink">বিশ্বব্যাপী সম্প্ৰদায়</label> যি ৱেবক মুক্ত, ৰাজহুৱা আৰু সকলোৱে অভিগম কৰিব পৰা বনাবলে একেলগে কাম কৰি আছে।
helpus = সহায় কৰিব বিচাৰে? <label data-l10n-name="helpus-donateLink">এটা বৰঙণি দিয়ক</label> অথবা <label data-l10n-name="helpus-getInvolvedLink">জড়িত হওক!</label>
bottomLinks-license = অনুজ্ঞা তথ্য
bottomLinks-rights = অন্ত-ব্যৱহাৰকাৰী অধিকাৰসমূহ
bottomLinks-privacy = গোপনীয়তা নীতি

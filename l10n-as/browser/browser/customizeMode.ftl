# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

customize-mode-restore-defaults =
    .label = অবিকল্পিতক পুনৰুদ্ধাৰ কৰক
customize-mode-lwthemes-menu-manage =
    .label = ব্যৱস্থাপনা কৰক
    .accesskey = M
customize-mode-titlebar =
    .label = শীৰ্ষক বাৰ
customize-mode-lwthemes =
    .label = থীমসমূহ
customize-mode-lwthemes-menu-get-more =
    .label = অধিক থীমসমূহ প্ৰাপ্ত কৰক
    .accesskey = G
customize-mode-undo-cmd =
    .label = বাতিল কৰক
customize-mode-lwthemes-my-themes =
    .value = মোৰ থীমসমূহ

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

safe-mode-window =
    .title = { -brand-short-name } সুৰক্ষিত অৱস্থা
    .style = max-width: 400px
start-safe-mode =
    .label = সুৰক্ষিত অৱস্থাত আৰম্ভ কৰক
safe-mode-description = সুৰক্ষিত অৱস্থা { -brand-short-name } ৰ এটা বিশেষ অৱস্থা যাক সমস্যাসমূহ সমস্যামুক্তি কৰিবলে ব্যৱহাৰ কৰিব পাৰি।

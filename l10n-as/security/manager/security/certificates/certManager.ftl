# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

certmgr-title =
    .title = প্ৰমাণপত্ৰৰ পৰিচালক
certmgr-tab-mine =
    .label = আপোনাৰ প্ৰমাণপত্ৰসমূহ
certmgr-tab-people =
    .label = মানুহ
certmgr-tab-servers =
    .label = চাৰ্ভাৰসমূহ
certmgr-tab-ca =
    .label = কৰ্ত্তৃপক্ষ
certmgr-detail-general-tab-title =
    .label = সাধাৰণ
    .accesskey = G
certmgr-detail-pretty-print-tab-title =
    .label = বিৱৰণ
    .accesskey = D
certmgr-pending-label =
    .value = বৰ্তমানে প্ৰমাণপত্ৰ সতা সত্য নিৰূপণ কৰা হৈ আছে…
certmgr-subject-info-label =
    .value = চিহ্নিত স্থানলৈ প্ৰদান কৰা হৈছে
certmgr-issuer-info-label =
    .value = প্ৰেৰক
certmgr-period-of-validity-label =
    .value = বৈধতাৰ অৱধি
certmgr-fingerprints-label =
    .value = আঙুলিৰ চাপ
certmgr-subject-label = চিহ্নিত স্থানলৈ প্ৰদান কৰা হৈছে
certmgr-issuer-label = প্ৰেৰক
certmgr-period-of-validity = বৈধতাৰ অৱধি
certmgr-fingerprints = আঙুলিৰ চাপ
certmgr-cert-detail =
    .title = প্ৰমাণপত্ৰৰ বিৱৰণ
    .buttonlabelaccept = বন্ধ কৰক
    .buttonaccesskeyaccept = C
certmgr-cert-detail-cn =
    .value = সাধাৰণ নাম (CN)
certmgr-cert-detail-o =
    .value = সংগঠন (O)
certmgr-cert-detail-ou =
    .value = সন্থাৰ একক (OU)
certmgr-cert-detail-serialnumber =
    .value = ক্ৰম সংখ্যা
certmgr-cert-detail-sha256-fingerprint =
    .value = SHA-256 ফিংগাৰপ্ৰিন্ট
certmgr-cert-detail-sha1-fingerprint =
    .value = SHA1 ফিংগাৰপ্ৰিন্ট
certmgr-cert-detail-commonname = সাধাৰণ নাম (CN)
certmgr-cert-detail-org = সংগঠন (O)
certmgr-cert-detail-orgunit = সন্থাৰ একক (OU)
certmgr-cert-detail-serial-number = ক্ৰম সংখ্যা
certmgr-cert-detail-sha-256-fingerprint = SHA-256 ফিংগাৰপ্ৰিন্ট
certmgr-cert-detail-sha-1-fingerprint = SHA1 ফিংগাৰপ্ৰিন্ট
certmgr-edit-ca-cert =
    .title = CA প্ৰমাণপত্ৰ বিশ্বাসৰ পছন্দ সম্পাদন কৰক
    .style = width: 48em;
certmgr-edit-cert-edit-trust = বিশ্বাসৰ পছন্দ সম্পাদন কৰক:
certmgr-edit-cert-trust-ssl =
    .label = এই প্ৰমাণপত্ৰই ৱেব ছাইট চিনিব পাৰে।
certmgr-edit-cert-trust-email =
    .label = এই প্ৰমাণপত্ৰই মেইল ব্যৱহাৰকাৰী চিনিব পাৰে।
certmgr-delete-cert =
    .title = প্ৰমাণপত্ৰ আঁতৰাওক
    .style = width: 48em; height: 24em;
certmgr-cert-name =
    .label = প্ৰমাণপত্ৰৰ নাম
certmgr-cert-server =
    .label = চাৰ্ভাৰ
certmgr-override-lifetime =
    .label = জীৱনকাল
certmgr-token-name =
    .label = সুৰক্ষাৰ ডিভাইচ
certmgr-begins-on = আৰম্ভ হয়
certmgr-begins-label =
    .label = আৰম্ভ হয়
certmgr-begins-value =
    .value = { certmgr-begins-label.label }
certmgr-expires-on = শেষ হয়
certmgr-expires-label =
    .label = শেষ হয়
certmgr-expires-value =
    .value = { certmgr-expires-label.label }
certmgr-email =
    .label = ই-মেইল ঠিকনা
certmgr-serial =
    .label = ক্ৰম সংখ্যা
certmgr-view =
    .label = দৰ্শন কৰক…
    .accesskey = V
certmgr-edit =
    .label = ভৰষা সম্পাদনা কৰক…
    .accesskey = E
certmgr-export =
    .label = এক্সপোৰ্ট কৰক…
    .accesskey = x
certmgr-delete =
    .label = মচক…
    .accesskey = D
certmgr-delete-builtin =
    .label = মচক অথবা অবিশ্বাস কৰক…
    .accesskey = D
certmgr-backup =
    .label = বেকআপ লওক…
    .accesskey = B
certmgr-backup-all =
    .label = সকলো বেকআপ লওক…
    .accesskey = k
certmgr-restore =
    .label = ইমপোৰ্ট কৰক…
    .accesskey = m
certmgr-details =
    .value = প্ৰমাণপত্ৰৰ ক্ষেত্ৰবোৰ
    .accesskey = F
certmgr-fields =
    .value = ক্ষেত্ৰৰ মান
    .accesskey = V
certmgr-add-exception =
    .label = ব্যতিক্ৰম যোগ কৰক…
    .accesskey = x
exception-mgr =
    .title = সুৰক্ষা ব্যতিক্ৰম যোগ কৰক
exception-mgr-extra-button =
    .label = সুৰক্ষা ব্যতিক্ৰম সুনিশ্চিত কৰক
    .accesskey = C
exception-mgr-supplemental-warning = ন্যায্য বেঙ্ক, ভঁৰাল, আৰু অন্য ৰাজহুৱা ছাইটবোৰে আপোনাক এয়া কৰিবলে নোসোধে।
exception-mgr-cert-location-url =
    .value = অৱস্থান:
exception-mgr-cert-location-download =
    .label = প্ৰমাণপত্ৰ লওক
    .accesskey = G
exception-mgr-cert-status-view-cert =
    .label = দৰ্শন কৰক…
    .accesskey = V
exception-mgr-permanent =
    .label = চিৰস্থায়ীভাৱে এই ব্যতিক্ৰম সঞ্চয় কৰক
    .accesskey = P
pk11-bad-password = দিয়া পাছৱাৰ্ড ভুল।
pkcs12-decode-err = ফাইল ডিক'ড কৰিবলৈ বিফল।  হয় ই PKCS #12 আকৃতিত নহয়, দূষিত হৈছে, বা আপুনি দিয়া পাছৱাৰ্ড অশুদ্ধ।
pkcs12-unknown-err-restore = অজ্ঞাত কাৰণত PKCS #12 ফাইল পুনৰ স্থাপন কৰিবলৈ বিফল।
pkcs12-unknown-err-backup = অজ্ঞাত কাৰণত PKCS #12 বেক-আপ ফাইল সৃষ্টি কৰিবলৈ বিফল।
pkcs12-unknown-err = The PKCS #12 কাৰ্য্য অজ্ঞাত কাৰণত বিফল হ'ল।
pkcs12-info-no-smartcard-backup = smart card ৰ নিচিনা যান্ত্ৰিক সামগ্ৰীৰ সুৰক্ষাৰ যন্ত্ৰৰ পৰা প্ৰমাণপত্ৰ বেক-আপ কৰিব নোৱাৰি।
pkcs12-dup-data = প্ৰমাণপত্ৰ আৰু ব্যক্তিগত কি' সুৰক্ষাৰ যন্ত্ৰত ইতিমধ্যে আছে। প্ৰমাণপত্ৰ আৰু ব্যক্তিগত কি' ইতিমধ্য সুৰক্ষা ডিভাইচত উপস্থিত।

## PKCS#12 file dialogs

choose-p12-backup-file-dialog = কপি কৰি ৰক্ষা কৰা ফাইলৰ নাম
file-browse-pkcs12-spec = PKCS12 ফাইল
choose-p12-restore-file-dialog = ইমপোৰ্ট কৰিব লগিয়া প্ৰমাণপত্ৰ ফাইল

## Import certificate(s) file dialog

file-browse-certificate-spec = প্ৰমাণপত্ৰৰ ফাইল
import-ca-certs-prompt = ইমপোৰ্ট কৰিব লগা CA প্ৰমাণপত্ৰ(সমূহ) থকা ফাইল বাছক
import-email-cert-prompt = ইমপোৰ্ট কৰিব লগা কোনো লোকৰ ই-মেইল প্ৰমাণপত্ৰ থকা ফাইল বাছক

## For editing certificates trust

# Variables:
#   $certName: the name of certificate
edit-trust-ca = "{ $certName }" প্ৰমাণপত্ৰই এটা প্ৰমাণপত্ৰৰ কৰ্ত্তৃপক্ষৰ প্ৰতিনিধি কৰে।

## For Deleting Certificates

delete-user-cert-title =
    .title = আপোনাৰ নিজৰ প্ৰমাণপত্ৰ আঁতৰাওক
delete-user-cert-confirm = এই প্ৰমাণপত্ৰসমূহ আঁতৰাবলৈ আপুনি নিশ্চিত নে?
delete-user-cert-impact = আপুনি নিজৰে এটা প্ৰমাণপত্ৰ আঁতৰালে, আপুনি তাক নিজৰ পৰিচয় হিচাপে আৰু ব্যৱহাৰ কৰিব নোৱাৰে।
delete-ssl-cert-title =
    .title = চাৰ্ভাৰৰ প্ৰমাণপত্ৰৰ অব্যাহতি আঁতৰাওক
delete-ssl-cert-confirm = আপুনি এই চাৰ্ভাৰৰ অব্যাহতি আঁতৰাবলৈ নিশ্চিত নে?
delete-ssl-cert-impact = আপুনি চাৰ্ভাৰৰ অব্যাহতি আঁতৰালে, আপুনি তাৰ সাধাৰণ সুৰক্ষাৰ পৰীক্ষা পুনঃ স্থাপন কৰে আৰু এটা বৈধ প্ৰমাণপত্ৰ ব্যৱহাৰ কৰাৰ প্ৰয়োজন হয়।
delete-ca-cert-title =
    .title = CA প্ৰমাণপত্ৰসমূহ মচি পেলাওক বা ভৰষাহীন কৰক
delete-ca-cert-confirm = আপুনি এই CA প্ৰমাণপত্ৰসমূহ মচি পেলোৱাৰ অনুৰোধ কৰিছে। বিল্ট-ইন প্ৰমাণপত্ৰসমূহৰ বাবে সকলো ভৰষা মচি পেলোৱা হব, যাৰ একে প্ৰভাৱ আছে। আপুনি নিশ্চিত নে আপুনি মচিব বা ভৰষাহীন কৰিব বিচাৰে?
delete-ca-cert-impact = যদি আপুনি এটা প্ৰমাণপত্ৰ কতৃপক্ষ (CA) প্ৰমাণপত্ৰ মচি পেলায় বা ভৰষাহীন কৰে, এই অনুপ্ৰয়োগে সেই CA প্ৰেৰণ কৰা কোনো প্ৰমাণপত্ৰয় ভৱিষ্যতত ভৰষা নকৰে।
delete-email-cert-title =
    .title = ই-মেইল প্ৰমাণপত্ৰ আঁতৰাওক
delete-email-cert-confirm = আপুনি এই কিজনৰ ই-মেইল প্ৰমাণপত্ৰ আঁতৰাবলৈ নিশ্চিত নে?
delete-email-cert-impact = আপুনি এইজনৰ ই-মেইল প্ৰমাণপত্ৰ আঁতৰালে, আপুনি তেওঁলৈ আৰু এনক্ৰিপ্ট কৰা ই-মেইল পঠিয়াব নোৱাৰিব।

## Cert Viewer

not-present =
    .value = <প্ৰমাণপত্ৰ অংশ নহয়>
# Cert verification
cert-verified = প্ৰমাণপত্ৰক পৰীক্ষা কৰা হোৱা নাই কাৰণ:
# Add usage
verify-ssl-client =
    .value = SSL Client Certificate
verify-ssl-server =
    .value = SSL চাৰ্ভাৰৰ প্ৰমাণপত্ৰ
verify-ssl-ca =
    .value = SSL প্ৰমাণপত্ৰৰ কৰ্ত্তৃপক্ষ
verify-email-signer =
    .value = ই-মেইল স্বাক্ষৰক প্ৰমাণপত্ৰ
verify-email-recip =
    .value = ই-মেইল গ্ৰহনকাৰী প্ৰমাণপত্ৰ
# Cert verification
cert-not-verified-cert-revoked = ইয়াক ৰিভ'ক কৰাৰ কাৰণে এই প্ৰমাণপত্ৰক পৰীক্ষা কৰিব পৰা নগল।
cert-not-verified-cert-expired = ই শেষ হোৱাৰ কাৰণে এই প্ৰমাণপত্ৰক পৰীক্ষা কৰিব পৰা নগল।
cert-not-verified-cert-not-trusted = ই বিশ্বাসী নোহোৱাৰ কাৰণে এই প্ৰমাণপত্ৰক পৰীক্ষা কৰিব পৰা নগল।
cert-not-verified-issuer-not-trusted = প্ৰদানকৰোঁতা বিশ্বাসী নোহোৱাৰ কাৰণে এই প্ৰমাণপত্ৰক পৰীক্ষা কৰিব পৰা নগল।
cert-not-verified-issuer-unknown = প্ৰদানকৰোঁতা অজ্ঞাত হোৱাৰ কাৰণে এই প্ৰমাণপত্ৰক পৰীক্ষা কৰিব পৰা নগল।
cert-not-verified-ca-invalid = CA প্ৰমাণপত্ৰ অবৈধ হোৱাৰ কাৰণে এই প্ৰমাণপত্ৰক পৰীক্ষা কৰিব পৰা নগল।
cert-not-verified_algorithm-disabled = এই প্ৰমাণপত্ৰক সতাসত্য নিৰূপণ কৰিব নোৱাৰি কাৰণ ইয়াক এটা স্বাক্ষৰ এলগৰিথম ব্যৱহাৰ কৰি স্বাক্ষৰ কৰা হৈছিল যাক অসামৰ্থবান কৰা হৈছিল কাৰণ সেই এলগৰিথম সুৰক্ষিত নাছিল।
cert-not-verified-unknown = অজ্ঞাত কাৰণে এই প্ৰমাণপত্ৰ পৰীক্ষা কৰিব পৰা নগল।

## Add Security Exception dialog

add-exception-branded-warning = এই ছাইটক { -brand-short-name } এ কেনেদৰে চিনি পাই আপুনি সেইটো আওকাণ কৰি আগবাছিব খুজিছে।
add-exception-invalid-header = এই ছাইটে নিজকে অবৈধ তথ্যৰে নিজকে চিনাকি দিব বিচাৰে।
add-exception-domain-mismatch-short = ভুল ছাইট
add-exception-expired-short = পূৰনি তথ্য
add-exception-unverified-or-bad-signature-short = অজ্ঞাত পৰিচয়
add-exception-unverified-or-bad-signature-long = প্ৰমাণপত্ৰক ভৰষা কৰিব নোৱাৰি, কাৰণ ইয়াক এটা সুৰক্ষিত স্বাক্ষৰ ব্যৱহাৰ কৰা পৰিচিত কতৃপক্ষ দ্বাৰা সতাসত্য নিৰূপণ কৰা হোৱা নাই
add-exception-valid-short = বৈধ প্ৰমাণপত্ৰ
add-exception-valid-long = এই ছাইটে বৈধ, প্ৰমাণিত পৰিচয় দিয়ে।  অব্যাহতি যোগ কৰাৰ প্ৰয়োজন নাই।
add-exception-checking-short = তথ্য পৰীক্ষা কৰা হৈছে
add-exception-no-cert-short = কোনো তথ্য নাই

## Certificate export "Save as" and error dialogs

save-cert-as = প্ৰমাণপত্ৰ ফাইলত ৰক্ষা কৰক
cert-format-base64 = X.509 প্ৰমাণপত্ৰ (PEM)
cert-format-base64-chain = শৃংখল (PEM) ৰ সৈতে X.509 প্ৰমাণপত্ৰ
cert-format-der = X.509 প্ৰমাণপত্ৰ (DER)
cert-format-pkcs7 = X.509 প্ৰমাণপত্ৰ (PKCS#7)
cert-format-pkcs7-chain = শংখল (PKCS#7) ৰ সৈতে X.509 প্ৰমাণপত্ৰ
write-file-failure = ফাইলত ভুল

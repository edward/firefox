# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

password-not-set =
    .value = (নিৰ্ধাৰিত নহয়)
failed-pw-change = প্ৰমূখ পাছৱাৰ্ড সলনি কৰিব পৰা নাযায়।
incorrect-pw = আপুনি শুদ্ধ প্ৰমূখ পাছৱাৰ্ড নিদিলে। অনুগ্ৰহ কৰি আকৌ চেষ্টা কৰক।
pw-change-ok = প্ৰমূখ পাছৱাৰ্ড সফলভাবে সলনি কৰা হ'ল।
pw-empty-warning = আপোনাৰ সংৰক্ষিত ৱেব আৰু ঈ-মেইলৰ পাছৱাৰ্ড, ফৰ্ম তথ্য, আৰু ব্যক্তিগত চাবিক সুৰক্ষা দিয়া ন'হ'ব।
pw-erased-ok = আপুনি নিজৰ প্ৰমূখ পাছৱাৰ্ড আঁতৰাই দিলে।  { pw-empty-warning }
pw-not-wanted = সকিয়নি! আপুনি প্ৰমূখ পাছৱাৰ্ড ব্যৱহাৰ নকৰিবলৈ নিশ্চিত কৰিছে। { pw-empty-warning }
pw-change2empty-in-fips-mode = আপুনি বৰ্ত্তমানে FIPS মোডত। FIPS ক এটা ৰিক্ত নোহোৱা প্ৰমূখ পাছৱাৰ্ডৰ প্ৰয়োজন।
pw-change-success-title = পাছৱাৰ্ড সলনি কৰা সফল হ'ল
pw-change-failed-title = পাছৱাৰ্ড সলনি কৰা ব্যৰ্থ হ'ল
pw-remove-button =
    .label = আঁতৰাওক
set-password =
    .title = প্ৰমূখ পাছৱাৰ্ড সলনি কৰক
set-password-old-password = বৰ্ত্তমানৰ পাছৱাৰ্ড:
set-password-new-password = নতুন পাছৱাৰ্ড দিয়ক:
set-password-reenter-password = পাছৱাৰ্ড আকৌ দিয়ক:
set-password-meter = পাছৱাৰ্ডৰ গুণৰ একক
set-password-meter-loading = ল'ড কৰা হৈছে
master-password-description = প্ৰমূখ পাছৱাৰ্ড ব্যৱহাৰ কৰা হয় ছাইটৰ পাছৱাৰ্ডৰ দৰে সংবেদনশীল তথ্য সংৰক্ষণ কৰিবলৈ ব্যৱহাৰ কৰা হয়।  আপুনি এটা প্ৰমূখ পাছৱাৰ্ড সৃষ্টি কৰিলে প্ৰতি অধিবেশনত এবাৰ তাক লিখিবলৈ কোৱা হ'ব যেতিয়া { -brand-short-name } এ পাছৱাৰ্ডৰে সংৰক্ষিত তথ্য উদ্ধাৰ কৰে।
master-password-warning = আপুনি নিৰ্ধাৰণ কৰা পাছৱাৰ্ড অনুগ্ৰহ কৰি মনত ৰাখিব।  প্ৰমূখ পাছৱাৰ্ড পাহৰিলে, সি সংৰক্ষণ কৰা কোনো তথ্য অভিগম কৰিব নোৱাৰিব।
remove-password =
    .title = প্ৰমূখ পাছৱাৰ্ড আঁতৰাওক
remove-info =
    .value = আগবাঢ়িবলৈ আপুনি বৰ্ত্তমানৰ পাছৱাৰ্ড দিব লাগিব:
remove-warning1 = আপোনাৰ প্ৰমূখ পাছৱাৰ্ড ব্যৱহাৰ কৰা হয় গোপন তথ্য যেনে ছাইটৰ পাছৱাৰ্ড ৰক্ষা কৰিবলৈ।
remove-warning2 = প্ৰমূখ পাছৱাৰ্ড আঁতৰালে আপোনাৰ কম্পিউটাৰৰ ক্ষতি হ'লে আপোনাৰ তথ্য সংৰক্ষিত ন'হ'ব।
remove-password-old-password =
    .value = বৰ্ত্তমানৰ পাছৱাৰ্ড:

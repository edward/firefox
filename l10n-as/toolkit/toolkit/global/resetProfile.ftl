# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

refresh-profile-dialog =
    .title = { -brand-short-name } সতেজ কৰক
refresh-profile-dialog-button =
    .label = { -brand-short-name } সতেজ কৰক
refresh-profile-description = সমস্যাসমূহ সমাধান কৰিবলৈ আৰু পৰিৱেশন উদ্ধাৰ কৰিবলৈ নতুনকৈ আৰম্ভ কৰক।
refresh-profile-description-details = ই:
refresh-profile-remove = আপোনাৰ এড-অনসমূহ আৰু স্বনিৰ্বাচনসমূহ আতৰাওক
refresh-profile-restore = আপোনাৰ ব্ৰাউছাৰ সংহতিসমূহ অবিকল্পিতলৈ পুনৰ সংহতি কৰক
refresh-profile = { -brand-short-name } ক টিউন আপ কৰক
refresh-profile-button = { -brand-short-name } সতেজ কৰক…

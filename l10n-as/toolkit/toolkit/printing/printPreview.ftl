# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

printpreview-close =
    .label = বন্ধ কৰক
    .accesskey = C
printpreview-portrait =
    .label = পট্ৰেইট
    .accesskey = o
printpreview-landscape =
    .label = লেণ্ডস্কেইপ
    .accesskey = L
printpreview-scale =
    .value = স্কেইল:
    .accesskey = S
printpreview-shrink-to-fit =
    .label = খাপ খুৱাবলৈ সৰু কৰক
printpreview-custom =
    .label = স্বনিৰ্বাচিত...
printpreview-print =
    .label = প্ৰিন্ট কৰক…
    .accesskey = P
printpreview-of =
    .value = ৰ
printpreview-custom-prompt =
    .value = স্বনিৰ্বাচিত স্কেইল...
printpreview-page-setup =
    .label = পৃষ্ঠাৰ সংস্থাপন…
    .accesskey = u
printpreview-page =
    .value = পৃষ্ঠা:
    .accesskey = a

## Variables
## $percent (integer) - menuitem percent label
## $arrow (String) - UTF-8 arrow character for navigation buttons

printpreview-homearrow =
    .label = { $arrow }
    .tooltiptext = প্ৰথম পৃষ্ঠা
printpreview-previousarrow =
    .label = { $arrow }
    .tooltiptext = আগৰ পৃষ্ঠা
printpreview-nextarrow =
    .label = { $arrow }
    .tooltiptext = পিছৰ পৃষ্ঠা
printpreview-endarrow =
    .label = { $arrow }
    .tooltiptext = শেষৰ পৃষ্ঠা

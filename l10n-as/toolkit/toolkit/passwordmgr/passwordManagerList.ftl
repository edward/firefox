# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

window-close =
    .key = w
focus-search-shortcut =
    .key = f
focus-search-altshortcut =
    .key = k
copy-username-cmd =
    .label = ব্যৱহাৰকাৰীনাম কপি কৰক
    .accesskey = U
copy-password-cmd =
    .label = পাছৱাৰ্ড কপি কৰক
    .accesskey = C
column-heading-site =
    .label = ছাইট
column-heading-username =
    .label = ব্যৱহাৰকাৰী নাম
column-heading-password =
    .label = পাছৱাৰ্ড
column-heading-time-created =
    .label = প্ৰথম ব্যৱহৃত
column-heading-time-last-used =
    .label = সৰ্বশেষ ব্যৱহৃত
column-heading-time-password-changed =
    .label = সৰ্বশেষ পৰিবৰ্তিত
column-heading-times-used =
    .label = ব্যৱহাৰৰ সময়কাল
remove =
    .label = আতৰাওক
    .accesskey = R
import =
    .label = ইমপোৰ্ট কৰক…
    .accesskey = I
close-button =
    .label = বন্ধ কৰক
    .accesskey = C
remove-all-passwords-prompt = আপুনি সকলো পাছৱাৰ্ড আতৰাবলে নিশ্চিত নে?
remove-all-passwords-title = সকলো পাছৱাৰ্ড আতৰাওক
no-master-password-prompt = আপুনি আপোনাৰ পাছৱাৰ্ড দেখুৱাবলে নিশ্চিত নে?

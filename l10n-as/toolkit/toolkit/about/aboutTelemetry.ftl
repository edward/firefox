# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-telemetry-page-title = টেলিমেট্ৰি তথ্য
about-telemetry-general-data-section = সাধাৰণ তথ্য
about-telemetry-histograms-section = হিস্টোগ্ৰামসমূহ
about-telemetry-keyed-histogram-section = কি'ড হিস্ট'গ্ৰামচ্
about-telemetry-simple-measurements-section = সাধাৰণ জোখমাখ
about-telemetry-slow-sql-section = লেহেম SQL স্টেইটমেন্টবোৰ
about-telemetry-addon-details-section = এড-অন বিৱৰণসমূহ
about-telemetry-late-writes-section = শেষৰ লিখনিসমূহ
about-telemetry-full-sql-warning = টোকা: লেহেম SQL ডিবাগিং সামৰ্থবান কৰা আছে। সম্পূৰ্ণ SQL স্ট্ৰিংসমূহ তলত দেখুৱা হব পাৰে কিন্তু সিহতক টেলিমেট্ৰিত জমা দিয়া নহব।
# Variables:
#   $telemetryServerOwner (String): the value of the toolkit.telemetry.server_owner preference. Typically "Mozilla"
about-telemetry-page-subtitle = এই পৃষ্ঠায় টেলিমেট্ৰি দ্বাৰা সংগ্ৰহ কৰা পৰিৱেশন, হাৰ্ডৱেৰ, ব্যৱহাৰ আৰু স্বনিৰ্বাচনসমূহৰ বিষয়ে তথ্য দেখুৱায়। এই তথ্য { -brand-full-name } ক উন্নত কৰাত সহায় কৰিবলে { $telemetryServerOwner } লৈ জমা দিয়া হয়।
# button label to copy the histogram
about-telemetry-histogram-copy = কপি কৰক
# these strings are used in the “Slow SQL Statements” section
about-telemetry-slow-sql-main = মূখ্য থ্ৰেডত লেহেম SQL স্টেইটমেন্টসমূহ
about-telemetry-slow-sql-other = সহায়ক থ্ৰেডসমূহত লেহেম SQL স্টেইটমেন্টসমূহ
about-telemetry-slow-sql-hits = হিটসমূহ
about-telemetry-slow-sql-average = গড় সময় (ms)
about-telemetry-slow-sql-statement = স্টেইটমেন্ট
# these strings are used in the “Add-on Details” section
about-telemetry-addon-table-id = এড-অন ID
about-telemetry-addon-table-details = বিৱৰণ
# Variables:
#   $addonProvider (String): the name of an Add-on Provider (e.g. “XPI”, “Plugin”)
about-telemetry-addon-provider = { $addonProvider } প্ৰদানকাৰী
about-telemetry-keys-header = বৈশিষ্ট্য
# Variables:
#   $lateWriteCount (Integer): the number of the late writes
about-telemetry-late-writes-title = শেষ লিখনি #{ $lateWriteCount }
about-telemetry-stack-title = স্টেক:
about-telemetry-memory-map-title = মেমৰিৰ মেপ:
about-telemetry-error-fetching-symbols = চিহ্নসমূহ প্ৰাপ্ত কৰোতে এটা ত্ৰুটি দেখা দিলে। আপুনি ইন্টাৰনেটৰ সৈতে সংযুক্ত আছে নে নীৰিক্ষণ কৰি পুনৰ চেষ্টা কৰি চাওক।

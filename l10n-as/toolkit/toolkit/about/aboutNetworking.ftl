# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

title = নেটৱাৰ্কিংৰ বিষয়ে
warning = ই সম্পূৰ্ণ পৰিক্ষামূলক। পূৰ্ণাংগ পৰ্যবেক্ষনৰ অবিহনে ব্যৱহাৰ নকৰিব।
show-next-time-checkbox = পিছৰ বাৰ এই সকিয়নি দেখুৱাব
ok = ঠিক আছে
sockets = চকেটসমূহ
dns = DNS
websockets = ৱেবচকেটসমূহ
refresh = সতেজ কৰক
auto-refresh = প্ৰতি ৩ ছেকেণ্ডত স্বচালিতভাৱে সতেজ কৰক
hostname = হস্টনাম
port = পৰ্ট
ssl = SSL
active = সক্ৰিয়
idle = অলস
host = হস্ট
tcp = TCP
sent = প্ৰেৰিত
received = প্ৰাপ্ত
family = পৰিয়াল
addresses = ঠিকনাসমূহ
expires = অৱসান ঘটিব (ছেকেণ্ড)
messages-sent = প্ৰেৰিত বাৰ্তাসমূহ
messages-received = প্ৰাপ্ত বাৰ্তাসমূহ
bytes-sent = প্ৰেৰিত বাইট
bytes-received = প্ৰাপ্ত বাইট

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

history-title = ইতিহাস আপডেইট কৰক
close-button-label =
    .buttonlabelcancel = বন্ধ কৰক
    .title = ইতিহাস আপডেইট কৰক
no-updates-label = কোনো আপডেইট ইনস্টল কৰা হোৱা নাই
name-header = আপডেইটৰ নাম
date-header = ইনস্টলৰ তাৰিখ
type-header = ধৰণ
state-header = অবস্থা
# Used to display update history
#
# Variables:
#   $name (String): name of the update
#   $buildID (String): build identifier from the local updates.xml
update-full-name =
    .name = { $name } ({ $buildID })
# Used to display update history
#
# Variables:
#   $name (String): name of the update
#   $buildID (String): build identifier from the local updates.xml
update-full-build-name = { $name } ({ $buildID })
update-details = বিৱৰণ
update-installed-on = চিহ্নিত স্থানত ইনস্টল্ড: { $date }
update-status = অৱস্থা: { $status }

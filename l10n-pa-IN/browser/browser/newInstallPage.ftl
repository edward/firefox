# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### For this feature, "installation" is used to mean "this discrete download of
### Firefox" and "version" is used to mean "the specific revision number of a
### given Firefox channel". These terms are not synonymous.

title = ਅਹਿਮ ਖ਼ਬਰਾਂ
heading = ਆਪਣੇ { -brand-short-name } ਪਰੋਫਾਇਲ ਲਈ ਬਦਲੋ
changed-title = ਕੀ ਬਦਲ ਗਿਆ?
options-title = ਮੇਰੇ ਕੋਲ ਕੀ ਚੋਣਾਂ ਹਨ?
resources = ਸਰੋਤ:
sync-label = ਆਪਣਾ ਈਮੇਲ ਭਰੋ
sync-input =
    .placeholder = ਈਮੇਲ
sync-button = ਜਾਰੀ ਰੱਖੋ
sync-learn = ਹੋਰ ਜਾਣੋ

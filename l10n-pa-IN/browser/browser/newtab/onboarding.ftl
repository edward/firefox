# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = ਹੁਣੇ ਕੋਸ਼ਿਸ਼ ਕਰੋ
onboarding-button-label-get-started = ਸ਼ੁਰੂ ਕਰੀਏ
onboarding-welcome-header = { -brand-short-name } ਵਲੋਂ ਜੀ ਆਇਆਂ ਨੂੰ
onboarding-start-browsing-button-label = ਬਰਾਊਜ਼ ਕਰਨਾ ਸ਼ੁਰੂ ਕਰੋ

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = ਪ੍ਰਾਈਵੇਟ ਬਰਾਊਜ਼ਿੰਗ
onboarding-private-browsing-text = ਖੁਦ ਬਰਾਊਜ਼ ਕਰੋ। ਸਮੱਗਰੀ 'ਤੇ ਪਾਬੰਦੀ ਨਾਲ ਪ੍ਰਾਈਵੇਟ ਬਰਾਊਜ਼ਿੰਗ ਰਾਹੀਂ ਆਨਲਾਈਨ ਟਰੈਕਰਾਂ ਉੱਤੇ ਪਾਬੰਦੀ ਲੱਗਦੀ ਹੈ, ਜੋ ਕਿ ਵੈੱਬ 'ਤੇ ਤੁਹਾਡਾ ਪਿੱਛਾ ਕਰਦੇ ਹਨ।
onboarding-screenshots-title = ਸਕਰੀਨਸ਼ਾਟ
onboarding-addons-title = ਐਡ-ਆਨ
# Note: "Sync" in this case is a generic verb, as in "to synchronize"
onboarding-fxa-title = ਸਿੰਕ ਕਰੋ
onboarding-fxa-text = { -fxaccount-brand-name } ਲਈ ਸਾਈਨ ਅੱਪ ਕਰੋ ਅਤੇ ਆਪਣੇ ਬੁੱਕਮਾਰਕਾਂ, ਪਾਸਵਰਡਾਂ ਨੂੰ ਸਿੰਕ ਕਰੋ ਤੇ ਜਿੱਥੇ ਵੀ { -brand-short-name } ਨੂੰ ਵਰਤੋਂ, ਉੱਥੇ ਟੈਬਾਂ ਨੂੰ ਖੋਲ੍ਹੋ।

## Message strings belonging to the Return to AMO flow

return-to-amo-sub-header = ਬੱਲੇ, ਤੁਸੀਂ { -brand-short-name } ਲਿਆ ਹੈ
# <icon></icon> will be replaced with the icon belonging to the extension
#
# Variables:
#   $addon-name (String) - Name of the add-on
return-to-amo-addon-header = ਆਓ ਹੁਣ ਤੁਹਾਨੂੰ <icon></icon><b>{ $addon-name } ਦੇਈਏ।</b>
return-to-amo-extension-button = ਇਕਸਟੈਨਸ਼ਨ ਜੋੜੋ
return-to-amo-get-started-button = { -brand-short-name } ਨਾਲ ਸ਼ੁਰੂ ਕਰੋ

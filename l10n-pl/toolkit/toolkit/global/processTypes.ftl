# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

process-type-web = Strony
# process used to run privileged pages,
# such as about:home
process-type-privileged = Uprzywilejowane strony
process-type-extension = Rozszerzenie
# process used to open file:// URLs
process-type-file = Lokalny plik
# process used to isolate webpages that requested special
# permission to allocate large amounts of memory
process-type-weblargeallocation = Duży przydział pamięci
# process used to communicate with the GPU for
# graphics acceleration
process-type-gpu = GPU

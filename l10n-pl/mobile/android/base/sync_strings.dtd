<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "Firefox Sync">
<!ENTITY syncBrand.shortName.label "Sync">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label 'Połącz z &syncBrand.shortName.label;'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'Aby aktywować nowe urządzenie, wybierz na nim opcję „Skonfiguruj &syncBrand.shortName.label;”.'>
<!ENTITY sync.subtitle.pair.label 'Aby aktywować, na innym urządzeniu wybierz „Powiąż urządzenie”.'>
<!ENTITY sync.pin.default.label '…\n…\n…\n'>
<!ENTITY sync.link.nodevice.label 'Nie mam przy sobie tego urządzenia…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'Dane logowania'>
<!ENTITY sync.configure.engines.title.history 'Historia'>
<!ENTITY sync.configure.engines.title.tabs 'Karty'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS1; na urządzeniu &formatS2;'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'Menu Zakładki'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'Etykiety'>
<!ENTITY bookmarks.folder.toolbar.label 'Pasek zakładek'>
<!ENTITY bookmarks.folder.other.label 'Pozostałe zakładki'>
<!ENTITY bookmarks.folder.desktop.label 'Zakładki z komputera'>
<!ENTITY bookmarks.folder.mobile.label 'Zakładki z telefonu'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'Przypięte'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'Wróć do przeglądarki'>

<!ENTITY fxaccount_getting_started_welcome_to_sync 'Witaj w usłudze &syncBrand.shortName.label;'>
<!ENTITY fxaccount_getting_started_description2 'Zaloguj się i synchronizuj karty, zakładki, dane logowania i więcej.'>
<!ENTITY fxaccount_getting_started_get_started 'Zaloguj się'>
<!ENTITY fxaccount_getting_started_old_firefox 'Korzystasz ze starej wersji usługi &syncBrand.shortName.label;?'>

<!ENTITY fxaccount_status_auth_server 'Serwer konta'>
<!ENTITY fxaccount_status_sync_now 'Synchronizuj'>
<!ENTITY fxaccount_status_syncing2 'Synchronizowanie…'>
<!ENTITY fxaccount_status_device_name 'Nazwa urządzenia'>
<!ENTITY fxaccount_status_sync_server 'Serwer synchronizacji'>
<!ENTITY fxaccount_status_needs_verification2 'Konto wymaga weryfikacji. Stuknij, aby wysłać nowy odnośnik weryfikujący.'>
<!ENTITY fxaccount_status_needs_credentials 'Nie można się połączyć. Stuknij, aby się zalogować.'>
<!ENTITY fxaccount_status_needs_upgrade '&brandShortName; wymaga aktualizacji przed zalogowaniem.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled '&syncBrand.shortName.label; jest skonfigurowany, ale nie synchronizuje się automatycznie. Włącz automatyczną synchronizację danych w ustawieniach systemu Android.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 '&syncBrand.shortName.label; jest skonfigurowany, ale nie synchronizuje się automatycznie. Włącz automatyczną synchronizację danych w ustawieniach kont systemu Android.'>
<!ENTITY fxaccount_status_needs_finish_migrating 'Stuknij, aby zalogować się do nowego konta Firefoksa.'>
<!ENTITY fxaccount_status_choose_what 'Wybierz, co synchronizować'>
<!ENTITY fxaccount_status_bookmarks 'Zakładki'>
<!ENTITY fxaccount_status_history 'Historia'>
<!ENTITY fxaccount_status_passwords2 'Dane logowania'>
<!ENTITY fxaccount_status_tabs 'Otwarte karty'>
<!ENTITY fxaccount_status_additional_settings 'Dodatkowe ustawienia'>
<!ENTITY fxaccount_pref_sync_use_metered2 'Tylko przez Wi-Fi'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 'Uniemożliwia przeglądarce &brandShortName; synchronizowanie przez sieć komórkową lub taryfową'>
<!ENTITY fxaccount_status_legal 'Podstawa prawna' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'Warunki użytkowania'>
<!ENTITY fxaccount_status_linkprivacy2 'Polityka prywatności'>
<!ENTITY fxaccount_remove_account 'Rozłącz…'>

<!ENTITY fxaccount_remove_account_dialog_title2 'Rozłączyć synchronizację?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'Dane przeglądania pozostaną na tym urządzeniu, ale nie będą więcej synchronizowane z kontem Firefoksa.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 'Rozłączono konto Firefoksa użytkownika &formatS;.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'Rozłącz'>

<!ENTITY fxaccount_enable_debug_mode 'Włącz debugowanie'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title 'Opcje usługi &syncBrand.shortName.label;'>
<!ENTITY fxaccount_options_configure_title 'Skonfiguruj usługę &syncBrand.shortName.label;'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 '&syncBrand.shortName.label; nie może się połączyć'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 'Stuknij, aby zalogować się jako &formatS;'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title 'Dokończyć aktualizację &syncBrand.shortName.label;?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text 'Stuknij, aby zalogować się jako &formatS;'>

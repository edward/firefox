<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY aboutPrivateBrowsing.notPrivate                 "No está actualmente en una ventana privada.">
<!ENTITY privatebrowsingpage.openPrivateWindow.label     "Abrir una ventana privada">
<!ENTITY privatebrowsingpage.openPrivateWindow.accesskey "P">

<!ENTITY privateBrowsing.title                           "Navegación privada">
<!ENTITY aboutPrivateBrowsing.info.notsaved.before       "Cuando navega en una ventana privada, Firefox ">
<!ENTITY aboutPrivateBrowsing.info.notsaved.emphasize    "no guarda">
<!ENTITY aboutPrivateBrowsing.info.notsaved.after        " sus:">
<!ENTITY aboutPrivateBrowsing.info.visited               "páginas visitadas">
<!ENTITY aboutPrivateBrowsing.info.searches              "búsquedas">
<!ENTITY aboutPrivateBrowsing.info.cookies               "cookies">
<!ENTITY aboutPrivateBrowsing.info.temporaryFiles        "archivos temporales">
<!ENTITY aboutPrivateBrowsing.info.saved.before          "Firefox ">
<!ENTITY aboutPrivateBrowsing.info.saved.emphasize       "guardará">
<!ENTITY aboutPrivateBrowsing.info.saved.after2          " sus:">
<!ENTITY aboutPrivateBrowsing.info.downloads             "descargas">
<!ENTITY aboutPrivateBrowsing.info.bookmarks             "marcadores">
<!ENTITY aboutPrivateBrowsing.info.clipboard             "texto copiado">
<!ENTITY aboutPrivateBrowsing.note.before                "La navegación privada ">
<!ENTITY aboutPrivateBrowsing.note.emphasize             "no le hace anónimo">
<!ENTITY aboutPrivateBrowsing.note.after                 " en Internet. Su empleador o proveedor de servicios de Internet aún pueden saber qué paginas visita.">
<!ENTITY aboutPrivateBrowsing.learnMore3.before          "Más información sobre ">
<!ENTITY aboutPrivateBrowsing.learnMore3.title           "Navegación privada">
<!ENTITY aboutPrivateBrowsing.learnMore3.after           ".">

<!ENTITY trackingProtection.startTour1                   "Ver cómo funciona">

<!ENTITY contentBlocking.title                           "Bloqueo de contenido">
<!ENTITY contentBlocking.description                     "Algunos sitios web usan rastreadores que pueden controlar su actividad a través de Internet. En las ventanas privadas, el bloqueo de contenido de Firefox bloquea automáticamente muchos rastreadores que pueden recopilar información acerca de su navegación.">

<!-- Strings for new Private Browsing with Search -->
<!-- LOCALIZATION NOTE (aboutPrivateBrowsing.search.placeholder): This is the placeholder
                       text for the search box.   -->
<!ENTITY aboutPrivateBrowsing.search.placeholder         "Buscar en la web">
<!ENTITY aboutPrivateBrowsing.info.title                 "Está en una ventana privada">
<!ENTITY aboutPrivateBrowsing.info.description           "&brandShortName; borra su historial de búsqueda y navegación al salir de la aplicación o al cerrar todas las ventanas y pestañas de navegación privada. Aunque esto no le hace anónimo para los sitios web o para su proveedor de servicios de Internet, hace que sea más fácil mantener en privado lo que hace en línea de cualquier otra persona que utilice este equipo.">
<!-- LOCALIZATION NOTE (aboutPrivateBrowsing.info.myths): This is a link to a SUMO article
                       about private browsing myths.   -->
<!ENTITY aboutPrivateBrowsing.info.myths                 "Mitos comunes sobre la navegación privada">

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

refresh-profile-dialog =
    .title = { -brand-short-name } କୁ ସତେଜ କରନ୍ତୁ
refresh-profile-dialog-button =
    .label = { -brand-short-name } କୁ ସତେଜ କରନ୍ତୁ
refresh-profile-description = ସମସ୍ୟାଗୁଡ଼ିକର ସମାଧାନ ପାଇଁ ନୂଆ କରି ଆରମ୍ଭ କରନ୍ତୁ ଏବଂ କାର୍ଯ୍ୟକାରିତୀକୁ ସଂରକ୍ଷଣ କରନ୍ତୁ।
refresh-profile-description-details = ଏହା ହେବ:
refresh-profile-remove = ଆପଣଙ୍କର ଏଡ଼ଅନ୍‌ଏବଂ ଇଚ୍ଛାରୂପଣଗୁଡ଼ିକୁ ବାହାର କରନ୍ତୁ
refresh-profile-restore = ଆପଣଙ୍କର ବ୍ରାଉଜର ସେଟିଙ୍ଗକୁ ସେଗୁଡ଼ିକର ପୂର୍ବନିର୍ଦ୍ଧାରିତ ସ୍ଥାନରେ ପୁନଃ ସଂରକ୍ଷଣ କରନ୍ତୁ
refresh-profile = { -brand-short-name } କୁ ସନ୍ତୁଳନ କରନ୍ତୁ
refresh-profile-button = { -brand-short-name } କୁ ସତେଜ କରନ୍ତୁ…

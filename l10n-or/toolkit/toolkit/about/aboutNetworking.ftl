# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

title = ନେଟୱର୍କିଙ୍ଗ ବିଷୟରେ
warning = ଏହା ଅତ୍ୟଧିକ ପରୀକ୍ଷାମୂଳକ ଅଟେ। ବୟସ୍କ ବ୍ୟକ୍ତିଙ୍କ ନିରୀକ୍ଷଣ ବିନା ଏହାକୁ ବ୍ୟବହାର କରନ୍ତୁ ନାହିଁ।
show-next-time-checkbox = ଏହି ଚେତାବନୀକୁ ପରବର୍ତ୍ତି ସମୟରେ ଦର୍ଶାନ୍ତୁ
ok = ଠିକ ଅଛି
sockets = ସକେଟଗୁଡ଼ିକ
dns = DNS
websockets = ୱେବ ସକେଟଗୁଡ଼ିକ
refresh = ସତେଜ କରନ୍ତୁ
auto-refresh = ପ୍ରତ୍ୟେକ 3 ସେକେଣ୍ଡରେ ସ୍ୱୟଂଚାଳିତ ଭାବରେ ସତେଜ କରନ୍ତୁ
hostname = ହୋଷ୍ଟ ନାମ
port = ପୋର୍ଟ
ssl = SSL
active = ସକ୍ରିୟ
idle = ନିଷ୍କ୍ରିୟ ହୋଇଛି
host = ହୋଷ୍ଟ
tcp = TCP
sent = ପଠାଯାଇଛି
received = ପ୍ର୍ରାପ୍ତ ହେଲା
family = ପରିବାର
addresses = ଠିକଣାଗୁଡିକ
expires = ସମୟ ସମାପ୍ତି (ସେକେଣ୍ଡ)
messages-sent = ସନ୍ଦେଶଗୁଡ଼ିକୁ ପଠାସରିଛି
messages-received = ସନ୍ଦେଶଗୁଡ଼ିକୁ ପାଇଛି
bytes-sent = ପଠାଯାଇଥିବା ବାଇଟ
bytes-received = ପ୍ରାପ୍ତ ହୋଇଥିବା ବାଇଟ

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

window-close =
    .key = w
focus-search-shortcut =
    .key = f
focus-search-altshortcut =
    .key = k
copy-password-cmd =
    .label = ପ୍ରବେଶ ସଂକେତକୁ ନକଲ କରନ୍ତୁ
    .accesskey = C
column-heading-site =
    .label = ସାଇଟ
column-heading-username =
    .label = ଚାଳକ ନାମ
column-heading-password =
    .label = ପ୍ରବେଶ ସଂକେତ
column-heading-time-created =
    .label = ପ୍ରଥମ ଥର ବ୍ୟବହୃତ
column-heading-time-last-used =
    .label = ଶେଷ ଥର ବ୍ୟବହୃତ
column-heading-time-password-changed =
    .label = ଶେଷ ପରିବର୍ତ୍ତିନ
column-heading-times-used =
    .label = ଉପଯୋଗ ହୋଇଥିବା ସମୟ
remove =
    .label = କାଢ଼ନ୍ତୁ
    .accesskey = R
import =
    .label = ଆମଦାନୀ କରନ୍ତୁ…
    .accesskey = I
close-button =
    .label = ବନ୍ଦକରନ୍ତୁ
    .accesskey = C
remove-all-passwords-prompt = ଆପଣ ନିଶ୍ଚିତ କି ଆପଣ ଆପଣଙ୍କର ସମସ୍ତ ପ୍ରବେଶ ସଂକେତ ହଟାଇବାକୁ ଚାହୁଁଛନ୍ତି?
remove-all-passwords-title = ସମସ୍ତ ପ୍ରବେଶ ସଂକେତକୁ ହଟାନ୍ତୁ
no-master-password-prompt = ଆପଣ ନିଶ୍ଚିତ କି ଆପଣ ଆପଣଙ୍କର ପ୍ରବେଶ ସଂକେତ ଦର୍ଶାଇବାକୁ ଚାହୁଁଛନ୍ତି?

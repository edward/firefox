# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

customize-mode-restore-defaults =
    .label = ପୂର୍ବ ନିର୍ଦ୍ଧାରିତଗୁଡିକୁ ପୁନଃସ୍ଥାପନ କରନ୍ତୁ
customize-mode-lwthemes-menu-manage =
    .label = ପରିଚାଳନ କରନ୍ତୁ
    .accesskey = M
customize-mode-titlebar =
    .label = ଶୀର୍ଷକ ପଟି
customize-mode-lwthemes =
    .label = ପ୍ରସଙ୍ଗ
customize-mode-lwthemes-menu-get-more =
    .label = ଅଧିକ ପ୍ରସଙ୍ଗ ପ୍ରାପ୍ତ କରନ୍ତୁ
    .accesskey = G
customize-mode-undo-cmd =
    .label = ପଦକ୍ଷେପ ବାତିଲ କରନ୍ତୁ
customize-mode-lwthemes-my-themes =
    .value = ମୋର ପ୍ରସଙ୍ଗ

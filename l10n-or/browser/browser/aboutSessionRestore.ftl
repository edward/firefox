# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restore-page-tab-title = ଅଧିବେଶନକୁ ପୁନଃସ୍ଥାପନ କରନ୍ତୁ
# When tabs are distributed across multiple windows, this message is used as a
# header above the group of tabs for each window.
#
# Variables:
#    $windowNumber: Progressive number associated to each window
restore-page-window-label = ୱିଣ୍ଡୋ { $windowNumber }
restore-page-restore-header =
    .label = ପୁନଃସ୍ଥାପନ କରନ୍ତୁ
restore-page-list-header =
    .label = ୱିଣ୍ଡୋ ଏବଂ ଟ୍ୟାବଗୁଡ଼ିକ

## The following strings are used in about:welcomeback

welcome-back-tab-title = ସଫଳ!
welcome-back-page-title = ସଫଳ!
welcome-back-page-info = { -brand-short-name } ଯିବା ପାଇଁ ପ୍ରସ୍ତୁତ।
welcome-back-restore-button =
    .label = ଚାଲ ଯିବା!
    .accesskey = L
welcome-back-restore-some-label = ଯାହାକୁ ଆପଣ ଚାହୁଁଛନ୍ତି ତାକୁହିଁ କେବଳ ପୁନଃସଂରକ୍ଷିତ କରନ୍ତୁ
welcome-back-page-info-link = ଆପଣଙ୍କର ଏଡ-ଅନ୍‌ ଏବଂ ଇଚ୍ଛାରୂପଣକୁ କଢ଼ାଯାଇଛି ଏବଂ ଆପଣଙ୍କର ବ୍ରାଉଜର ସେଟିଙ୍ଗକୁ ପୂର୍ବନିର୍ଦ୍ଧାରିତ ଭାବରେ ପୁନଃସ୍ଥାପନ କରାଯାଇଛି। ଯଦି ଏହା ଆପଣଙ୍କର ସମସ୍ୟାର ସମାଧାନ ନକରେ, <a data-l10n-name="link-more">ଆପଣ କଣ କରିପାରିବେ ତାହା ବିଷୟରେ ଅଧିକ ଜାଣନ୍ତୁ।</a>

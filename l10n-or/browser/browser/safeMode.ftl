# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

safe-mode-window =
    .title = { -brand-short-name } ସୁରକ୍ଷିତ ଧାରା
    .style = max-width: 400px
start-safe-mode =
    .label = ସୁରକ୍ଷିତ ଧାରାରେ ଆରମ୍ଭ ହୋଇଥାଏ
refresh-profile =
    .label = { -brand-short-name } କୁ ସତେଜ କରନ୍ତୁ
safe-mode-description = ସୁରକ୍ଷିତ ଧାରା { -brand-short-name } ର ଏକ ବିଶେଷ ଧାରା ଯାହାକୁ ସମସ୍ୟାଗୁଡ଼ିକର ସମାଧାନ ପାଇଁ ବ୍ୟବହାର କରାଯାଇ ପାରିବ।
refresh-profile-instead = ଆପଣ ତୃଟିନିବାରଣକୁ ଏଡ଼ାଇ ଦେଇପାରିବେ ଏବଂ { -brand-short-name } କୁ ସତେଜ କରିବା ପାଇଁ ଚେଷ୍ଟା କରିପାରିବେ।
# Shown on the safe mode dialog after multiple startup crashes. 
auto-safe-mode-description = { -brand-short-name } ଆରମ୍ଭ ହେବା ସମୟରେ ଅପ୍ରତ୍ୟାଶିତ ଭାବରେ ବନ୍ଦ ହୋଇଛି। ଏହା ହୁଏତଃ ଏଡ-ଅନ କିମ୍ବା ଅନ୍ୟ ସମସ୍ୟା ହେତୁ ହୋଇପାରେ। ଆପଣ ସୁରକ୍ଷିତ ଧାରାରେ  ସମସ୍ୟାର ସମାଧାନ କରିବା ପାଇଁ ଚେଷ୍ଟାକରିପାରିବେ ।

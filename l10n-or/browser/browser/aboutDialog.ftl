# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

aboutDialog-title =
    .title = { -brand-full-name } ବିଷୟରେ
update-checkForUpdatesButton =
    .label = ଅପଡେଟସବୁ ଖୋଜନ୍ତୁ
    .accesskey = C
update-checkingForUpdates = ଅପଡେଟସବୁ ପରଖୁଛି…
update-applying = ଅପଡେଟଟି ପ୍ରୟୋଗ କରୁଛି…
update-failed = ଅପଡେଟ ବିଫଳ ।<label data-l10n-name="failed-link">ନୂତନତମ ସଂସ୍କରଣକୁ ଆହରଣ କରନ୍ତୁ</label>
update-failed-main = ଅପଡେଟ ବିଫଳ ।<a data-l10n-name="failed-link-main">ନୂତନତମ ସଂସ୍କରଣକୁ ଆହରଣ କରନ୍ତୁ</a>
update-adminDisabled = ଆପଣଙ୍କ ସିଷ୍ଟମ ପରିଚାଳକଙ୍କ ଦ୍ୱାରା ନିଷ୍କ୍ରିୟ କରାଯାଇଥିବା ଅପଡେଟସବୁ
update-noUpdatesFound = { -brand-short-name } ଟି ଅଦ୍ୟତିତ ଅଛି
update-otherInstanceHandlingUpdates = { -brand-short-name } ଅନ୍ୟ ଏକ ସ୍ଥିତିରେ ଅଦ୍ୟତିତ ହୋଇଛି
update-manual = ଅପଡେଟସବୁ ଏଠାରେ ପାଇବେ:<label data-l10n-name="manual-link"/>
update-unsupported = ଏହି ସିଷ୍ଟମରେ ଆଉ ଅପଡେଟ କରିପାରିବେ ନାହିଁ ।<label data-l10n-name="unsupported-link">ଅଧିକ ଜ୍ଞାନ ଆହରଣ କରନ୍ତୁ</label>
channel-description = ଆପଣ ବର୍ତ୍ତମାନ  ଏଠାରେ ଅଛନ୍ତି<label data-l10n-name="current-channel"></label>ଚ୍ୟାନେଲ ଅପଡେଟ କରନ୍ତୁ ।
warningDesc-version = { -brand-short-name } ଟି ପରୀକ୍ଷାମୂଳକ ସ୍ଥିତିରେ ଅଛି ଏବଂ ହୁଏତଃ ଅସ୍ଥାୟୀ ହୋଇଥିବ।
community-exp = <label data-l10n-name="community-exp-mozillaLink">{ -vendor-short-name }</label> ଏକ ଅଟେ <label data-l10n-name="community-exp-creditsLink">ବିଶ୍ୱବ୍ୟାପି ସମ୍ପ୍ରଦାୟ</label> ୱେବକୁ ମୁକ୍ତ, ସର୍ବସାଧାରଣ ଏବଂ ସମସ୍ତଙ୍କ ପାଇଁ ଅଭିଗମ୍ୟ ରଖିବା ପାଇଁ ଏକା ସହିତ କାମ କରୁଅଛୁ।
community-2 = { -brand-short-name } ଟି ଏହା ଦ୍ୱାରା ରଚିତ <label data-l10n-name="community-mozillaLink">{ -vendor-short-name }</label>, a <label data-l10n-name="community-creditsLink">ବିଶ୍ୱ ସମ୍ପ୍ରଦାୟ</label> ୱେବକୁ ମୁକ୍ତ, ସର୍ବସାଧାରଣ ଏବଂ ସମସ୍ତଙ୍କ ପାଇଁ ଅଭିଗମ୍ୟ କରିବା ପାଇଁ ଏକା ସହିତ ମିଶି କାମ କରୁଅଛୁ।
bottomLinks-license = ଅନୁମତି ପତ୍ର ସୂଚନା
bottomLinks-rights = ଚାଳକ ସୂଚନା
bottomLinks-privacy = ଗୋପନୀୟତା ନିତୀ

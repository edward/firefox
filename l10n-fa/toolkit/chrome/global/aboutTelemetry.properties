# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE(pageSubtitle):
# - %1$S is replaced by the value of the toolkit.telemetry.server_owner preference
# - %2$S is replaced by brandFullName
pageSubtitle = این صفحه اطلاعاتی درباره کارایی، سخت‌افزار، استفاده و سفارشی‌سازی‌های جمع‌آوری شده توسط مسافت‌سنج را نشان می‌دهد. این اطلاعات در %1$S ثبت شده است تا در بهبود %2$S کمک کند.

# LOCALIZATION NOTE(homeExplanation):
# - %1$S is either telemetryEnabled or telemetryDisabled
# - %2$S is either extendedTelemetryEnabled or extendedTelemetryDisabled
homeExplanation = دورسنجی %1$S است و دورسنجی گسترش یافته %2$S است.
telemetryEnabled = فعال
telemetryDisabled = غیرفعال
extendedTelemetryEnabled = فعال
extendedTelemetryDisabled = غیرفعال

# LOCALIZATION NOTE(settingsExplanation):
# - %1$S is either releaseData or prereleaseData
# - %2$S is either telemetryUploadEnabled or telemetryUploadDisabled
settingsExplanation = سنجش از راه دور %1$S را ذخیره می‌کند و %2$S بارگذاری می‌کند.
releaseData = انتشار اطلاعات
prereleaseData = قبل از انتشار اطلاعات
telemetryUploadEnabled = فعال شد
telemetryUploadDisabled = غیرفعال شد

# LOCALIZATION NOTE(pingDetails):
# - %1$S is replaced by a link with pingExplanationLink as text
# - %2$S is replaced by namedPing
pingDetails = هر قطعه از این اطلاعات به همراه“%1$S” ارسال شده است. شما در حال نگاه کردن به پینگ %2$S هستید.
# LOCALIZATION NOTE(namedPing):
# - %1$S is replaced by the ping localized timestamp, e.g. “2017/07/08 10:40:46”
# - %2$S is replaced by the ping name, e.g. “saved-session”
namedPing = %1$S, %2$S
# LOCALIZATION NOTE(pingDetailsCurrent):
# - %1$S is replaced by a link with pingExplanationLink as text
# - %2$S is replaced by currentPing
pingDetailsCurrent = هر قطعه از این اطلاعات به همراه“%1$S” ارسال شده است. شما در حال نگاه کردن به پینگ %2$S هستید.
pingExplanationLink = پینگ‌ها
currentPing = جاری

# LOCALIZATION NOTE(filterPlaceholder): string used as a placeholder for the
# search field, %1$S is replaced by the section name from the structure of the
# ping. More info about it can be found here:
# https://firefox-source-docs.mozilla.org/toolkit/components/telemetry/telemetry/data/main-ping.html
filterPlaceholder = پیدا کردن در %1$S
filterAllPlaceholder = یافتن تمام بخش‌ها

# LOCALIZATION NOTE(resultsForSearch): %1$S is replaced by the searched terms
resultsForSearch = نتایج برای “%1$S”
# LOCALIZATION NOTE(noSearchResults):
# - %1$S is replaced by the section name from the structure of the ping.
# More info about it can be found here: https://firefox-source-docs.mozilla.org/toolkit/components/telemetry/telemetry/data/main-ping.html
# - %2$S is replaced by the current text in the search input
noSearchResults = متاسفیم! ولی نتیجه ای در%1$S برای “%2$S” وجود ندارد
# LOCALIZATION NOTE(noSearchResultsAll): %S is replaced by the searched terms
noSearchResultsAll = متاسفیم! نتیجه ای برای بخش “%S” پیدا نشد
# LOCALIZATION NOTE(noDataToDisplay): %S is replaced by the section name.
# This message is displayed when a section is empty.
noDataToDisplay = متاسفیم! ولی در حال حاضر اطلاعاتی برای “%S” در دسترس نیست
# LOCALIZATION NOTE(currentPingSidebar): used as a tooltip for the “current”
# ping title in the sidebar
currentPingSidebar = پینگ جاری
# LOCALIZATION NOTE(telemetryPingTypeAll): used in the “Ping Type” select
telemetryPingTypeAll = همه

# LOCALIZATION NOTE(histogram*): these strings are used in the “Histograms” section
histogramSamples = نمونه‌ها
histogramAverage = میانگین
histogramSum = جمع
# LOCALIZATION NOTE(histogramCopy): button label to copy the histogram
histogramCopy = رونوشت برداشتن

# LOCALIZATION NOTE(telemetryLog*): these strings are used in the “Telemetry Log” section
telemetryLogTitle = وقایع مسافت‌سنج
telemetryLogHeadingId = شناسه
telemetryLogHeadingTimestamp = زمان
telemetryLogHeadingData = اطلاعات

# LOCALIZATION NOTE(slowSql*): these strings are used in the “Slow SQL Statements” section
slowSqlMain = عبارت‌های SQL کُند در ترد اصلی
slowSqlOther = عبارت‌های SQL کُند در تردهای کمکی
slowSqlHits = بازدیدها
slowSqlAverage = زمان میانگین (ms)
slowSqlStatement = عبارت

# LOCALIZATION NOTE(histogram*): these strings are used in the “Add-on Details” section
addonTableID = شناسه افزونه
addonTableDetails = جزئیات
# LOCALIZATION NOTE(addonProvider):
# - %1$S is replaced by the name of an Add-on Provider (e.g. “XPI”, “Plugin”)
addonProvider = فراهم‌کننده %1$S

keysHeader = ویژگی
namesHeader = نام
valuesHeader = مقدار

# LOCALIZATION NOTE(chrome-hangs-title):
# - %1$S is replaced by the number of the hang
# - %2$S is replaced by the duration of the hang
chrome-hangs-title = گزارش‌های توقف #%1$S (%2$S ثانیه)
# LOCALIZATION NOTE(captured-stacks-title):
# - %1$S is replaced by the string key for this stack
# - %2$S is replaced by the number of times this stack was captured
captured-stacks-title = %1$S (تعداد دریافت‌ها: %2$S)
# LOCALIZATION NOTE(late-writes-title):
# - %1$S is replaced by the number of the late write
late-writes-title = دیرنویس #%1$S

stackTitle = پشته:
memoryMapTitle = نقشه حافظه:

errorFetchingSymbols = خطایی هنگام دریافت علائم رخ داد. لطفا اتصال خود را به اینترنت بررسی و مجددا تلاش کنید.

parentPayload = ظرفیت والد
# LOCALIZATION NOTE(childPayloadN):
# - %1$S is replaced by the number of the child payload (e.g. “1”, “2”)
childPayloadN = فرزند محموله %1$S
timestampHeader = مهر زمان
categoryHeader = دسته بندی
methodHeader = روش
objectHeader = شئ
extraHeader = اضافی

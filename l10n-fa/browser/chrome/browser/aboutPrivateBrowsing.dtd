<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY aboutPrivateBrowsing.notPrivate                 "شما در حال حاضر در پنجره‌ی ناشناس قرار ندارید.">
<!ENTITY privatebrowsingpage.openPrivateWindow.label     "یک پنجره‌ی ناشناس باز کن">
<!ENTITY privatebrowsingpage.openPrivateWindow.accesskey "ن">

<!ENTITY privateBrowsing.title                           "مرور ناشناس">
<!ENTITY aboutPrivateBrowsing.info.notsaved.before       "هنگامی که شما در یک پنجره ناشناس شروع به مرور وب می‌کنید، فایرفاکس موارد زیر را ">
<!ENTITY aboutPrivateBrowsing.info.notsaved.emphasize    "ذخیره نمی‌کند">
<!ENTITY aboutPrivateBrowsing.info.notsaved.after        ":">
<!ENTITY aboutPrivateBrowsing.info.visited               "صفحات بازدید شده">
<!ENTITY aboutPrivateBrowsing.info.searches              "جست‌وجوها">
<!ENTITY aboutPrivateBrowsing.info.cookies               "کوکی‌ها">
<!ENTITY aboutPrivateBrowsing.info.temporaryFiles        "پرونده‌های موقتی">
<!ENTITY aboutPrivateBrowsing.info.saved.before          "فایرفاکس ">
<!ENTITY aboutPrivateBrowsing.info.saved.emphasize       "اطلاعات زیر را ذخیره خواهد کرد">
<!ENTITY aboutPrivateBrowsing.info.saved.after2          ":">
<!ENTITY aboutPrivateBrowsing.info.downloads             "بارگیری‌ها">
<!ENTITY aboutPrivateBrowsing.info.bookmarks             "نشانک‌ها">
<!ENTITY aboutPrivateBrowsing.info.clipboard             "متن رونوشت شده">
<!ENTITY aboutPrivateBrowsing.note.before                "مرور ناشناس ">
<!ENTITY aboutPrivateBrowsing.note.emphasize             "شما را در اینترنت ناشناس نمی‌کند.">
<!ENTITY aboutPrivateBrowsing.note.after                 " کارفرما یا ارائه‌دهنده خدمات اینترنتی شما همچنان می‌تواند از صفحاتی که بازدید می‌کنید اطلاع داشته باشد.">
<!ENTITY aboutPrivateBrowsing.learnMore3.before          "در مورد‌ آن بیشتر بدانید">
<!ENTITY aboutPrivateBrowsing.learnMore3.title           "مرور خصوصی">
<!ENTITY aboutPrivateBrowsing.learnMore3.after           ".">

<!ENTITY trackingProtection.startTour1                   "ببینید چگونه کار می‌کند">

<!ENTITY contentBlocking.title                           "مسدود کردن محتوا">
<!ENTITY contentBlocking.description                     "برخی وب‌سایت‌ها از ردیاب‌هایی استفاده می‌کنند که می‌توانند بر فعالیت‌های شما در تمام اینترنت نظارت کنند. در پنجره ناشناس، مسدود کردن محتوای فایرفاکس، به صورت خودکار ردیاب‌های بسیاری که می‌توانند درمورد نحوه‌ی مرور شما اطلاعات جمع‌آوری کنند را مسدود می‌کند.">

<!-- Strings for new Private Browsing with Search -->
<!-- LOCALIZATION NOTE (aboutPrivateBrowsing.search.placeholder): This is the placeholder
                       text for the search box.   -->
<!ENTITY aboutPrivateBrowsing.search.placeholder         "وب را جست‌وجو کنید">
<!ENTITY aboutPrivateBrowsing.info.title                 "شما در یک پنجرهٔ ناشناس هستید">
<!ENTITY aboutPrivateBrowsing.info.description           "&brandShortName; جست‌وجوها و تاریخچه مرور شما را هنگام خروج از برنامه یا بستن تمام پنجره‌ها و زبانه‌های ناشناس پاک می‌کند. در حالی که اینکار شما را در وب‌سایت‌ها یا فراهم‌کنندهٔ اینترنت شما ناشناس نمی‌کند، این کار مخفی کردن فعالیت‌های آنلاین شما را برای هرکس دیگری که از این رایانه استفاده می‌کند را ساده‌تر می‌کند.">
<!-- LOCALIZATION NOTE (aboutPrivateBrowsing.info.myths): This is a link to a SUMO article
                       about private browsing myths.   -->
<!ENTITY aboutPrivateBrowsing.info.myths                 "افسانه‌های متداول در مورد مرور ناشناس">

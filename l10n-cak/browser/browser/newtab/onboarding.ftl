# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-learn-more = Tetamäx ch'aqa' chik
onboarding-button-label-try-now = Tatojtob'ej Wakami
onboarding-button-label-get-started = Titikirisäx
onboarding-welcome-header = Ütz apetik pa { -brand-short-name }
onboarding-welcome-body = K'o awik'in ri okik'amaya'l.<br/>Tawetamaj ri ch'aqa' chik taq { -brand-product-name }.
onboarding-welcome-learn-more = Tawetamaj ch'aqa' chik pa ruwi' ri taq rutzil.
onboarding-join-form-header = Tatunu' awi' rik'in { -brand-product-name }
onboarding-join-form-body = Richin natikirisaj, tatz'ib'aj ri rochochib'al ataqoya'l.
onboarding-join-form-email =
    .placeholder = Titz'ib'äx taqoya'l
onboarding-join-form-email-error = Ütz taqoya'l najowäx
onboarding-join-form-legal = Pa rub'eyal, rat nawojqaj ri <a data-l10n-name="terms">Rojqanem Samaj</a> chuqa' <a data-l10n-name="privacy">Rutzijol Ichinanem</a>.
onboarding-join-form-continue = Titikïr chik el
onboarding-start-browsing-button-label = Tichap Okem Pa K'amaya'l

## These are individual benefit messages shown with an image, title and
## description.

onboarding-benefit-products-title = Okel taq Tikojil
onboarding-benefit-products-text = Kasamäj rik'in jun molaj samajib'äl, ri nukamelaj ri awichinanem pa ronojel awokisab'al.

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = Ichinan Okem pa K'amaya'l
onboarding-private-browsing-text = Ayon katok pa k'amaya'l. Ichinan Okem pa K'amaya'l rik'in Q'aton Rupam K'amab'ey, yeruq'ät ojqanela' yatkitzeqelb'ej pan ajk'amaya'l.
onboarding-screenshots-title = Chapoj taq ruwäch
onboarding-screenshots-text = Tichap, tiyak chuqa' tikomonïx ri chapoj ruwäch - akuchi' man yatel ta { -brand-short-name }. Tachapa' jun peraj o chijun ri ruxaq toq atokinäq pa k'amaya'l. K'a ri tayaka' pa ajk'amaya'l richin man k'ayew ta yatok chi kipam o ye'akomonij.
onboarding-addons-title = Taq tz'aqat
onboarding-addons-text = Ke'atz'aqatisaj ch'aqa' chik taq samaj richin nib'an chi ri { -brand-short-name } anin nisamäj chawäch. Ke'ajunamaj taq ajil, tatz'eta' ri meq'tewal o tak'utu' ri ab'anikil rik'in jun ichinan wachinel.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Katok pa k'amaya'l aninäq, na'owinäq rub'anikil o yalan ütz kik'in taq tz'aqat achi'el Ghostery, ri nuya' q'ij chawe richin ye'aq'ät ri itzel taq rutzijol.
# Note: "Sync" in this case is a generic verb, as in "to synchronize"
onboarding-fxa-title = Tixim
onboarding-fxa-text = Tatz'ib'aj awi' richin nik'oje' jun { -fxaccount-brand-name } chuqa' ke'axima' ri taq ayaketal, ewan taq tzij chuqa' jaqäl taq ruwi' akuchi' nawokisaj { -brand-short-name }.

## Message strings belonging to the Return to AMO flow

return-to-amo-sub-header = Ütz ütz, awichinan { -brand-short-name }
# <icon></icon> will be replaced with the icon belonging to the extension
#
# Variables:
#   $addon-name (String) - Name of the add-on
return-to-amo-addon-header = Wakami niqatäq chawe <icon></icon><b>{ $addon-name }.</b>
return-to-amo-extension-button = Titz'aqatisäx jun K'amal
return-to-amo-get-started-button = Titikirisäx rik'in { -brand-short-name }

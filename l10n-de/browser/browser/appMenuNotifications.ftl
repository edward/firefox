# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

appmenu-update-available =
    .label = Es ist ein neues { -brand-shorter-name }-Update verfügbar.
    .buttonlabel = Update herunterladen
    .buttonaccesskey = h
    .secondarybuttonlabel = Nicht jetzt
    .secondarybuttonaccesskey = N
appmenu-update-available-message = Aktualisieren Sie { -brand-shorter-name } für die neuesten Verbesserungen in Sachen Geschwindigkeit und Datenschutz.
appmenu-update-manual =
    .label = { -brand-shorter-name } kann nicht auf die neueste Version aktualisieren.
    .buttonlabel = { -brand-shorter-name } herunterladen
    .buttonaccesskey = h
    .secondarybuttonlabel = Nicht jetzt
    .secondarybuttonaccesskey = N
appmenu-update-manual-message = Laden Sie sich die aktuelle Version von { -brand-shorter-name } herunter und wir helfen Ihnen bei der Installation.
appmenu-update-whats-new =
    .value = Die Änderungen im Detail.
appmenu-update-restart =
    .label = Zum Aktualisieren von { -brand-shorter-name } neu starten
    .buttonlabel = Neu starten und wiederherstellen
    .buttonaccesskey = w
    .secondarybuttonlabel = Nicht jetzt
    .secondarybuttonaccesskey = N
appmenu-update-restart-message = Nach einem schnellen Neustart des Browsers wird { -brand-shorter-name } alle offenen Tabs und Fenster wiederherstellen, die sich nicht im Privaten Modus befinden.
appmenu-addon-post-install-message = Die Add-ons lassen sich durch Klicken auf <image data-l10n-name='addon-install-icon'></image> im <image data-l10n-name='addon-menu-icon'></image>-Menü verwalten.

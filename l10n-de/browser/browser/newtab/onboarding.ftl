# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = Jetzt ausprobieren
onboarding-button-label-get-started = Einführung
onboarding-welcome-header = Willkommen bei { -brand-short-name }
onboarding-start-browsing-button-label = Lossurfen

## These strings belong to the individual onboarding messages.

## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section
onboarding-private-browsing-title = Privates Surfen
onboarding-private-browsing-text = Im Netz ohne neugierige Blicke anderer dank privatem Surfen mit Blockierung von Seitenelementen, welche Nutzeraktitvitäten über verschiedene Websites hinweg verfolgen wollen.

onboarding-screenshots-title = Bildschirmfotos
onboarding-screenshots-text = Bildschirmfotos aufnehmen, speichern und teilen - ohne { -brand-short-name } zu verlassen. Nehmen Sie Seitenteile oder komplette Webseiten auf und speichern Sie diese anschließend im Internet, um schnell darauf zuzugreifen oder sie zu teilen.

onboarding-addons-title = Add-ons
onboarding-addons-text = Fügen Sie noch mehr Funktionen zu { -brand-short-name } hinzu, damit verschiedene Aufgaben einfacher von Hand gehen. Vergleichen Sie Preise, erhalten Sie Informationen zum Wetter oder drücken Sie Ihre Individualität mit einem eigenen Theme aus.

onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Surfen Sie schneller, eleganter und sicherer mit Erweiterungen wie z.B. Ghostery, welche lästige Werbung für Sie blockiert.

# Note: "Sync" in this case is a generic verb, as in "to synchronize"
onboarding-fxa-title = Synchronisieren
onboarding-fxa-text = Melden Sie sich bei { -fxaccount-brand-name } an und synchronisieren Sie Ihre Lesezeichen, Passwörter und offenen Tabs mit allen Geräten, auf denen Sie { -brand-short-name } verwenden.

## Message strings belonging to the Return to AMO flow
return-to-amo-sub-header = Exzellent, Sie haben { -brand-short-name }

# <icon></icon> will be replaced with the icon belonging to the extension
#
# Variables:
#   $addon-name (String) - Name of the add-on
return-to-amo-addon-header = Installieren Sie jetzt <icon></icon><b>{ $addon-name }.</b>
return-to-amo-extension-button = Erweiterung installieren
return-to-amo-get-started-button = Informationen zu { -brand-short-name }

<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY aboutPrivateBrowsing.notPrivate                 "Du är nu inte i ett privat fönster.">
<!ENTITY privatebrowsingpage.openPrivateWindow.label     "Öppna ett privat fönster">
<!ENTITY privatebrowsingpage.openPrivateWindow.accesskey "p">

<!ENTITY privateBrowsing.title                           "Privat surfning">
<!ENTITY privateBrowsing.title.tracking                  "Privat surfning med spårningsskydd">
<!ENTITY aboutPrivateBrowsing.info.notsaved.before       "När du surfar i ett privat fönster, ">
<!ENTITY aboutPrivateBrowsing.info.notsaved.emphasize    "sparar Firefox inte">
<!ENTITY aboutPrivateBrowsing.info.notsaved.after        ":">
<!ENTITY aboutPrivateBrowsing.info.visited               "besökta sidor">
<!ENTITY aboutPrivateBrowsing.info.searches              "sökningar">
<!ENTITY aboutPrivateBrowsing.info.cookies               "kakor">
<!ENTITY aboutPrivateBrowsing.info.temporaryFiles        "tillfälliga filer">
<!ENTITY aboutPrivateBrowsing.info.saved.before          "Firefox ">
<!ENTITY aboutPrivateBrowsing.info.saved.emphasize       "kommer att spara">
<!ENTITY aboutPrivateBrowsing.info.saved.after2          " dina:">
<!ENTITY aboutPrivateBrowsing.info.downloads             "hämtningar">
<!ENTITY aboutPrivateBrowsing.info.bookmarks             "bokmärken">
<!ENTITY aboutPrivateBrowsing.info.clipboard             "kopierad text">
<!ENTITY aboutPrivateBrowsing.note.before                "Privat surfning ">
<!ENTITY aboutPrivateBrowsing.note.emphasize             "gör dig inte anonym">
<!ENTITY aboutPrivateBrowsing.note.after                 " på internet. Din arbetsgivare eller internetleverantör kan fortfarande spåra sidorna du besöker.">
<!ENTITY aboutPrivateBrowsing.learnMore3.before          "Läs mer om ">
<!ENTITY aboutPrivateBrowsing.learnMore3.title           "Privat surfning">
<!ENTITY aboutPrivateBrowsing.learnMore3.after           ".">

<!ENTITY trackingProtection.title                        "Spårningsskydd">
<!ENTITY trackingProtection.description2                 "Vissa webbplatser använder spårare som kan övervaka din aktivitet över hela internet. Med spårningsskydd kommer Firefox blockera många spårare som kan samla information om dina surfvanor.">

<!ENTITY trackingProtection.startTour1                   "Se hur det fungerar">

<!ENTITY contentBlocking.title                           "Innehållsblockering">
<!ENTITY contentBlocking.description                     "Vissa webbplatser använder trackers som kan övervaka din aktivitet på internet. I privata fönster blockerar Firefox-innehållsblockering automatiskt många trackers som kan samla information om ditt webbläsarbeteende.">

<!-- Strings for new Private Browsing with Search -->
<!-- LOCALIZATION NOTE (aboutPrivateBrowsing.search.placeholder): This is the placeholder
                       text for the search box.   -->
<!ENTITY aboutPrivateBrowsing.search.placeholder         "Sök på nätet">
<!ENTITY aboutPrivateBrowsing.info.title                 "Du är i ett privat fönster">
<!ENTITY aboutPrivateBrowsing.info.description           "&brandShortName; rensar din sök- och surfhistorik när du avslutar appen eller stänger alla privata flikar och fönster. Även om det här inte gör dig anonym för webbplatser eller din internetleverantör, gör det det lättare att behålla det du gör online privat från någon annan som använder den här datorn.">
<!-- LOCALIZATION NOTE (aboutPrivateBrowsing.info.myths): This is a link to a SUMO article
                       about private browsing myths.   -->
<!ENTITY aboutPrivateBrowsing.info.myths                 "Vanliga myter om privat surfning">

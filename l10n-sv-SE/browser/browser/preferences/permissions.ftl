# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

permissions-window =
    .title = Undantag
    .style = width: 45em
permissions-close-key =
    .key = w
permissions-address = Webbplatsens adress
    .accesskey = d
permissions-block =
    .label = Blockera
    .accesskey = B
permissions-session =
    .label = Tillåt för sessionen
    .accesskey = s
permissions-allow =
    .label = Tillåt
    .accesskey = å
permissions-site-name =
    .label = Webbplats
permissions-status =
    .label = Status
permissions-remove =
    .label = Ta bort webbplats
    .accesskey = T
permissions-remove-all =
    .label = Ta bort alla webbplatser
    .accesskey = a
permissions-button-cancel =
    .label = Avbryt
    .accesskey = A
permissions-button-ok =
    .label = Spara ändringar
    .accesskey = S
permissions-searchbox =
    .placeholder = Sök webbplats
permissions-capabilities-allow =
    .label = Tillåt
permissions-capabilities-block =
    .label = Blockera
permissions-capabilities-prompt =
    .label = Fråga alltid
permissions-capabilities-listitem-allow =
    .value = Tillåt
permissions-capabilities-listitem-block =
    .value = Blockera
permissions-capabilities-listitem-allow-first-party =
    .value = Tillåt endast första parten
permissions-capabilities-listitem-allow-session =
    .value = Tillåten för session

## Invalid Hostname Dialog

permissions-invalid-uri-title = Ogiltigt värdnamn
permissions-invalid-uri-label = Skriv in ett giltigt värdnamn

## Exceptions - Tracking Protection

permissions-exceptions-tracking-protection-window =
    .title = Undantag - Spårningsskydd
    .style = { permissions-window.style }
permissions-exceptions-tracking-protection-desc = Du har stängt av spårningsskydd på dessa webbplatser.
permissions-exceptions-content-blocking-window =
    .title = Undantag - Innehållsblockering
    .style = { permissions-window.style }
permissions-exceptions-content-blocking-desc = Du har inaktiverat innehållsblockering på dessa webbplatser.

## Exceptions - Cookies

permissions-exceptions-cookie-window =
    .title = Undantag - Kakor och webbplatsdata
    .style = { permissions-window.style }
permissions-exceptions-cookie-desc = Du kan ange vilka webbplatser som alltid eller aldrig får använda kakor och webbplatsdata.  Skriv den exakta adressen till den webbplats du vill hantera och klicka sedan på Blockera, Tillåt för sessionen eller Tillåt.

## Exceptions - Pop-ups

permissions-exceptions-popup-window =
    .title = Tillåtna webbplatser - popup-fönster
    .style = { permissions-window.style }
permissions-exceptions-popup-desc = Du kan ange vilka webbplatser som får öppna popup-fönster. Skriv in adressen till platsen du vill godkänna och klicka på Tillåt.

## Exceptions - Saved Logins

permissions-exceptions-saved-logins-window =
    .title = Undantag - Sparade inloggningar
    .style = { permissions-window.style }
permissions-exceptions-saved-logins-desc = Inloggningar för följande webbplatser kommer inte att sparas

## Exceptions - Add-ons

permissions-exceptions-addons-window =
    .title = Tillåtna webbplatser - Installation av tillägg
    .style = { permissions-window.style }
permissions-exceptions-addons-desc = Du kan ange vilka webbplatser som får installera tillägg. Skriv in adressen till platsen du vill godkänna och klicka på Tillåt.

## Exceptions - Autoplay Media

permissions-exceptions-autoplay-media-window2 =
    .title = Undantag - Automatisk uppspelning
    .style = { permissions-window.style }
permissions-exceptions-autoplay-media-desc2 = Du kan specificera vilka webbplatser som alltid eller aldrig tillåts spela media med ljud automatiskt. Skriv in adressen till platsen du vill hantera och klicka sedan på Błockera eller Tillåt.

## Site Permissions - Notifications

permissions-site-notification-window =
    .title = Inställningar - Behörigheter för aviseringar
    .style = { permissions-window.style }
permissions-site-notification-desc = Följande webbplatser har begärt att skicka meddelanden. Du kan ange vilka webbplatser som får skicka aviseringar. Du kan också blockera nya förfrågningar om att tillåta meddelanden.
permissions-site-notification-disable-label =
    .label = Blockera nya förfrågningar om att tillåta meddelanden
permissions-site-notification-disable-desc = Detta kommer att förhindra att webbplatser som inte listas ovan från att begära tillstånd att skicka meddelanden. Blockering av meddelanden kan störa vissa webbplatsfunktioner.

## Site Permissions - Location

permissions-site-location-window =
    .title = Inställningar - Behörigheter för plats
    .style = { permissions-window.style }
permissions-site-location-desc = Följande webbplatser har begärt att komma åt din position. Du kan ange vilka webbplatser som får komma åt din position. Du kan också blockera nya förfrågningar om att få tillgång till din position.
permissions-site-location-disable-label =
    .label = Blockera nya förfrågningar om att få tillgång till din position
permissions-site-location-disable-desc = Detta kommer att förhindra att webbplatser som inte listas ovan från att begära tillstånd att komma åt din position. Om du blockerar åtkomst till din position kan det störa vissa webbplatsfunktioner.

## Site Permissions - Camera

permissions-site-camera-window =
    .title = Inställningar - Behörigheter för kamera
    .style = { permissions-window.style }
permissions-site-camera-desc = Följande webbplatser har begärt att du ska komma åt din kamera. Du kan ange vilka webbplatser som får komma åt din kamera. Du kan också blockera nya förfrågningar om att komma åt din kamera.
permissions-site-camera-disable-label =
    .label = Blockera nya förfrågningar om att få tillgång till din kamera
permissions-site-camera-disable-desc = Detta kommer att förhindra att webbplatser som inte listas ovan från att begära tillstånd att komma åt din kamera. Att blockera åtkomst till din kamera kan störa vissa webbplatsfunktioner.

## Site Permissions - Microphone

permissions-site-microphone-window =
    .title = Inställningar - Behörigheter för mikrofon
    .style = { permissions-window.style }
permissions-site-microphone-desc = Följande webbplatser har begärt att du ska komma åt din mikrofon. Du kan ange vilka webbplatser som får komma åt din mikrofon. Du kan också blockera nya förfrågningar om att komma åt din mikrofon.
permissions-site-microphone-disable-label =
    .label = Blockera nya förfrågningar om att få tillgång till din mikrofon
permissions-site-microphone-disable-desc = Detta kommer att förhindra att webbplatser som inte listas ovan från att begära tillstånd att komma åt din mikrofon. Om du blockerar åtkomst till din mikrofon kan det störa vissa webbplatsfunktioner.

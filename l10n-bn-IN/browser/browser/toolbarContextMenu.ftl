# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

toolbar-context-menu-reload-selected-tab =
    .label = নির্বাচিত ট্যাবটিকে পুনরায় লোড করুন
    .accesskey = R
toolbar-context-menu-reload-selected-tabs =
    .label = নির্বাচিত ট্যাবগুলিকে পুনরায় লোড করুন
    .accesskey = R
toolbar-context-menu-bookmark-selected-tab =
    .label = নির্বাচিত ট্যাবটিকে বুকমার্ক করুন…
    .accesskey = T
toolbar-context-menu-bookmark-selected-tabs =
    .label = নির্বাচিত ট্যাবগুলোকে বুকমার্ক করুন…
    .accesskey = T
toolbar-context-menu-select-all-tabs =
    .label = সব ট্যাব নির্বাচন করুন
    .accesskey = S
toolbar-context-menu-undo-close-tab =
    .label = বন্ধ করা ট্যাব পুনরায় খুলুন
    .accesskey = U

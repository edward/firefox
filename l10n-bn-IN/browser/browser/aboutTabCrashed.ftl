# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

crashed-title = ট্যাব ক্র্যাশ প্রতিবেদক
crashed-close-tab-button = ট্যাব বন্ধ করুন
crashed-restore-tab-button = এই ট্যাবটি পুনরায় স্টোর করুন
crashed-restore-all-button = সমস্ত ক্র্যাশ হয়ে যাওয়া ট্যাবটি পুনরায় স্টোর করুন
crashed-header = গাহ। আপনার ট্যাব এইমাত্র ক্র্যাশ করল।
crashed-offer-help = আমরা সাহায্য করতে পারি!
crashed-single-offer-help-message = পেজের বিষয়বস্তু রিলোড করতে { crashed-restore-tab-button } নির্বাচন করুন।
crashed-multiple-offer-help-message = { crashed-restore-tab-button } অথবা { crashed-restore-all-button } নির্বাচন করুন পেজ//pages. রিলোড করতে।
crashed-request-help = আপনি কি আমাদের সাহায্য করবেন?
crashed-request-help-message = ক্র্যাশ রিপোর্ট আমাদের সমস্যার সমাধান করতে সাহায্য করে এবং { -brand-short-name } কে আরও ভালো বানায়।
crashed-request-report-title = এই ট্যাবটি রিপোর্ট করুন
crashed-send-report = একটি স্বয়ংক্রিয় প্রতিবেদন পাঠান যাতে আমরা এই ধরনের সমস্যার সমাধান করতে পারি।
crashed-comment =
    .placeholder = ঐচ্ছিক মন্তব্য (মন্তব্য সর্বজনীনভাবে দৃশ্যমান থাকে)
crashed-include-URL = { -brand-short-name } ক্র্যাশ করার সময় আপনি যেই ওয়েবসাইটে ছিলেন তার URLs যুক্ত করুন।
crashed-email-placeholder = ই-মেইল ঠিকানা এইখানে লিখুন
crashed-email-me = আরো সংবাদ উপলব্ধ হলে আমাকে ইমেল করুন
crashed-report-sent = ক্র্যাশ রিপোর্ট ইতিমধ্যে জমা হয়ে গেছে; ধন্যবাদ { -brand-short-name } বানাতে সাহায্য করবার জন্য এবং আরও ভালো করবার জন্য!
crashed-request-auto-submit-title = ব্যাকগ্রাউন্ড ট্যাবের রিপোর্ট করুন
crashed-auto-submit-checkbox = যখন { -brand-short-name } ক্রাস করে তখন রিপোর্টগুলি স্বয়ংক্রিয়ভাবে জমা দেওয়ার জন্য পছন্দগুলি আপডেট করুন।

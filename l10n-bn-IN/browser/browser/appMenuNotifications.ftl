# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

appmenu-update-available =
    .label = একটি নতুন { -brand-shorter-name } আপডেট উপলব্ধ আছে।
    .buttonlabel = আপডেট ডাউনলোড করুন
    .buttonaccesskey = D
    .secondarybuttonlabel = এখন নয়
    .secondarybuttonaccesskey = N
appmenu-update-available-message = { -brand-shorter-name } আপডেট করুন; সর্বশেষ গতি এবং গোপনীয়তার জন্য।
appmenu-update-manual =
    .label = { -brand-shorter-name } সর্বশেষ সংস্করণ আপডেট করতে পারবেন না।
    .buttonlabel = { -brand-shorter-name } ডাউনলোড করুন
    .buttonaccesskey = D
    .secondarybuttonlabel = এই মুহূর্তে নয়
    .secondarybuttonaccesskey = N
appmenu-update-manual-message = { -brand-shorter-name }-র একটি নতুন কপি ডাউনলোড করুন এবং আমরা আপনাকে এটি ইনস্টল করতে সাহায্য করব।
appmenu-update-whats-new =
    .value = দেখুন নতুন কি আছে।
appmenu-update-restart =
    .label = { -brand-shorter-name } আপডেট করতে পুনরায় চালু করুন।
    .buttonlabel = পুনরারম্ভ করুন এবং পুনরুদ্ধার করুন
    .buttonaccesskey = R
    .secondarybuttonlabel = এই মুহূর্তে নয়
    .secondarybuttonaccesskey = N
appmenu-update-restart-message = দ্রুত পুনর্সূচনার পরে, { -brand-shorter-name } আপনার সমস্ত খোলা ট্যাব এবং উইন্ডোগুলি পুনরুদ্ধার করবে যা ব্যক্তিগত ব্রাউজিং মোডে ছিল না।
appmenu-addon-post-install-message = আপনার অ্যাড-অন পরিচালনা করতে <image data-l10n-name='addon-install-icon'></image> মেনুতে <image data-l10n-name='addon-menu-icon'></image> ক্লিক করুন।

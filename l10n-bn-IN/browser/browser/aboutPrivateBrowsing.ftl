# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-private-browsing-learn-more = আরও জানুন <a data-l10n-name="learn-more">ব্যক্তিগত ব্রাউজিং</a>.
about-private-browsing-info-visited = পরিদর্শিত পেজ
privatebrowsingpage-open-private-window-label = একটি প্রাইভেট উইন্ডো খুলুল
    .accesskey = P
about-private-browsing-info-notsaved = যখন আপনি একটি ব্যাক্তিগত উইন্ডোতে ব্রাউজ করেন, { -brand-short-name }<strong>সংরক্ষণ করবে না</strong>:
about-private-browsing-info-bookmarks = বুকমার্ক
about-private-browsing-info-searches = অনুসন্ধান
about-private-browsing-info-downloads = ডাউনলোডসমূহ
private-browsing-title = ব্যক্তিগত ব্রাউজিং
about-private-browsing-info-saved = { -brand-short-name } <strong>সংরক্ষণ করা হবে</strong> আপনার:
about-private-browsing-info-clipboard = কপি করা টেক্সট
about-private-browsing-info-temporary-files = অস্থায়ী ফাইল
about-private-browsing-info-cookies = কুকিজ
tracking-protection-start-tour = এটিই কিভাবে কাজ করে দেখুন
about-private-browsing-note = ব্যক্তিগত ব্রাউজিং<strong>আপনাকে অজানা করবে না</strong>আপনার নিয়োগকর্তা বা ইন্টারনেট পরিষেবা প্রদানকারী এখনও আপনার পরিদর্শন করা পৃষ্ঠা ট্র্যাক করতে পারেন দয়া করে নোট করুন |
about-private-browsing-not-private = অাপনি বর্তমানে কোনো গোপন উইন্ডোতে নেই।
content-blocking-title = বিষয়বস্তুর ব্লকিং
content-blocking-description = কিছু ওয়েবসাইট ট্র্যাকার ব্যবহার করে যা ইন্টারনেট জুড়ে আপনার কার্যকলাপ পর্যবেক্ষণ করতে পারে। ব্যক্তিগত উইন্ডোতে, { -brand-short-name } বিষয়বস্তু ব্লকিং স্বয়ংক্রিয়ভাবে অনেক ট্র্যাকারকে প্রতিরোধ করে যা আপনার ব্রাউজিং-র আচরণ সম্পর্কে তথ্য সংগ্রহ করতে পারে।

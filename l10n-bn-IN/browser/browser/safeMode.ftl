# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

safe-mode-window =
    .title = { -brand-short-name } নিরাপদ মোড
    .style = max-width: 400px
start-safe-mode =
    .label = নিরাপদ মোডে আরম্ভ করুন
refresh-profile =
    .label = { -brand-short-name } রিফ্রেশ করুন
safe-mode-description = Safe Mode, { -brand-short-name }-র একটি বিশেষ মোড যার মধ্যে বিভিন্ন সমস্যার সমাধান করা যাবে।
safe-mode-description-details = আপনার অ্যাড-অন এবং কাস্টম সেটিংস সাময়িকভাবে অক্ষম করা হবে, এবং { -brand-short-name } -র বৈশিষ্ট্যগুলি বর্তমানের মত সঞ্চালন করতে পারে না।
refresh-profile-instead = এছারাও আপনি সমস্যা সমধান এবং রিফ্রেশ করার চেষ্টা করতে পারেন { -brand-short-name }।
# Shown on the safe mode dialog after multiple startup crashes. 
auto-safe-mode-description = { -brand-short-name } আরম্ভ করার সময় অপ্রত্যাশিতভাবে বন্ধ হয়েছে। অতিরিক্ত সামগ্রী অথবা অন্যান্য কারণে এই সমস্যা দেখা দিতে পারে। আপনি নিরাপদ মোড সমাধান দ্বারা সমস্যার সমাধান করার চেষ্টা করতে পারেন।

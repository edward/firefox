# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

refresh-profile-dialog =
    .title = { -brand-short-name } পুনরায় নির্ধারণ করুন
refresh-profile-dialog-button =
    .label = { -brand-short-name } রিফ্রেশ করুন
refresh-profile-description = সমস্যা দূর করতে এবং পারফরম্যান্স ফেরানর জন্য নতুন করে শুরু করুন।
refresh-profile-description-details = এই হছে:
refresh-profile-remove = আপনার অ্যাড-অন্স এবং কাস্টমাইজেসন সরিয়ে দিন
refresh-profile-restore = আপনার ব্রাউজার সেটিংস্‌ তাদের ডিফল্টে পুনরুদ্ধার করুন
refresh-profile = { -brand-short-name } টিউন উপ দিন
refresh-profile-button = { -brand-short-name } রিফ্রেশ করুন…

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### This file contains the entities needed for the 'edit' menu
### It's currently only used for the Browser Console

editmenu-undo =
    .label = পূর্বাবস্থা
    .accesskey = U
editmenu-redo =
    .label = পুনরাবৃত্তি
    .accesskey = R
editmenu-cut =
    .label = কাট করুন
    .accesskey = t
editmenu-copy =
    .label = কপি করুন
    .accesskey = C
editmenu-paste =
    .label = পেস্ট করুন
    .accesskey = P
editmenu-delete =
    .label = মুছুন
    .accesskey = D
editmenu-select-all =
    .label = সমগ্র নির্বাচন করুন
    .accesskey = A

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

title = নেটওয়ার্কিং সম্পর্কে
warning = এটি পরীক্ষামূলক, বিশেষজ্ঞ ব্যক্তির তত্ত্বাবধান ব্যতীত ব্যবহার করবেন না।
show-next-time-checkbox = পরবর্তীবার এই সতর্কবার্তা প্রদর্শন করা হবে
ok = ঠিক আছে
http = HTTP
sockets = সকেটগুলি
dns = DNS
websockets = ওয়েব সকেটগুলি
refresh = নতুন করে প্রদর্শন
auto-refresh = প্রতি ৩ সেকেন্ডে অটো রিফ্রেশ হবে
hostname = হোস্ট-নেম
port = পোর্ট
ssl = SSL
active = সক্রিয়
idle = নিষ্ক্রিয়
host = হোস্ট
tcp = TCP
sent = প্রেরিত হয়েছে
received = প্রাপ্ত হয়েছে
family = সংকলন
addresses = ঠিকানা
expires = মেয়াদ শেষ (সেকেন্ডে)
messages-sent = বার্তা পাঠানো হয়েছে
messages-received = বার্তা প্রাপ্ত হয়েছে
bytes-sent = বাইট পাঠানো হয়েছে
bytes-received = বাইট প্রাপ্ত হয়েছে
logging = লগ হচ্ছে
log-tutorial = দেখুন <a data-l10n-name="logging">HTTP লগ হচ্ছে</a> কিভাবে এই টুল ব্যবহার করতে হয় তার নির্দেশাবলীর জন্য।
current-log-file = বর্তমান লগ ফাইল:
current-log-modules = বর্তমান লগ মডিউলগুলি:
set-log-file = লগ ফাইল সেট করুন
set-log-modules = লগ মডিউল সেট করুন
start-logging = লগ করা শুরু করুন
stop-logging = লগ করা বন্ধ করুন
dns-lookup = DNS Lookup
dns-lookup-button = সমাধান
dns-domain = ডোমেইন:
dns-lookup-table-column = IPs

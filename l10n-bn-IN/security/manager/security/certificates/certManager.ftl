# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

certmgr-title =
    .title = সার্টিফিকেট পরিচালন ব্যবস্থা
certmgr-tab-mine =
    .label = আপনার ব্যক্তিগত সার্টিফিকেট
certmgr-tab-people =
    .label = ব্যক্তি
certmgr-tab-servers =
    .label = সার্ভার
certmgr-tab-ca =
    .label = কর্তৃপক্ষ (অথোরিটি)
certmgr-detail-general-tab-title =
    .label = সাধারণ
    .accesskey = G
certmgr-detail-pretty-print-tab-title =
    .label = বিবরণ
    .accesskey = D
certmgr-pending-label =
    .value = বর্তমানে চিহ্নিত সার্টিফিকেট পরীক্ষা করা হচ্ছে…
certmgr-subject-info-label =
    .value = প্রাপক
certmgr-issuer-info-label =
    .value = প্রকাশক
certmgr-period-of-validity-label =
    .value = বৈধতার সময়কাল
certmgr-fingerprints-label =
    .value = ফিঙ্গারপ্রিন্ট
certmgr-subject-label = প্রাপক
certmgr-issuer-label = প্রকাশক
certmgr-period-of-validity = বৈধতার সময়কাল
certmgr-fingerprints = ফিঙ্গারপ্রিন্ট
certmgr-cert-detail =
    .title = সার্টিফিকেটের বিবরণ
    .buttonlabelaccept = বন্ধ
    .buttonaccesskeyaccept = C
certmgr-cert-detail-cn =
    .value = সাধারণ নাম (CN)
certmgr-cert-detail-o =
    .value = প্রতিষ্ঠান (O)
certmgr-cert-detail-ou =
    .value = প্রাতিষ্ঠানিক একক (OU)
certmgr-cert-detail-serialnumber =
    .value = ক্রমিক সংখ্যা
certmgr-cert-detail-sha256-fingerprint =
    .value = SHA-256 ফিঙ্গারপ্রিন্ট
certmgr-cert-detail-sha1-fingerprint =
    .value = SHA1 ফিঙ্গারপ্রিন্ট
certmgr-cert-detail-commonname = সাধারণ নাম (CN)
certmgr-cert-detail-org = প্রতিষ্ঠান (O)
certmgr-cert-detail-orgunit = প্রাতিষ্ঠানিক একক (OU)
certmgr-cert-detail-serial-number = ক্রমিক সংখ্যা
certmgr-cert-detail-sha-256-fingerprint = SHA-256 ফিঙ্গারপ্রিন্ট
certmgr-cert-detail-sha-1-fingerprint = SHA1 ফিঙ্গারপ্রিন্ট
certmgr-edit-ca-cert =
    .title = CA সার্টিফিকেটের বিশ্বস্ততা সংক্রান্ত বৈশিষ্ট্য সম্পাদন করুন
    .style = width: 48em;
certmgr-edit-cert-edit-trust = বিশ্বস্ততা সংক্রান্ত বৈশিষ্ট্য সম্পাদন করুন:
certmgr-edit-cert-trust-ssl =
    .label = এই সার্টিফিকেটের সাহায্যে ওয়েব-সাইট সনাক্ত করা সম্ভব।
certmgr-edit-cert-trust-email =
    .label = এই সার্টিফিকেটের সাহায্যে মেইল ব্যবহারকারীদের সনাক্ত করা সম্ভব।
certmgr-delete-cert =
    .title = সার্টিফিকেট মুছে ফেলুন
    .style = width: 48em; height: 24em;
certmgr-cert-name =
    .label = সার্টিফিকেটের নাম
certmgr-cert-server =
    .label = সার্ভার
certmgr-override-lifetime =
    .label = সীমাহীন মেয়াদ
certmgr-token-name =
    .label = নিরাপত্তা ডিভাইস
certmgr-begins-on = আরম্ভ হল
certmgr-begins-label =
    .label = আরম্ভ হল
certmgr-begins-value =
    .value = { certmgr-begins-label.label }
certmgr-expires-on = মেয়াদ পূরণের তারিখ
certmgr-expires-label =
    .label = মেয়াদ পূরণের তারিখ
certmgr-expires-value =
    .value = { certmgr-expires-label.label }
certmgr-email =
    .label = ই-মেইল ঠিকানা
certmgr-serial =
    .label = ক্রমিক সংখ্যা
certmgr-view =
    .label = প্রদর্শন…
    .accesskey = V
certmgr-edit =
    .label = বিশ্বস্ততার মাত্রা পরিবর্তন করুন…
    .accesskey = E
certmgr-export =
    .label = এক্সপোর্ট করুন…
    .accesskey = এ
certmgr-delete =
    .label = মুছে ফেলুন…
    .accesskey = D
certmgr-delete-builtin =
    .label = মুছে ফেলুন অথবা অবিশ্বস্ত রূপে চিহ্নিত করুন…
    .accesskey = D
certmgr-backup =
    .label = ব্যাক-আপ করুন…
    .accesskey = B
certmgr-backup-all =
    .label = সমগ্র ব্যাক-আপ করুন…
    .accesskey = স
certmgr-restore =
    .label = ইম্পোর্ট করুন…
    .accesskey = ই
certmgr-details =
    .value = সার্টিফিকেটের ক্ষেত্র
    .accesskey = F
certmgr-fields =
    .value = ক্ষেত্রের মান
    .accesskey = V
certmgr-hierarchy =
    .value = সার্টিফিকেটের অনুক্রম
    .accesskey = H
certmgr-add-exception =
    .label = ব্যতিক্রম যোগ করুন…
    .accesskey = ব
exception-mgr =
    .title = নিরাপত্তা জড়িত ব্যতিক্রম যোগ করুন
exception-mgr-extra-button =
    .label = নিরাপত্তা জড়িত ব্যতিক্রম নিশ্চিত করুন
    .accesskey = C
exception-mgr-supplemental-warning = বৈধ ব্যাঙ্ক, বিপনী ও অন্যান্য সার্বজনীন সাইট দ্বারা এই অনুরোধ করা হবে না।
exception-mgr-cert-location-url =
    .value = অবস্থান:
exception-mgr-cert-location-download =
    .label = সার্টিফিকেট প্রাপ্ত করুন
    .accesskey = G
exception-mgr-cert-status-view-cert =
    .label = প্রদর্শন…
    .accesskey = V
exception-mgr-permanent =
    .label = এই ব্যতিক্রমটি স্থায়ীরূপে সংরক্ষণ করুন
    .accesskey = P
pk11-bad-password = উল্লিখিত পাসওয়ার্ড সঠিক নয়।
pkcs12-decode-err = ফাইল ডিকোড করতে ব্যর্থ।  সম্ভবত এটি PKCS #12 বিন্যাসে নেই, ক্ষতিগ্রস্থ হয়েছে, অথবা আপনার উল্লিখিত পাসওয়ার্ডটি সঠিক নয়।
pkcs12-unknown-err-restore = অজ্ঞাত কারণে PKCS #12 ফাইলটি উদ্ধার করা যায়নি।
pkcs12-unknown-err-backup = অজ্ঞাত কারণে PKCS #12 ব্যাক-আপ ফাইলটি নির্মাণ করা যায়নি।
pkcs12-unknown-err = অজ্ঞাত কারণে PKCS #12 কর্মটি ব্যর্থ হয়েছে।
pkcs12-info-no-smartcard-backup = কোনো হার্ডওয়্যার নিরাপত্তা ডিভাইস যেমন স্মার্ট-কার্ড থেকে সার্টিফিকেট ব্যাক-আপ করা সম্ভব নয়।
pkcs12-dup-data = সার্টিফিকেট ও ব্যক্তিগত-কি (key) নিরাপত্তা ডিভাইসের মধ্যে উপস্থিত রয়েছে।

## PKCS#12 file dialogs

choose-p12-backup-file-dialog = যে ফাইল ব্যাক-আপ করা হবে
file-browse-pkcs12-spec = PKCS12 ফাইল
choose-p12-restore-file-dialog = ইম্পোর্ট করার উদ্দেশ্যে চিহ্নিত সার্টিফিকেট ফাইল

## Import certificate(s) file dialog

file-browse-certificate-spec = সার্টিফিকেট ফাইল
import-ca-certs-prompt = ইম্পোর্ট করার উদ্দেশ্যে CA সার্টিফিকেট ধারণকারী কোনো ফাইল নির্বাচন করুন
import-email-cert-prompt = কোনো ব্যক্তির ই-মেইল সার্টিফিকেট ইম্পোর্ট করার উদ্দেশ্যে তা ধারণকারী ফাইল নির্বাচন করুন

## For editing certificates trust

# Variables:
#   $certName: the name of certificate
edit-trust-ca = "{ $certName }" একটি সার্টিফিকেট অথোরিটি (CA)-কে চিহ্নিত করছে।

## For Deleting Certificates

delete-user-cert-title =
    .title = আপনার সার্টিফিকেটগুলি মুছে ফেলুন
delete-user-cert-confirm = আপনি কি নিশ্চিতরূপে এই সার্টিফিকেটগুলি মুছে ফেলতে ইচ্ছুক?
delete-user-cert-impact = আপনার কোনো নিজস্ব সার্টিফিকেট মুছে ফেলা হলে, পরিচয় প্রমাণের জন্য আপনি তা পুনরায় ব্যবহার করতে সক্ষম হবেন না।
delete-ssl-cert-title =
    .title = সার্ভার সার্টিফিকেটের ব্যতিক্রম মুছে ফেলুন
delete-ssl-cert-confirm = আপনি কি নিশ্চিতরূপে সার্ভারের এই ব্যতিক্রমগুলি মুছে ফেলতে ইচ্ছুক?
delete-ssl-cert-impact = সার্ভার সংক্রান্ত ব্যতিক্রম মুছে ফেলা হলে, সংশ্লিষ্ট সার্ভারের জন্য নিরাপত্তামূলক পরীক্ষা করা হবে ও একটি বৈধ সার্টিফিকেটের ব্যবহার আবশ্যক হবে।
delete-ca-cert-title =
    .title = CA সার্টিফিকেট মুছে ফেলুন অথবা অবিশ্বস্ত রূপে ধার্য করুন
delete-ca-cert-confirm = চিহ্নিত CA সার্টিফিকেটগুলি আপনি মুছে ফেলার অনুরোধ জানিয়েছেন। বিল্ট-ইন সার্টিফিকেটের ক্ষেত্রে সর্বধরনের বিশ্বস্ততা মুছে ফেলা হলে সমতূল্য প্রভাব সৃষ্টি হবে। আপনি কি নিশ্চিতরূপে মুছে ফেলতে অথবা অবিশ্বস্ত রূপে ধার্য করতে ইচ্ছুক?
delete-ca-cert-impact = কোনো সার্টিফিকেট অথোরিটির (CA) সার্টিফিকেট মুছে ফেলা হলে অথবা অবিশ্বস্ত হিসাবে ধার্য করা হলে, ঐ CA-র দ্বারা প্রকাশিত কোনো সার্টিফিকেট এই অ্যাপ্লিকেশন দ্বারা বিশ্বাস করা হবে না।
delete-email-cert-title =
    .title = ই-মেইল সার্টিফিকেট মুছে ফেলুন
delete-email-cert-confirm = আপনি কি নিশ্চিতরূপে উল্লিখিত ব্যক্তিদের ই-মেইল সার্টিফিকেটগুলি মুছে ফেলতে ইচ্ছুক?
delete-email-cert-impact = কোনো ই-মেইল সার্টিফিকেট মুছে ফেলা হলে সংশ্লিষ্ট ব্যক্তিদেরকে  এনক্রিপ্ট করা ই-মেইল পাঠানো সম্ভব হবে না।
# Used for semi-uniquely representing a cert.
#
# Variables:
#   $serialNumber : the serial number of the cert in AA:BB:CC hex format.
cert-with-serial =
    .value = সিরিয়াল নম্বরের সাথে সার্টিফিকেট: { $serialNumber }

## Cert Viewer

not-present =
    .value = <সার্টিফিকেটের অংশ নয়>
# Cert verification
cert-verified = এই সার্টিফিকেটটি নিম্নলিখিত কর্মের জন্য পরীক্ষিত হয়েছে:
# Add usage
verify-ssl-client =
    .value = SSL ক্লায়েন্ট সার্টিফিকেট
verify-ssl-server =
    .value = SSL সার্ভার সার্টিফিকেট
verify-ssl-ca =
    .value = SSL সার্টিফিকেট অথোরিটি
verify-email-signer =
    .value = ই-মেইল স্বাক্ষরকারী সার্টিফিকেট
verify-email-recip =
    .value = ই-মেইল গ্রহণকারী সার্টিফিকেট
# Cert verification
cert-not-verified-cert-revoked = সার্টিফিকেট প্রত্যাখ্যান হওয়ার দরুন এটি পরীক্ষা করা সম্ভব হয়নি।
cert-not-verified-cert-expired = মেয়াদ পূর্ণ হওয়ার দরুন এই সার্টিফিকেটটি পরীক্ষা করা যায়নি।
cert-not-verified-cert-not-trusted = বিশ্বস্ত না হওয়ার দরুন এই সার্টিফিকেটটি পরীক্ষা করা যায়নি।
cert-not-verified-issuer-not-trusted = প্রকাশকারী বিশ্বস্ত না হওয়ার দরুন এই সার্টিফিকেটটি পরীক্ষা করা সম্ভব হয়নি।
cert-not-verified-issuer-unknown = প্রকাশকারী পরিচিত না হওয়ার দরুন এই সার্টিফিকেটটি পরীক্ষা করা সম্ভব হয়নি।
cert-not-verified-ca-invalid = CA সার্টিফিকেট বৈধ না হওয়ার দরুন এই সার্টিফিকেটটি পরীক্ষা করা সম্ভব হয়নি।
cert-not-verified_algorithm-disabled = এই সার্টিফিকেটটি স্বাক্ষর করার জন্য ব্যবহৃত অ্যালগোরিদমটি নিরাপদ না হওয়ায় সেটি নিষ্ক্রিয় করার ফলে এই সার্টিফিকেটটি যাচাই করা যায়নি
cert-not-verified-unknown = অজানা কারণের দরুন এই সার্টিফিকেট পরীক্ষা করা যায়নি।

## Add Security Exception dialog

add-exception-branded-warning = { -brand-short-name } দ্বারা এই সাইটের পরিচয় নির্ধারণের প্রক্রিয়া আপনি পরিবর্তন করতে চলেছেন।
add-exception-invalid-header = এই সাইট দ্বারা অবৈধ তথ্য সহযোগে নিজের পরিচয় প্রমাণের প্রচেষ্টা করা হয়েছে।
add-exception-domain-mismatch-short = সাইট সঠিক নয়
add-exception-domain-mismatch-long = এই সার্টিফিকেট অন্য একটি সাইটের, যা মনে হতে পারে কেউ সাইটে ছদ্মবেশ ধারণ করবার চেষ্টা করছে।
add-exception-expired-short = মেয়াদোত্তীর্ণের তথ্য
add-exception-expired-long = এই সার্টিফিকেটটি বর্তমানে বৈধ নয়। এটা হয়তো চুরি করা হয়েছে বা হারিয়ে গেছে, এবং সাইটে ছদ্মবেশধারণকারী কারুর দ্বারা ব্যবহৃত হতে পারে।
add-exception-unverified-or-bad-signature-short = অজানা পরিচয়
add-exception-unverified-or-bad-signature-long = পরিচিত সার্টিফিকেট অথোরিটি দ্বারা নিরাপদ স্বাক্ষর সহ পরীক্ষিত না হওয়ার ফলে এই সার্টিফিকেটটি বিশ্বস্ত নয়।
add-exception-valid-short = বৈধ সার্টিফিকেট
add-exception-valid-long = এই সাইট দ্বারা বৈধ ও পরীক্ষিত পরিচয় উপলব্ধ করা হয়েছে।  এই ক্ষেত্রে ব্যতিক্রম যোগ করার প্রয়োজন নেই।
add-exception-checking-short = তথ্য পরীক্ষা করা হচ্ছে
add-exception-checking-long = সাইটের পরিচয় নির্ধারণের প্রচেষ্টা করা হচ্ছে…
add-exception-no-cert-short = কোনো তথ্য উপলব্ধ নেই
add-exception-no-cert-long = চিহ্নিত সাইটের পরিচয় নির্ধারণের অবস্থা সম্পর্কে তথ্য প্রাপ্ত করতে ব্যর্থ।

## Certificate export "Save as" and error dialogs

save-cert-as = ফাইলের মধ্যে সার্টিফিকেট সংরক্ষণ করা হবে
cert-format-base64 = X.509 সার্টিফিকেট (PEM)
cert-format-base64-chain = চেইন সহ X.509 সার্টিফিকেট (PEM)
cert-format-der = X.509 সার্টিফিকেট (DER)
cert-format-pkcs7 = X.509 সার্টিফিকেট (PKCS#7)
cert-format-pkcs7-chain = চেইন সহ X.509 সার্টিফিকেট (PKCS#7)
write-file-failure = ফাইল সংক্রান্ত ত্রুটি

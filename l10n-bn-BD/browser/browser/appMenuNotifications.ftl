# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

appmenu-update-available =
    .label = নতুন { -brand-shorter-name } হালনাগাদ রয়েছে।
    .buttonlabel = হালনাগাদ ডাউনলোড
    .buttonaccesskey = D
    .secondarybuttonlabel = এখন নয়
    .secondarybuttonaccesskey = N
appmenu-update-available-message = গতি ও গোপনীয়তায় নতুন কিছুর জন্য { -brand-shorter-name } হালনাগাদ করুন।
appmenu-update-manual =
    .label = { -brand-shorter-name } সাম্প্রতিকতম সংস্করণে হালনাগাদ করা যায়নি।
    .buttonlabel = { -brand-shorter-name } ডাউনলোড
    .buttonaccesskey = D
    .secondarybuttonlabel = এখন নয়
    .secondarybuttonaccesskey = N
appmenu-update-manual-message = একটি ফ্রেশ { -brand-shorter-name } কপি ডাউনলোড করুন এবং আমরা সেটি ইনস্টল করতে সহায়তা করব।
appmenu-update-whats-new =
    .value = দেখুন নতুন কি আছে।
appmenu-update-restart =
    .label = { -brand-shorter-name } হালনাগাদ করতে রিস্টার্ট করুন।
    .buttonlabel = রিস্টার্ট ও রিস্টোর
    .buttonaccesskey = R
    .secondarybuttonlabel = এখন নয়
    .secondarybuttonaccesskey = N
appmenu-update-restart-message = দ্রুত পুনরাম্ভের পরে, { -brand-shorter-name } আপনার সমস্ত খোলা ট্যাব এবং উইন্ডোগুলি পুনরুদ্ধার করবে যা ব্যক্তিগত ব্রাউজিং মোডে নয়।
appmenu-addon-private-browsing-installed =
    .buttonlabel = ঠিকাছে, বুঝতে পেরেছি
    .buttonaccesskey = O
appmenu-addon-post-install-message = আপনার অ্যাড-অন পরিচালনা করতে <image data-l10n-name='addon-install-icon'></image> মেনুতে <image data-l10n-name='addon-menu-icon'></image> ক্লিক করুন।

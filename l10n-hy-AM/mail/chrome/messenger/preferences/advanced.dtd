<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY itemGeneral.label       "Ընդհանուր">
<!ENTITY dataChoicesTab.label    "Ընտրել տվյալները">
<!ENTITY itemUpdate.label        "Թարմացնել">
<!ENTITY itemNetworking.label    "Ցանցը և ազատ տեղ">
<!ENTITY itemCertificates.label  "Հավաստագրեր">

<!-- General Settings -->
<!ENTITY enableGlodaSearch.label       "Միացնել Ընդհանուր Որոնումը և Ինդեքսավորումը">
<!ENTITY enableGlodaSearch.accesskey   "Մ">
<!ENTITY dateTimeFormatting.label      "Ամսաթվի և ժամանակի ձևաչափ">
<!ENTITY allowHWAccel.label            "Հնարավորության դեպքում օգտագործել սարքակազմի արագացում">
<!ENTITY allowHWAccel.accesskey        "ս">
<!ENTITY storeType.label               "Նամակները պահելու եղանակը՝ նոր հաշիվների համար.">
<!ENTITY storeType.accesskey           "ե">
<!ENTITY mboxStore2.label              "Ֆայլ թղթապանակում (mbox)">
<!ENTITY maildirStore.label            "Ֆայլ ամեն նամակի համար (maildir)">

<!ENTITY scrolling.label               "Թերթումը">
<!ENTITY useAutoScroll.label           "Օգտագործել ինքնավար թերթում">
<!ENTITY useAutoScroll.accesskey       "U">
<!ENTITY useSmoothScrolling.label      "Օգտագործել կոկիկ թերթում">
<!ENTITY useSmoothScrolling.accesskey  "m">

<!ENTITY systemIntegration.label       "Համակարգային ինտեգրում">
<!ENTITY alwaysCheckDefault.label      "Բացելիս միշտ ստուգել, թե արդյոք &brandShortName;-ը փոստային հիմնական ծրագիրն է">
<!ENTITY alwaysCheckDefault.accesskey  "A">
<!ENTITY searchIntegration.label       "Թույլատրել &searchIntegration.engineName;-ին որոնել նամակներ">
<!ENTITY searchIntegration.accesskey   "s">
<!ENTITY checkDefaultsNow.label        "Ստուգել...">
<!ENTITY checkDefaultsNow.accesskey    "N">
<!ENTITY configEditDesc.label          "Ընդլայնված կարգավորում">
<!ENTITY configEdit.label              "Խմբագրիչի կարգ...">
<!ENTITY configEdit.accesskey          "C">
<!ENTITY returnReceiptsInfo.label      "Որոշեք, թե &brandShortName;-ը ինչպես վարվի ստացականների հետ։">
<!ENTITY showReturnReceipts.label      "Ստացականներ...">
<!ENTITY showReturnReceipts.accesskey  "R">

<!-- Data Choices -->
<!ENTITY telemetrySection.label          "Telemetry">
<!ENTITY telemetryDesc.label             "Տարածում է արագագործության, օգտագործման, սարքակազմի և ձեր դիտարկիչի կարգավորումները &vendorShortName;-ի հետ՝ &brandShortName;-ը էլ ավելի լավարկելու համար">
<!ENTITY enableTelemetry.label           "Միացնել Telemetry-ին">
<!ENTITY enableTelemetry.accesskey       "T">
<!ENTITY telemetryLearnMore.label        "Իմանալ ավելին">

<!ENTITY crashReporterSection.label      "Վթարի Զեկուցիչ">
<!ENTITY crashReporterDesc.label         "&brandShortName;-ը ուղարկում է Վթարների Զեկույց՝ օգնելու համար դարձնել &vendorShortName;-ը էլ ավելի կայուն և անվտանգ">
<!ENTITY enableCrashReporter.label       "Միացնել Վթարների Զեկույցը">
<!ENTITY enableCrashReporter.accesskey   "Վ">
<!ENTITY crashReporterLearnMore.label    "Իմանալ ավելին">

<!-- Update -->
<!-- LOCALIZATION NOTE (updateApp.label):
  Strings from aboutDialog.dtd are displayed in this section of the preferences.
  Please check for possible accesskey conflicts.
-->
<!ENTITY updateApp2.label                "&brandShortName;-ի թարմացումներ">
<!-- LOCALIZATION NOTE (updateApp.version.*): updateApp.version.pre is
  followed by a version number, keep the trailing space or replace it with
  a different character as needed. updateApp.version.post is displayed after
  the version number, and is empty on purpose for English. You can use it
  if required by your language.
 -->
<!ENTITY updateApp.version.pre           "Տարբերակ՝ ">
<!ENTITY updateApp.version.post          "">
<!ENTITY updateAuto.label                "Ինքնաշխատ տեղադրել թարմացումները (խորհուրդ է տրվում)">
<!ENTITY updateAuto.accesskey            "Ի">
<!ENTITY updateCheck.label               "Ստուգել թարմացումները, բայց ես ինքս կորոշեմ տեղադրել, թե ոչ">
<!ENTITY updateCheck.accesskey           "Ս">
<!ENTITY updateManual.label              "Երբեք չստուգել թարմացումները (խորհուրդ չի տրվում)">
<!ENTITY updateManual.accesskey          "Ե">
<!ENTITY updateHistory.label             "Ցուցադրել թարմացումների պատմությունը">
<!ENTITY updateHistory.accesskey         "ա">

<!ENTITY useService.label                "Թարմացումները տեղադրել խորապատկերում">
<!ENTITY useService.accesskey            "b">

<!-- Networking and Disk Space -->
<!ENTITY showSettings.label            "Կարգավորումներ...">
<!ENTITY showSettings.accesskey        "S">
<!ENTITY proxiesConfigure.label        "Կարգավորել, թե ինչպես &brandShortName;-ը մուտք գործի համացանց">
<!ENTITY connectionsInfo.caption       "Կապակցում">
<!ENTITY offlineInfo.caption           "Անցանց">
<!ENTITY offlineInfo.label             "Կարգավորել անցանցը">
<!ENTITY showOffline.label             "Անցանց...">
<!ENTITY showOffline.accesskey         "O">

<!ENTITY Diskspace                       "Ազատ տեղ">
<!ENTITY offlineCompactFolders.label     "Սեղմել թղթապանակները, եթե դա կխնայի՝ ">
<!ENTITY offlineCompactFolders.accesskey "a">
<!ENTITY offlineCompactFoldersMB.label   "ՄԲ ընդամենը">

<!-- LOCALIZATION NOTE:
  The entities useCacheBefore.label and useCacheAfter.label appear on a single
  line in preferences as follows:

  &useCacheBefore.label  [ textbox for cache size in MB ]   &useCacheAfter.label;
-->
<!ENTITY useCacheBefore.label            "Օգտագործել մինչև">
<!ENTITY useCacheBefore.accesskey        "U">
<!ENTITY useCacheAfter.label             "ՄԲ պահոցի համար">
<!ENTITY overrideSmartCacheSize.label    "Վերագրել շտեմի ինքնաշխատ կառավարումը">
<!ENTITY overrideSmartCacheSize.accesskey "v">
<!ENTITY clearCacheNow.label             "Մաքրել Հիմա">
<!ENTITY clearCacheNow.accesskey         "C">

<!-- Certificates -->
<!ENTITY certSelection.description       "Երբ սպասարկիչը պահանջում է հավաստագիր.">
<!ENTITY certs.auto                      "Ընտրել որևէ մեկը">
<!ENTITY certs.auto.accesskey            "S">
<!ENTITY certs.ask                       "Ամեն անգամ հարցնել">
<!ENTITY certs.ask.accesskey             "A">
<!ENTITY enableOCSP.label                "OCSP հարցման պատասխանիչ սպասարկիչը՝ հաստատելու ընթացիկ վավերության վկայագիրը">
<!ENTITY enableOCSP.accesskey            "O">

<!ENTITY manageCertificates.label "Կառավարել վկայագրերը">
<!ENTITY manageCertificates.accesskey "Կ">
<!ENTITY viewSecurityDevices.label "Անվտանգության սարքեր">
<!ENTITY viewSecurityDevices.accesskey "ս">

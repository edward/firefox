<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "Firefox Sync">
<!ENTITY syncBrand.shortName.label "Sync">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label 'Kennaskañ ouzh &syncBrand.shortName.label;'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'Evit gweredekaat ho trevnad nevez, diuzit ”&syncBrand.shortName.label;” war an trevnad.'>
<!ENTITY sync.subtitle.pair.label 'Da weredekaat, diuzit “Koublañ un trevnad” war ho trevnad all.'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'N\&apos;emañ ket an trevnad ganin…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'Titouroù kennaskañ'>
<!ENTITY sync.configure.engines.title.history 'Roll istor'>
<!ENTITY sync.configure.engines.title.tabs 'Ivinelloù'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS1; war &formatS2;'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'Lañser ar sinedoù'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'Gerioù Alc\&apos;hwez'>
<!ENTITY bookmarks.folder.toolbar.label 'Barrenn ostilhoù ar sinedoù'>
<!ENTITY bookmarks.folder.other.label 'Sinedoù all'>
<!ENTITY bookmarks.folder.desktop.label 'Sinedoù urzhiataer'>
<!ENTITY bookmarks.folder.mobile.label 'Sinedoù hezougell'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'Spilhennet'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'Distreiñ d\&apos;ar merdeiñ'>

<!ENTITY fxaccount_getting_started_welcome_to_sync 'Donemat war &syncBrand.shortName.label;'>
<!ENTITY fxaccount_getting_started_description2 'Kennaskit evit goubredañ hoc\&apos;h ivinelloù, ho sinedoù, ho titouroù kennaskañ hag ouzhpenn c\&apos;hoazh.'>
<!ENTITY fxaccount_getting_started_get_started 'Deraouiñ'>
<!ENTITY fxaccount_getting_started_old_firefox 'Emaoc\&apos;h oc\&apos;h ober gant un handelv all eus &syncBrand.shortName.label; ?'>

<!ENTITY fxaccount_status_auth_server 'Dafariad ar gont'>
<!ENTITY fxaccount_status_sync_now 'Goubredañ bremañ'>
<!ENTITY fxaccount_status_syncing2 'O goubredañ...'>
<!ENTITY fxaccount_status_device_name 'Anv an trevnad'>
<!ENTITY fxaccount_status_sync_server 'Dafariad Sync'>
<!ENTITY fxaccount_status_needs_verification2 'Ezhomm ez eus gwiriañ ho kont. Stokit da gas ar postel gwiriañ en-dro.'>
<!ENTITY fxaccount_status_needs_credentials 'N\&apos;haller ket kennaskañ. Stokit d\&apos;en em varilhañ.'>
<!ENTITY fxaccount_status_needs_upgrade 'Ret eo deoc\&apos;h hizivaat &brandShortName; d\&apos;en em varilhañ.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled 'Kefluniet eo &syncBrand.shortName.label;, ne c\&apos;houbred ket ent emgefreek avat. Trec\&apos;haolit an arventenn ”Goubredañ ar roadennoù emgefreek” e Arventennoù Android &gt; Arver ar roadennoù.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 'Kefluniet eo &syncBrand.shortName.label;, ne c\&apos;houbred ket ent emgefreek avat. Trec\&apos;haolit an arventenn ”Goubredañ ar roadennoù emgefreek” e Arventennoù Android &gt; Arver ar roadennoù.'>
<!ENTITY fxaccount_status_needs_finish_migrating 'Stokit evit kennaskañ d\&apos;ho Kont Firefox nevez.'>
<!ENTITY fxaccount_status_choose_what 'Dibabit petra vo goubredet'>
<!ENTITY fxaccount_status_bookmarks 'Sinedoù'>
<!ENTITY fxaccount_status_history 'Roll istor'>
<!ENTITY fxaccount_status_passwords2 'Titouroù kennaskañ'>
<!ENTITY fxaccount_status_tabs 'Digeriñ ivinelloù'>
<!ENTITY fxaccount_status_additional_settings 'Arventennoù ouzhpenn'>
<!ENTITY fxaccount_pref_sync_use_metered2 'Goubredañ dre Wi-Fi hepken'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 'Mirout &brandShortName; da c\&apos;houbredañ war ur rouedad pellgomzer hezoug'>
<!ENTITY fxaccount_status_legal 'Lezennek' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'Divizoù arver'>
<!ENTITY fxaccount_status_linkprivacy2 'Evezhiadennoù a-fet buhez prevez'>
<!ENTITY fxaccount_remove_account 'Digennaskañ&ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 'Digennaskañ diouzh Sync?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'Ho roadennoù merdeiñ a chomo war an trevnad-mañ me ne vo ket goubredet ken gant ho kont.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 'Dilamet eo bet ar gont Firefox &formatS;.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'Digennaskañ'>

<!ENTITY fxaccount_enable_debug_mode 'Diweredekaat ar mod diveugañ'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title 'Dibarzhioù &syncBrand.shortName.label;'>
<!ENTITY fxaccount_options_configure_title 'Kefluniañ &syncBrand.shortName.label;'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 'N\&apos;eo ket kennasket &syncBrand.shortName.label;'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 'Stokit d\&apos;en em gennaskañ gant &formatS;'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title 'Echuiñ gant hizivadenn &syncBrand.shortName.label; ?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text 'Stokit d\&apos;en em gennaskañ gant &formatS;'>

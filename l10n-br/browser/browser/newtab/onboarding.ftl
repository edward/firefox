# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = Amprouit bremañ
onboarding-button-label-get-started = Stagañ e-barzh
onboarding-welcome-header = Donemat war { -brand-short-name }
onboarding-start-browsing-button-label = Stagañ da verdeiñ

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = Merdeiñ prevez
onboarding-private-browsing-text = Merdeit drezoc'h hoc'h unan. Ar merdeiñ prevez gant ar stanker endalc'had a stank heulierien enlinenn a heuilh ac'hanoc'h war ar web.
onboarding-screenshots-title = Tapadennoù skramm
onboarding-screenshots-text = Kemerit, enrollit ha rannit tapadennoù skramm - hep kuitaat { -brand-short-name }. Tapit ul lodenn pe ur bajenn a-bezh en ur verdeiñ. Enrollit war ar web evit gallout he haeziñ hag he rannañ aesoc'h.
onboarding-addons-title = Askouezhioù
onboarding-addons-text = Ouzhpennit keweriusterioù a lako { -brand-short-name } da labourat muioc'h evidoc'h. Keñveriit prizioù, sellit ouzh liv an amzer pe eztaolit ho personelezh gant un neuz personelaet.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Merdeit herrekoc'h, speredekoc'h, pe diogeloc'h gant askouezhioù evel Ghostery hag a ro tro deoc'h da stankañ ar bruderezhioù aloubus.
# Note: "Sync" in this case is a generic verb, as in "to synchronize"
onboarding-fxa-title = Synkro
onboarding-fxa-text = Krouit ho { -fxaccount-brand-name } ha goubredit ho sinedoù, gerioù-tremen hag hoc'h ivinelloù digor e pep lec'h ma implijit { -brand-short-name }.

## Message strings belonging to the Return to AMO flow

return-to-amo-sub-header = Dispar, { -brand-short-name } a zo ganeoc'h
# <icon></icon> will be replaced with the icon belonging to the extension
#
# Variables:
#   $addon-name (String) - Name of the add-on
return-to-amo-addon-header = Staliomp <icon></icon><b>{ $addon-name }</b> bremañ.
return-to-amo-extension-button = Ouzhpennañ an askouezh
return-to-amo-get-started-button = Krogit gant { -brand-short-name }

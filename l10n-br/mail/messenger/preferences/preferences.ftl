# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

choose-messenger-language-description = Dibabit ar yezhoù arveret evit diskouez al lañserioù, kemennadennoù ha rebuzadurioù eus { -brand-short-name }.
manage-messenger-languages-button =
    .label = Yezhoù all...
    .accesskey = Y
confirm-messenger-language-change-description = Adloc'hit { -brand-short-name } da arloañ ar c'hemmoù
confirm-messenger-language-change-button = Arloañ hag adloc'hañ
update-pref-write-failure-title = Fazi skrivañ
# Variables:
#   $path (String) - Path to the configuration file
update-pref-write-failure-message = N'haller ket enrollañ ar gwellvezioù. Dic'houest eo da skrivañ er restr: { $path }

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

choose-messenger-language-description = Wubjeŕśo rěcy, kótarež se wužywaju, aby menije, powěsći a powěźeńki z { -brand-short-name } pokazali.
manage-messenger-languages-button =
    .label = Alternatiwy definěrowaś…
    .accesskey = l
confirm-messenger-language-change-description = Startujśo { -brand-short-name } znowego, aby toś te změny nałožył
confirm-messenger-language-change-button = Nałožyś a znowego startowaś
update-pref-write-failure-title = Pisańska zmólka
# Variables:
#   $path (String) - Path to the configuration file
update-pref-write-failure-message = Nastajenje njedajo se składowaś. Njejo było móžno do dataje pisaś: { $path }

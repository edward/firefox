# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-learn-more = Даведацца больш
onboarding-button-label-try-now = Паспрабуйце зараз
onboarding-button-label-get-started = Пачаць
onboarding-welcome-header = Вітаем у { -brand-short-name }
onboarding-join-form-header = Далучыцца да { -brand-product-name }
onboarding-join-form-body = Увядзіце ваш адрас эл.пошты для пачатку працы.
onboarding-join-form-email =
    .placeholder = Увядзіце адрас эл.пошты
onboarding-join-form-email-error = Патрабуецца сапраўдны адрас эл.пошты
onboarding-join-form-continue = Працягнуць
onboarding-start-browsing-button-label = Пачаць агляданне

## These are individual benefit messages shown with an image, title and
## description.


## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = Прыватнае агляданне
onboarding-private-browsing-text = Аглядайце самі па сабе. Прыватнае агляданне з блакаваннем змесціва блакіруе сеціўныя трэкеры, якія сочаць за вамі ў інтэрнэце.
onboarding-screenshots-title = Здымкі экрана
onboarding-screenshots-text = Рабіце, захоўвайце і дзяліцеся здымкамі экрана — не выходзячы з { -brand-short-name }. Пры агляданні захапіце ўчастак або ўсю старонку. Потым захавайце ў інтэрнэце, каб мець лёгкі доступ і хутка дзяліцца.
onboarding-addons-title = Дадаткі
onboarding-addons-text = Дадавайце іншыя функцыі, каб { -brand-short-name } працаваў лепш для вас. Параўноўвайце цэны, глядзіце надвор'е, або падкрэслівайце індывідуальнасць уласнай тэмай афармлення.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Зрабіце агляд хутчэйшым, бяспечнейшым і зручнейшым з дадаткамі накшталт Ghostery, які дазваляе блакіраваць рэкламу, што раздражняе.
# Note: "Sync" in this case is a generic verb, as in "to synchronize"
onboarding-fxa-title = Сінхранізаваць
onboarding-fxa-text = Зарэгіструйцеся ў { -fxaccount-brand-name } і сінхранізуйце свае закладкі, паролі і адкрытыя карткі ўсюды, дзе карыстаецеся { -brand-short-name }.
# "Mobile" is short for mobile/cellular phone, "Browser" is short for web
# browser.
onboarding-mobile-phone-button = Сцягнуць мабільны браўзер
onboarding-pocket-anywhere-title = Чытайце і слухайце ў любым месцы
onboarding-facebook-container-button = Дадаць пашырэнне

## Message strings belonging to the Return to AMO flow

return-to-amo-sub-header = Выдатна, у вас ёсць { -brand-short-name }
# <icon></icon> will be replaced with the icon belonging to the extension
#
# Variables:
#   $addon-name (String) - Name of the add-on
return-to-amo-addon-header = Цяпер давайце пяройдзем да <icon></icon><b>{ $addon-name }.</b>
return-to-amo-extension-button = Дадаць пашырэнне
return-to-amo-get-started-button = Пачніце працаваць з { -brand-short-name }

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Krefst endurræsingar
restart-required-header = Því miður þurfum við að gera svolítið til að halda áfram.
restart-required-intro = Búið er að setja inn uppfærslu á bakvið tjöldin. Smelltu á Endurræsa { -brand-short-name } til að ljúka við innsetninguna.
restart-required-description = Við munum endurheimta allar síður, glugga og flipa í kjölfarið svo þú getir haldið áfram.
restart-button-label = Endurræsi { -brand-short-name }

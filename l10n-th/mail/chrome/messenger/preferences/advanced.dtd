<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY itemGeneral.label       "ทั่วไป">
<!ENTITY dataChoicesTab.label    "ทางเลือกข้อมูล">
<!ENTITY itemUpdate.label        "การอัปเดต">
<!ENTITY itemNetworking.label    "เครือข่ายและพื้นที่ดิสก์">
<!ENTITY itemCertificates.label  "ใบรับรอง">

<!-- General Settings -->
<!ENTITY enableGlodaSearch.label       "เปิดใช้งานการค้นหาส่วนกลางและตัวทำดัชนี">
<!ENTITY enableGlodaSearch.accesskey   "ป">
<!ENTITY dateTimeFormatting.label      "การจัดรูปแบบวันที่และเวลา">
<!ENTITY languageSelector.label        "ภาษา">
<!ENTITY allowHWAccel.label            "ใช้การเร่งความเร็วด้วยฮาร์ดแวร์เมื่อพร้อมใช้งาน">
<!ENTITY allowHWAccel.accesskey        "ช">
<!ENTITY storeType.label               "ชนิดการเก็บข้อความสำหรับบัญชีใหม่:">
<!ENTITY storeType.accesskey           "น">
<!ENTITY mboxStore2.label              "ไฟล์ต่อโฟลเดอร์ (mbox)">
<!ENTITY maildirStore.label            "ไฟล์ต่อข้อความ (maildir)">

<!ENTITY scrolling.label               "การเลื่อน">
<!ENTITY useAutoScroll.label           "ใช้การเลื่อนอัตโนมัติ">
<!ENTITY useAutoScroll.accesskey       "ช">
<!ENTITY useSmoothScrolling.label      "ใช้การเลื่อนแบบลื่นไหล">
<!ENTITY useSmoothScrolling.accesskey  "ก">

<!ENTITY systemIntegration.label       "การผนวกรวมระบบ">
<!ENTITY alwaysCheckDefault.label      "ตรวจสอบเสมอว่า &brandShortName; เป็นไคลเอนต์จดหมายเริ่มต้นหรือไม่เมื่อเริ่มต้น">
<!ENTITY alwaysCheckDefault.accesskey  "ต">
<!ENTITY searchIntegration.label       "อนุญาตให้ &searchIntegration.engineName; ค้นหาข้อความ">
<!ENTITY searchIntegration.accesskey   "อ">
<!ENTITY checkDefaultsNow.label        "ตรวจสอบตอนนี้…">
<!ENTITY checkDefaultsNow.accesskey    "ต">
<!ENTITY configEditDesc.label          "การกำหนดค่าขั้นสูง">
<!ENTITY configEdit.label              "ตัวแก้ไขการกำหนดค่า…">
<!ENTITY configEdit.accesskey          "ว">
<!ENTITY returnReceiptsInfo.label      "กำหนดวิธีที่ &brandShortName; จัดการการแจ้งเตือนการเปิดอ่าน">
<!ENTITY showReturnReceipts.label      "การแจ้งเตือนการเปิดอ่าน…">
<!ENTITY showReturnReceipts.accesskey  "ก">

<!-- Data Choices -->
<!ENTITY telemetrySection.label          "การวัดและส่งข้อมูลทางไกล">
<!ENTITY telemetryDesc.label             "แบ่งปันข้อมูลประสิทธิภาพ, การใช้งาน, ฮาร์ดแวร์ และการปรับแต่งเกี่ยวกับไคลเอนต์อีเมลของคุณกับ &vendorShortName; เพื่อช่วยเราทำ &brandShortName; ให้ดีขึ้น">
<!ENTITY enableTelemetry.label           "เปิดใช้งานการวัดและส่งข้อมูลทางไกล">
<!ENTITY enableTelemetry.accesskey       "ป">
<!ENTITY telemetryLearnMore.label        "เรียนรู้เพิ่มเติม">

<!ENTITY crashReporterSection.label      "ตัวรายงานข้อขัดข้อง">
<!ENTITY crashReporterDesc.label         "&brandShortName; ส่งรายงานข้อขัดข้องเพื่อช่วยให้ &vendorShortName; สร้างไคลเอนต์อีเมลของคุณให้เสถียรและปลอดภัยยิ่งขึ้น">
<!ENTITY enableCrashReporter.label       "เปิดใช้งานตัวรายงานข้อขัดข้อง">
<!ENTITY enableCrashReporter.accesskey   "ด">
<!ENTITY crashReporterLearnMore.label    "เรียนรู้เพิ่มเติม">

<!-- Update -->
<!-- LOCALIZATION NOTE (updateApp.label):
  Strings from aboutDialog.dtd are displayed in this section of the preferences.
  Please check for possible accesskey conflicts.
-->
<!ENTITY updateApp2.label                "การอัปเดต &brandShortName;">
<!-- LOCALIZATION NOTE (updateApp.version.*): updateApp.version.pre is
  followed by a version number, keep the trailing space or replace it with
  a different character as needed. updateApp.version.post is displayed after
  the version number, and is empty on purpose for English. You can use it
  if required by your language.
 -->
<!ENTITY updateApp.version.pre           "รุ่น ">
<!ENTITY updateApp.version.post          "">
<!ENTITY updateAppAllow.description      "อนุญาตให้ &brandShortName;">
<!ENTITY updateAuto.label                "ติดตั้งการอัปเดตโดยอัตโนมัติ (แนะนำ: ช่วยเพิ่มความปลอดภัย)">
<!ENTITY updateAuto.accesskey            "ต">
<!ENTITY updateCheck.label               "ตรวจสอบการอัปเดต แต่ให้ฉันเลือกว่าจะติดตั้งหรือไม่">
<!ENTITY updateCheck.accesskey           "ร">
<!ENTITY updateHistory.label             "แสดงประวัติการอัปเดต">
<!ENTITY updateHistory.accesskey         "ส">

<!ENTITY useService.label                "ใช้บริการเบื้องหลังเพื่อติดตั้งการอัปเดต">
<!ENTITY useService.accesskey            "ช">

<!ENTITY updateCrossUserSettingWarning.description "การตั้งค่านี้จะนำไปใช้กับบัญชี Windows ทั้งหมดและโปรไฟล์ &brandShortName; ที่ใช้ &brandShortName; รุ่นที่ติดตั้งนี้">

<!-- Networking and Disk Space -->
<!ENTITY showSettings.label            "การตั้งค่า…">
<!ENTITY showSettings.accesskey        "ก">
<!ENTITY proxiesConfigure.label        "กำหนดค่าวิธีที่ &brandShortName; เชื่อมต่อกับอินเทอร์เน็ต">
<!ENTITY connectionsInfo.caption       "การเชื่อมต่อ">
<!ENTITY offlineInfo.caption           "ออฟไลน์">
<!ENTITY offlineInfo.label             "กำหนดค่าการตั้งค่าออฟไลน์">
<!ENTITY showOffline.label             "ออฟไลน์…">
<!ENTITY showOffline.accesskey         "อ">

<!ENTITY Diskspace                       "เนื้อที่ดิสก์">
<!ENTITY offlineCompactFolders.label     "กระชับโฟลเดอร์ทั้งหมดเมื่อสามารถประหยัดได้มากกว่า">
<!ENTITY offlineCompactFolders.accesskey "ก">
<!ENTITY offlineCompactFoldersMB.label   "MB ของขนาดรวม">

<!-- LOCALIZATION NOTE:
  The entities useCacheBefore.label and useCacheAfter.label appear on a single
  line in preferences as follows:

  &useCacheBefore.label  [ textbox for cache size in MB ]   &useCacheAfter.label;
-->
<!ENTITY useCacheBefore.label            "ใช้ถึง">
<!ENTITY useCacheBefore.accesskey        "ช">
<!ENTITY useCacheAfter.label             "MB ของพื้นที่สำหรับแคช">
<!ENTITY overrideSmartCacheSize.label    "เขียนทับการจัดการแคชอัตโนมัติ">
<!ENTITY overrideSmartCacheSize.accesskey "ข">
<!ENTITY clearCacheNow.label             "ล้างตอนนี้">
<!ENTITY clearCacheNow.accesskey         "ล">

<!-- Certificates -->
<!ENTITY certSelection.description       "เมื่อเซิร์ฟเวอร์ขอใบรับรองส่วนบุคคลของฉัน:">
<!ENTITY certs.auto                      "เลือกมาหนึ่งโดยอัตโนมัติ">
<!ENTITY certs.auto.accesskey            "ล">
<!ENTITY certs.ask                       "ถามฉันทุกครั้ง">
<!ENTITY certs.ask.accesskey             "ถ">
<!ENTITY enableOCSP.label                "สืบค้นเซิร์ฟเวอร์ตอบกลับ OCSP เพื่อยืนยันความถูกต้องของใบรับรองปัจจุบัน">
<!ENTITY enableOCSP.accesskey            "ส">

<!ENTITY manageCertificates.label "จัดการใบรับรอง">
<!ENTITY manageCertificates.accesskey "จ">
<!ENTITY viewSecurityDevices.label "อุปกรณ์ความปลอดภัย">
<!ENTITY viewSecurityDevices.accesskey "ค">

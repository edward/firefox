<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY itemFormatting.label             "การจัดรูปแบบ">
<!ENTITY itemTags.label                   "ป้ายกำกับ">
<!ENTITY itemAdvanced.label               "ขั้นสูง">

<!ENTITY style.label                      "ลักษณะ:">
<!ENTITY style.accesskey                  "ล">
<!ENTITY regularStyle.label               "ธรรมดา">
<!ENTITY bold.label                       "ตัวหนา">
<!ENTITY italic.label                     "ตัวเอียง">
<!ENTITY boldItalic.label                 "ตัวเอียงหนา">
<!ENTITY size.label                       "ขนาด:">
<!ENTITY size.accesskey                   "ข">
<!ENTITY regularSize.label                "ธรรมดา">
<!ENTITY bigger.label                     "ใหญ่">
<!ENTITY smaller.label                    "เล็ก">
<!ENTITY quotedTextColor.label            "สี:">
<!ENTITY quotedTextColor.accesskey        "ส">
<!ENTITY displayWidth.label               "ข้อความตัวอักษรธรรมดา">
<!ENTITY displayText.label                "เมื่อแสดงข้อความตัวอักษรธรรมดาที่ถูกอ้างอิง:">

<!-- LOCALIZATION NOTE : (emoticonsAndStructs.label) 'Emoticons' are also known as 'Smileys', e.g. :-)   -->
<!ENTITY convertEmoticons.label        "แสดงไอคอนสื่ออารมณ์เป็นกราฟิก">
<!ENTITY convertEmoticons.accesskey    "แ">

<!-- labels -->
<!ENTITY displayTagsText.label     "สามารถใช้แท็กเพื่อจัดหมวดหมู่และลำดับความสำคัญของข้อความของคุณได้">
<!ENTITY newTagButton.label        "ใหม่…">
<!ENTITY newTagButton.accesskey    "ม">
<!ENTITY editTagButton1.label      "แก้ไข…">
<!ENTITY editTagButton1.accesskey  "ก">
<!ENTITY removeTagButton.label     "ลบ">
<!ENTITY removeTagButton.accesskey "ล">

<!-- Fonts and Colors -->
<!ENTITY fontsAndColors1.label   "แบบอักษรและสี">
<!ENTITY defaultFont.label       "แบบอักษรเริ่มต้น:">
<!ENTITY defaultFont.accesskey   "บ">
<!ENTITY defaultSize.label       "ขนาด:">
<!ENTITY defaultSize.accesskey   "ข">
<!ENTITY fontOptions.accesskey   "น">
<!ENTITY fontOptions.label       "ขั้นสูง…">
<!ENTITY colorButton.label       "สี…">
<!ENTITY colorButton.accesskey   "ส">

<!-- Advanced -->
<!ENTITY reading.caption                  "การอ่าน">
<!ENTITY display.caption                  "การแสดงผล">
<!ENTITY showCondensedAddresses.label     "แสดงเฉพาะชื่อที่แสดงสำหรับผู้คนในสมุดรายชื่อของฉัน">
<!ENTITY showCondensedAddresses.accesskey "ส">

<!ENTITY autoMarkAsRead.label             "ทำเครื่องหมายข้อความว่าอ่านแล้วโดยอัตโนมัติ">
<!ENTITY autoMarkAsRead.accesskey         "ท">
<!ENTITY markAsReadNoDelay.label          "ทันทีที่แสดง">
<!ENTITY markAsReadNoDelay.accesskey      "แ">
<!-- LOCALIZATION NOTE (markAsReadDelay.label): This will concatenate to
     "After displaying for [___] seconds",
     using (markAsReadDelay.label) and a number (secondsLabel.label). -->
<!ENTITY markAsReadDelay.label            "หลังจากแสดงไปแล้ว">
<!ENTITY markAsReadDelay.accesskey        "ล">
<!ENTITY secondsLabel.label               "วินาที">
<!ENTITY openMsgIn.label                  "เปิดข้อความใน:">
<!ENTITY openMsgInNewTab.label            "แท็บใหม่">
<!ENTITY openMsgInNewTab.accesskey        "บ">
<!ENTITY reuseExpRadio0.label             "หน้าต่างข้อความใหม่">
<!ENTITY reuseExpRadio0.accesskey         "ห">
<!ENTITY reuseExpRadio1.label             "หน้าต่างข้อความที่มีอยู่">
<!ENTITY reuseExpRadio1.accesskey         "น">
<!ENTITY closeMsgOnMoveOrDelete.label     "ปิดหน้าต่าง/แท็บข้อความเมื่อย้ายหรือลบ">
<!ENTITY closeMsgOnMoveOrDelete.accesskey "ป">

<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY  generalTab.label            "ทั่วไป">

<!ENTITY  startupAction.label         "เมื่อ &brandShortName; เริ่ม:">
<!ENTITY  startupAction.accesskey     "ม">
<!ENTITY  startupOffline.label        "คงสถานะออฟไลน์ของบัญชีแชทของฉัน">
<!ENTITY  startupConnectAuto.label    "เชื่อมต่อบัญชีแชทของฉันโดยอัตโนมัติ">

<!-- LOCALIZATION NOTE: reportIdleAfter.label is displayed first, then
there's a field where the user can enter a number, and itemTime is
displayed at the end of the line. The translations of the
reportIdleAfter.label and idleTime parts don't have to mean the exact
same thing as in English; please try instead to translate the whole
sentence. -->
<!ENTITY  reportIdleAfter.label         "แจ้งให้ผู้ติดต่อของฉันทราบว่าฉันไม่ได้ใช้งานหลังจาก">
<!ENTITY  reportIdleAfter.accesskey     "จ">
<!ENTITY  idleTime                      "นาทีที่ไม่มีความเคลื่อนไหว">

<!ENTITY  andSetStatusToAway.label      "และตั้งค่าสถานะของฉันว่าไม่อยู่ด้วยข้อความสถานะนี้:">
<!ENTITY  andSetStatusToAway.accesskey  "ล">

<!ENTITY  sendTyping.label              "ส่งการแจ้งเตือนการพิมพ์ในการสนทนา">
<!ENTITY  sendTyping.accesskey          "ส">

<!ENTITY  chatNotifications.label             "เมื่อมีข้อความส่งมาถึงคุณโดยตรงแล้ว:">
<!ENTITY  desktopChatNotifications.label      "แสดงการแจ้งเตือน:">
<!ENTITY  desktopChatNotifications.accesskey  "แ">
<!ENTITY  completeNotification.label          "พร้อมชื่อผู้ส่งและตัวอย่างข้อความ">
<!ENTITY  buddyInfoOnly.label                 "พร้อมชื่อผู้ส่งเท่านั้น">
<!ENTITY  dummyNotification.label             "โดยไม่มีข้อมูลใด ๆ">
<!ENTITY  getAttention.label                  "กะพริบรายการแถบงาน">
<!ENTITY  getAttention.accesskey              "ก">
<!ENTITY  getAttentionMac.label               "ทำให้ไอคอน Dock เคลื่อนไหว">
<!ENTITY  getAttentionMac.accesskey           "ท">
<!ENTITY  chatSound.accesskey                 "เ">
<!ENTITY  chatSound.label                     "เล่นเสียง">
<!ENTITY  play.label                          "เล่น">
<!ENTITY  play.accesskey                      "น">
<!ENTITY  systemSound.label                   "เสียงระบบเริ่มต้นสำหรับจดหมายใหม่">
<!ENTITY  systemSound.accesskey               "ย">
<!ENTITY  customsound.label                   "ใช้ไฟล์เสียงดังต่อไปนี้">
<!ENTITY  customsound.accesskey               "ช">
<!ENTITY  browse.label                        "เรียกดู…">
<!ENTITY  browse.accesskey                    "ร">

<!ENTITY messageStyleTab.title             "ลักษณะข้อความ">
<!ENTITY messageStylePreview.label         "ตัวอย่าง:">
<!ENTITY messageStyleTheme.label           "ชุดตกแต่ง:">
<!ENTITY messageStyleTheme.accesskey       "ด">
<!ENTITY messageStyleThunderbirdTheme.label "Thunderbird">
<!ENTITY messageStyleBubblesTheme.label    "ฟองอากาศ">
<!ENTITY messageStyleDarkTheme.label       "มืด">
<!ENTITY messageStylePaperSheetsTheme.label "แผ่นกระดาษ">
<!ENTITY messageStyleSimpleTheme.label     "ธรรมดา">
<!ENTITY messageStyleDefaultTheme.label    "ค่าเริ่มต้น">
<!ENTITY messageStyleVariant.label         "รูปแบบอื่น:">
<!ENTITY messageStyleVariant.accesskey     "ป">
<!ENTITY messageStyleShowHeader.label      "แสดงส่วนหัว">
<!ENTITY messageStyleShowHeader.accesskey  "ง">
<!ENTITY messageStyleNoPreview.title       "ไม่มีตัวอย่างที่ใช้งานได้">
<!ENTITY messageStyleNoPreview.description "ชุดตกแต่งนี้ไม่ถูกต้องหรือไม่สามารถใช้งานได้ในขณะนี้ (ปิดใช้งานส่วนเสริม, โหมดปลอดภัย, …)">

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

label-disable = ปิดใช้งาน
label-enable = เปิดใช้งาน
label-more-information = ข้อมูลเพิ่มเติม: ข้อบกพร่อง { $bug }
text-disabled-in-about-config = คุณลักษณะนี้ได้ถูกปิดใช้งานใน about:config
text-title = about:compat

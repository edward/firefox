<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY aboutTelemetry.pingDataSource "Ffynhonnell data ping:">
<!ENTITY aboutTelemetry.showCurrentPingData "Data ping cyfredol">
<!ENTITY aboutTelemetry.showArchivedPingData "Data ping wedi ei archifo">
<!ENTITY aboutTelemetry.showSubsessionData "Dangos data is-sesiwn">
<!ENTITY aboutTelemetry.choosePing "Dewis ping:">
<!ENTITY aboutTelemetry.archivePingType "Math Ping">
<!ENTITY aboutTelemetry.archivePingHeader "Ping">
<!ENTITY aboutTelemetry.optionGroupToday "Heddiw">
<!ENTITY aboutTelemetry.optionGroupYesterday "Ddoe">
<!ENTITY aboutTelemetry.optionGroupOlder "Hŷn">
<!-- LOCALIZATION NOTE(aboutTelemetry.previousPing, aboutTelemetry.nextPing):
	These strings are displayed when selecting Archived pings, and they’re
	used to move to the next or previous ping. -->
<!ENTITY aboutTelemetry.previousPing "&lt;&lt;">
<!ENTITY aboutTelemetry.nextPing "&gt;&gt;">

<!ENTITY aboutTelemetry.pageTitle "Data Telemetreg">
<!ENTITY aboutTelemetry.moreInformations "Chwilio am ragor o wybodaeth?">
<!ENTITY aboutTelemetry.firefoxDataDoc "
Mae'r <a>Firefox Data Documentation</a> yn cynnwys canllawiau ar sut i weithio gyda'n offer data.
">
<!ENTITY aboutTelemetry.telemetryClientDoc "
Mae <a>dogfennaeth cleient Firefox Telemetry</a> yn cynnwys diffiniadau o gysyniadau, dogfennaeth API a chyfeiriadau data.">
<!ENTITY aboutTelemetry.telemetryDashboard "
Mae <a>byrddau gwaith Telemetreg</a> yn caniatáu i chi weld y data mae Mozilla yn ei dderbyn drwy'r Delemetreg.">

<!ENTITY aboutTelemetry.telemetryProbeDictionary "Mae'r <a>Geiriadur Archwilio</a> yn darparu manylion a disgrifiadau ar gyfer y chwilio gasglwyd gan Delemetreg.">

<!ENTITY aboutTelemetry.showInFirefoxJsonViewer "Agor yn y darllenydd JSON">

<!ENTITY aboutTelemetry.homeSection "Cartref">
<!ENTITY aboutTelemetry.generalDataSection "  Data Cyffredinol">
<!ENTITY aboutTelemetry.environmentDataSection "  Data'r Amgylchedd">
<!ENTITY aboutTelemetry.sessionInfoSection "  Manylion Sesiwn">
<!ENTITY aboutTelemetry.scalarsSection "  Scalarau">
<!ENTITY aboutTelemetry.keyedScalarsSection "  Graddfeydd Allweddedig">
<!ENTITY aboutTelemetry.histogramsSection "  Histogramau">
<!ENTITY aboutTelemetry.keyedHistogramsSection "  Histogramau Allweddol">
<!ENTITY aboutTelemetry.eventsSection "  Digwyddiadau">
<!ENTITY aboutTelemetry.simpleMeasurementsSection "  Mesuriadau Syml">
<!ENTITY aboutTelemetry.slowSqlSection "  Datganiadau SQL Araf">
<!ENTITY aboutTelemetry.addonDetailsSection "  Manylion Ychwanegyn">
<!ENTITY aboutTelemetry.capturedStacksSection "  Staciau wedi eu Cipio">
<!ENTITY aboutTelemetry.lateWritesSection "  Ysgrifennu Hwyr">
<!ENTITY aboutTelemetry.rawPayloadSection "Llwyth Bras">
<!ENTITY aboutTelemetry.raw "JSON bras">

<!ENTITY aboutTelemetry.fullSqlWarning "  SYLW: Mae dadfygio SQL araf wedi ei alluogi. Gall llinynnau SQL llawn gael eu dangos isod ond ni fyddant yn cael eu trosglwyddo i'r Telemetreg.">
<!ENTITY aboutTelemetry.fetchStackSymbols "  Estyn enwau swyddogaethau ar gyfer pentyrrau">
<!ENTITY aboutTelemetry.hideStackSymbols "  Dangos data pentwr bras">

<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY aboutTelemetry.pingDataSource "Patikrinti duomenų šaltinio ryšį:">
<!ENTITY aboutTelemetry.showCurrentPingData "Dabartinio ryšio patikrinimo duomenys">
<!ENTITY aboutTelemetry.showArchivedPingData "Archyve esantys ryšio tikrinimo duomenys">
<!ENTITY aboutTelemetry.showSubsessionData "Rodyti posesijinius duomenis">
<!ENTITY aboutTelemetry.choosePing "Pasirinkite ryšio patikrinimą:">
<!ENTITY aboutTelemetry.archivePingType "Ryšio tikrinimo tipas">
<!ENTITY aboutTelemetry.archivePingHeader "Ryšio patikrinimas">
<!ENTITY aboutTelemetry.optionGroupToday "Šiandien">
<!ENTITY aboutTelemetry.optionGroupYesterday "Vakar">
<!ENTITY aboutTelemetry.optionGroupOlder "Senesni">
<!-- LOCALIZATION NOTE(aboutTelemetry.previousPing, aboutTelemetry.nextPing):
	These strings are displayed when selecting Archived pings, and they’re
	used to move to the next or previous ping. -->
<!ENTITY aboutTelemetry.previousPing "&lt;&lt;">
<!ENTITY aboutTelemetry.nextPing "&gt;&gt;">

<!ENTITY aboutTelemetry.pageTitle "Telemetrijos duomenys">
<!ENTITY aboutTelemetry.moreInformations "Ieškote daugiau informacijos?">
<!ENTITY aboutTelemetry.firefoxDataDoc "<a>„Firefox“ duomenų dokumentacijoje</a> rasite pagalbos apie tai, kaip dirbti su mūsų duomenų įrankiais.">
<!ENTITY aboutTelemetry.telemetryClientDoc "<a>„Firefox“ telemetrijos kliento dokumentacijoje</a> rasite sąvokų apibrėžimus, API dokumentaciją bei duomenų rodyklę.">
<!ENTITY aboutTelemetry.telemetryDashboard "<a>Telemetrijos skydeliai</a> leidžia jums aiškiai matyti „Mozillai“ siunčiamus duomenis.">

<!ENTITY aboutTelemetry.telemetryProbeDictionary "<a>Zondų žodynas</a> pateikia telemetrijos surinktų zondų informaciją ir aprašus.">

<!ENTITY aboutTelemetry.showInFirefoxJsonViewer "Atverti JSON žiūrykle">

<!ENTITY aboutTelemetry.homeSection "Pradžia">
<!ENTITY aboutTelemetry.generalDataSection "  Įprasti duomenys">
<!ENTITY aboutTelemetry.environmentDataSection "  Aplinkos duomenys">
<!ENTITY aboutTelemetry.sessionInfoSection "  Seanso informacija">
<!ENTITY aboutTelemetry.scalarsSection "  Skaliarai">
<!ENTITY aboutTelemetry.keyedScalarsSection "  Raktiniai skaliarai">
<!ENTITY aboutTelemetry.histogramsSection "  Histogramos">
<!ENTITY aboutTelemetry.keyedHistogramsSection "  Raktinės histogramos">
<!ENTITY aboutTelemetry.eventsSection "  Įvykiai">
<!ENTITY aboutTelemetry.simpleMeasurementsSection "  Paprasti matavimai">
<!ENTITY aboutTelemetry.slowSqlSection "  Lėti SQL sakiniai">
<!ENTITY aboutTelemetry.addonDetailsSection "  Priedų duomenys">
<!ENTITY aboutTelemetry.capturedStacksSection "  Įrašyti dėklai">
<!ENTITY aboutTelemetry.lateWritesSection "  Vėlavę įrašymai">
<!ENTITY aboutTelemetry.rawPayloadSection "Neapdorotas turinys">
<!ENTITY aboutTelemetry.raw "Pirminis JSON">

<!ENTITY aboutTelemetry.fullSqlWarning "PASTABA: Įjungtas lėtų SQL sakinių derinimas. Žemiau gali būti rodomi pilni SQL sakiniai, tačiau jie nebus pateikiami telemetrijai.">
<!ENTITY aboutTelemetry.fetchStackSymbols "  Rinkti funkcijų vardus dėklams">
<!ENTITY aboutTelemetry.hideStackSymbols "  Rodyti neapdorotus dėklų duomenis">

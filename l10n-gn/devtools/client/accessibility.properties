# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE These strings are used inside the Accessibility panel
# which is available from the Web Developer sub-menu -> 'Accessibility'.
# The correct localization of this file might be to keep it in
# English, or another language commonly spoken among web developers.
# You want to make that choice consistent across the developer tools.
# A good criteria is the language in which you'd find the best
# documentation on web development on the web.

# LOCALIZATION NOTE (accessibility.role): A title text used for Accessibility
# tree header column that represents accessible element role.
accessibility.role=Apoha

# LOCALIZATION NOTE (accessibility.name): A title text used for Accessibility
# tree header column that represents accessible element name.
accessibility.name=Téra

# LOCALIZATION NOTE (accessibility.logo): A title text used for Accessibility
# logo used on the accessibility panel landing page.
accessibility.logo=Ta’anga’i jeikerãva

# LOCALIZATION NOTE (accessibility.properties): A title text used for header
# for Accessibility details sidebar.
accessibility.properties=Mba'etee

# LOCALIZATION NOTE (accessibility.treeName): A title text used for
# Accessibility tree (that represents accessible element name) container.
accessibility.treeName=Yvyra jeikerãva

# LOCALIZATION NOTE (accessibility.accessible.notAvailable): A title text
# displayed when accessible sidebar panel does not have an accessible object to
# display.
accessibility.accessible.notAvailable=Ne marandu oĩva

# LOCALIZATION NOTE (accessibility.enable): A title text for Enable
# accessibility button used to enable accessibility service.
accessibility.enable=Embojuruja jeikerã rehegua

# LOCALIZATION NOTE (accessibility.enabling): A title text for Enable
# accessibility button used when accessibility service is being enabled.
accessibility.enabling=Embojurujahína jeikerã rehegua…

# LOCALIZATION NOTE (accessibility.disable): A title text for Disable
# accessibility button used to disable accessibility service.
accessibility.disable=Eipe’a jeikerã rehegua

# LOCALIZATION NOTE (accessibility.disabling): A title text for Disable
# accessibility button used when accessibility service is being
# disabled.
accessibility.disabling=Eipe’ahína jeikerã rehegua…

# LOCALIZATION NOTE (accessibility.pick): A title text for Picker button
# button used to pick accessible objects from the page.
accessibility.pick=Eiporavo peteĩ mba’e kuatiaroguépe oikekuaáva

# LOCALIZATION NOTE (accessibility.disable.disabledTitle): A title text used for
# a tooltip for Disable accessibility button when accessibility service can not
# be disabled. It is the case when a user is using a 3rd party accessibility
# tool such as screen reader.
accessibility.disable.disabledTitle=Jeikekuaaha rape ndaikatúi oñembogue. Ojepuruhína okápe mboguatahára rembipurúgui.

# LOCALIZATION NOTE (accessibility.disable.enabledTitle): A title text used for
# a tooltip for Disable accessibility button when accessibility service can be
# disabled.
accessibility.disable.enabledTitle=Jeikekuaaha rape oguéta opaite tendayke ha ovetãme g̃uarã.

# LOCALIZATION NOTE (accessibility.enable.disabledTitle): A title text used for
# a tooltip for Enabled accessibility button when accessibility service can not
# be enabled.
accessibility.enable.disabledTitle=Jeikekuaaha rape ndaikatúi oñemyendy. Oñemboguéta ñemigua erohoryvéva jeikekuaaha rape rupi.

# LOCALIZATION NOTE (accessibility.enable.enabledTitle): A title text used for
# a tooltip for Enabled accessibility button when accessibility service can be
# enabled.
accessibility.enable.enabledTitle=Jeikekuaaha rape hendýta opaite tendayke ha ovetãme g̃uarã.

# LOCALIZATION NOTE (accessibility.learnMore): A text that is used as is or as textual
# description in places that link to accessibility inspector documentation.
accessibility.learnMore=Kuaave

# LOCALIZATION NOTE (accessibility.description.general): A title text used when
# accessibility service description is provided before accessibility inspector
# is enabled.
accessibility.description.general=Pe jeikekuaaha rembiapoite oñembogue hína ijypykue rupi nomba’apo porãi haguére. Ikatu hína embogue jeikekuaaha rembiapoite eipuru mboyve ambue tembipuru rupa mboguataharakuérape g̃uarãva.

# LOCALIZATION NOTE (accessibility.description.general.p1): A title text for the first
# paragraph, used when accessibility service description is provided before accessibility
# inspector is enabled. %S in the content will be replaced by a link at run time
# with the accessibility.learnMore string.
accessibility.description.general.p1=Pe jeike mbohekoha omoneĩ ñema’ẽag̃ui kuatiarogue ag̃agua jeikeha reheguáre, oipurúva mba’erechaha moñe’ẽha ha ambue tembipurupyahu pytyvõrãva. %S

# LOCALIZATION NOTE (accessibility.description.general.p2): A title text for the second
# paragraph, used when accessibility service description is provided before accessibility
# inspector is enabled.
accessibility.description.general.p2=Umi tembiapoite jeikeha rehegua ikatu ombyai ambue mboguatahára rembipuru mba’erupa rembiapo ha ojeipe’ava’erã ndojepurúi jave.

# LOCALIZATION NOTE (accessibility.description.oldVersion): A title text used
# when accessibility service description is provided when a client is connected
# to an older version of accessibility actor.
accessibility.description.oldVersion=Kóva ojuaju hína peteĩ mohendahavusu mopotĩha itujámava rehe. Eipuru hag̃ua pe jeikekuaaha rupa, ikatúpa eipuru pe mohendahavusu mopotĩha ipyahuvéva.

# LOCALIZATION NOTE (accessibility.tree.menu.printToJSON): A title text used when a
# context menu item for printing an accessible tree to JSON is rendered after triggering a
# context menu for an accessible tree row.
accessibility.tree.menu.printToJSON=Emonguatia JSON-pe

# LOCALIZATION NOTE (accessibility.checks): A title text used for header for checks
# section in Accessibility details sidebar.
accessibility.checks=Jehechajey

# LOCALIZATION NOTE (accessibility.checks.empty): A title text used for indicating that
# accessibility checks for a node yielded no results and another node should be
# selected.
accessibility.checks.empty=Eiporavo ambue mohendaha eku’ejey hag̃ua.

# LOCALIZATION NOTE (accessibility.contrast.header): A title text used for header for
# checks related to color and contrast.
accessibility.contrast.header=Sa’y ha sa’yambue

# LOCALIZATION NOTE (accessibility.contrast.error): A title text for the color
# contrast ratio, used when the tool is unable to calculate the contrast ratio value.
accessibility.contrast.error=Ndaikatúi ojeikuaa

# LOCALIZATION NOTE (accessibility.contrast.large.text): A title text for the color
# contrast ratio label indicating that the color contrast criteria used is if for large
# text. This is lower case because it's used as a label for a tree item in accessibility
# tree.
accessibility.contrast.large.text=moñe’ẽrã tuicháva

# LOCALIZATION NOTE (accessibility.contrast.large.title): A title text for the tooltip
# used for the large text label (see accessibility.contrast.large.text).
accessibility.contrast.large.title=Pe moñe’ẽrã ha’e 14 kyta ha isa’y hũ, 18 kyta térã tuichave.

# LOCALIZATION NOTE (accessibility.contrast.annotation.AA): A title text for the paragraph
# describing that the given colour contrast satisfies AA standard from Web Content
# Accessibility Guidelines. %S in the content will be replaced by a link at run time
# with the accessibility.learnMore string.
accessibility.contrast.annotation.AA=Ojapo WCAG AA rekovoña he’íva moñe’ẽrã hasy’ỹvape g̃uarã. %S

# LOCALIZATION NOTE (accessibility.contrast.annotation.AAA): A title text for the
# paragraph describing that the given colour contrast satisfies AAA standard from Web
# Content Accessibility Guidelines. %S in the content will be replaced by a link at run
# time with the accessibility.learnMore string.
accessibility.contrast.annotation.AAA=Ojapo WCAG AAA rekovoña he’íva moñe’ẽrã hasy’ỹvape g̃uarã. %S

# LOCALIZATION NOTE (accessibility.contrast.annotation.fail): A title text for the
# paragraph describing that the given colour contrast fails to meet the minimum level from
# Web Content Accessibility Guidelines. %S in the content will be replaced by a link at
# run time with the accessibility.learnMore string.
accessibility.contrast.annotation.fail=Ojapo WCAG rekovoña he’íva moñe’ẽrã hasy’ỹvape g̃uarã. %S

<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY itemFormatting.label             "Định dạng">
<!ENTITY itemTags.label                   "Thẻ">
<!ENTITY itemAdvanced.label               "Nâng cao">

<!ENTITY style.label                      "Kiểu:">
<!ENTITY style.accesskey                  "y">
<!ENTITY regularStyle.label               "Thông thường">
<!ENTITY bold.label                       "Đậm">
<!ENTITY italic.label                     "Nghiêng">
<!ENTITY boldItalic.label                 "Nghiêng đậm">
<!ENTITY size.label                       "Kích thước:">
<!ENTITY size.accesskey                   "z">
<!ENTITY regularSize.label                "Thông thường">
<!ENTITY bigger.label                     "Lớn hơn">
<!ENTITY smaller.label                    "Nhỏ hơn">
<!ENTITY quotedTextColor.label            "Màu sắc:">
<!ENTITY quotedTextColor.accesskey        "o">
<!ENTITY displayWidth.label               "Thư văn bản thuần túy">
<!ENTITY displayText.label                "Khi hiển thị các thư văn bản thuần túy được trích dẫn:">

<!-- LOCALIZATION NOTE : (emoticonsAndStructs.label) 'Emoticons' are also known as 'Smileys', e.g. :-)   -->
<!ENTITY convertEmoticons.label        "Hiển thị biểu tượng cảm xúc dưới dạng đồ họa">
<!ENTITY convertEmoticons.accesskey    "e">

<!-- labels -->
<!ENTITY displayTagsText.label     "Thẻ có thể được sử dụng để phân loại và ưu tiên thư của bạn.">
<!ENTITY newTagButton.label        "Mới…">
<!ENTITY newTagButton.accesskey    "N">
<!ENTITY editTagButton1.label      "Chỉnh sửa…">
<!ENTITY editTagButton1.accesskey  "E">
<!ENTITY removeTagButton.label     "Xóa">
<!ENTITY removeTagButton.accesskey "D">

<!-- Fonts and Colors -->
<!ENTITY fontsAndColors1.label   "Phông chữ &amp; màu sắc">
<!ENTITY defaultFont.label       "Phông chữ mặc định:">
<!ENTITY defaultFont.accesskey   "D">
<!ENTITY defaultSize.label       "Kích cỡ:">
<!ENTITY defaultSize.accesskey   "S">
<!ENTITY fontOptions.accesskey   "A">
<!ENTITY fontOptions.label       "Nâng cao…">
<!ENTITY colorButton.label       "Màu sắc…">
<!ENTITY colorButton.accesskey   "C">

<!-- Advanced -->
<!ENTITY reading.caption                  "Đọc">
<!ENTITY display.caption                  "Hiển thị">
<!ENTITY showCondensedAddresses.label     "Chỉ hiển thị tên cho những người trong sổ địa chỉ của tôi">
<!ENTITY showCondensedAddresses.accesskey "S">

<!ENTITY autoMarkAsRead.label             "Tự động đánh dấu thư là đã đọc">
<!ENTITY autoMarkAsRead.accesskey         "A">
<!ENTITY markAsReadNoDelay.label          "Ngay lập tức trên màn hình">
<!ENTITY markAsReadNoDelay.accesskey      "o">
<!-- LOCALIZATION NOTE (markAsReadDelay.label): This will concatenate to
     "After displaying for [___] seconds",
     using (markAsReadDelay.label) and a number (secondsLabel.label). -->
<!ENTITY markAsReadDelay.label            "Sau khi hiển thị">
<!ENTITY markAsReadDelay.accesskey        "d">
<!ENTITY secondsLabel.label               "giây">
<!ENTITY openMsgIn.label                  "Mở thư trong:">
<!ENTITY openMsgInNewTab.label            "Một thẻ mới">
<!ENTITY openMsgInNewTab.accesskey        "t">
<!ENTITY reuseExpRadio0.label             "Một cửa sổ thư mới">
<!ENTITY reuseExpRadio0.accesskey         "n">
<!ENTITY reuseExpRadio1.label             "Một cửa sổ thư hiện có">
<!ENTITY reuseExpRadio1.accesskey         "e">
<!ENTITY closeMsgOnMoveOrDelete.label     "Đóng cửa sổ thư/thẻ khi di chuyển hoặc xóa">
<!ENTITY closeMsgOnMoveOrDelete.accesskey "C">

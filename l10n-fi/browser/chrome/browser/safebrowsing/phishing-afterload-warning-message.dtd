<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "Takaisin">
<!ENTITY safeb.palm.seedetails.label "Näytä yksityiskohdat">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "Tämä ei ole petollinen sivusto…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "p">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "Varoituksen antoi <a id='advisory_provider'/>.">


<!ENTITY safeb.blocked.malwarePage.title2 "Tälle sivustolle siirtyminen voi vahingoittaa tietokonettasi">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "&brandShortName; esti tämän sivun, koska se yritti asentaa haittaohjelmaa, joka voi varastaa tai poistaa tietokoneellasi olevia tietoja.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "Sivuston <span id='malware_sitename'/> on <a id='error_desc_link'>ilmoitettu sisältävän haittaohjelmia</a>. Voit <a id='report_detection'>ilmoittaa tunnistusongelmasta</a> tai <a id='ignore_warning_link'>jättää riskin huomiotta</a> ja siirtyä turvattomalle sivustolle.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "Sivuston <span id='malware_sitename'/> on <a id='error_desc_link'>ilmoitettu sisältävän haittaohjelmia</a>. Voit <a id='report_detection'>ilmoittaa tunnistusongelmasta</a>.">

<!ENTITY safeb.blocked.malwarePage.learnMore "Lue lisää haitallisesta verkkosisällöstä, kuten viruksista ja haittaohjelmista, sekä keinoista, joilla voit suojata tietokonettasi, osoitteessa <a id='learn_more_link'>StopBadware.org</a>. Lue lisää &brandShortName;in verkkourkinnalta ja haittaohjelmilta suojauksesta osoitteessa <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.unwantedPage.title2 "Edessä olevalla sivustolla voi olla haittaohjelmia">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "&brandShortName; esti tämän sivun, koska se saattaa huijata sinut asentamaan ohjelmia, jotka voivat haitata verkon selaamista, esimerkiksi vaihtamalla aloitussivun tai näyttämällä ylimääräisiä mainoksia vierailemillasi sivustoilla.">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "Sivustolla <span id='unwanted_sitename'/> on <a id='error_desc_link'>ilmoitettu olevan haitallisia ohjelmia</a>. Voit <a id='ignore_warning_link'>jättää riskin huomiotta</a> ja siirtyä turvattomalle sivustolle.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "Sivustolla <span id='unwanted_sitename'/> on <a id='error_desc_link'>ilmoitettu olevan haitallisia ohjelmia</a>.">

<!ENTITY safeb.blocked.unwantedPage.learnMore "Lue lisää haitallisista ja ei-halutuista ohjelmista (englanniksi) <a id='learn_more_link'>Unwanted Software Policy</a> -tekstistä. Lue lisää &brandShortName;in verkkourkinnalta ja haittaohjelmilta suojauksesta osoitteessa <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.phishingPage.title3 "Petollinen sivusto edessä">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "&brandShortName; esti tämän sivun, koska se saattaa huijata sinut tekemään jotain vaarallista, kuten asentamaan ohjelmia tai paljastamaan henkilötietoja kuten salasanoja tai luottokorttitietoja.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "Sivusto <span id='phishing_sitename'/> on <a id='error_desc_link'>ilmoitettu petolliseksi</a>. Voit <a id='report_detection'>ilmoittaa tunnistusongelmasta</a> tai <a id='ignore_warning_link'>jättää riskin huomiotta</a> ja siirtyä turvattomalle sivustolle.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "Sivusto <span id='phishing_sitename'/> on <a id='error_desc_link'>ilmoitettu petolliseksi</a>. Voit <a id='report_detection'>ilmoittaa tunnistusongelmasta</a>.">

<!ENTITY safeb.blocked.phishingPage.learnMore "Lue lisää petollisista sivustoista ja verkkourkinnasta osoitteessa <a id='learn_more_link'>www.antiphishing.org</a>. Lue lisää &brandShortName;in verkkourkinnalta ja haittaohjelmilta suojauksesta osoitteessa <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.harmfulPage.title "Edessä olevalla sivustolla voi olla haittaohjelmia">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "&brandShortName; esti tämän sivun, koska se saattaa yrittää asentaa vaarallisia ohjelmia, jotka varastavat tai poistavat tietoja, esimerkiksi kuvia, salasanoja, viestejä ja luottokorttitietoja.">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "Sivuston <span id='harmful_sitename'/> on <a id='error_desc_link'>ilmoitettu sisältävän mahdollisesti haitallisen ohjelman</a>. Voit <a id='ignore_warning_link'>jättää riskin huomiotta</a> ja siirtyä turvattomalle sivustolle.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "Sivuston <span id='harmful_sitename'/> on <a id='error_desc_link'>ilmoitettu sisältävän mahdollisesti haitallisen ohjelman</a>.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "Lue lisää &brandShortName;in verkkourkinnalta ja haittaohjelmilta suojauksesta <a id='firefox_support'>support.mozilla.orgissa</a>.">

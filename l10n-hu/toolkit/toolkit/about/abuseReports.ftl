# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

abuse-report-subtitle = Mi a probléma?
# Variables:
#   $author-name (string) - Name of the add-on author
abuse-report-addon-authored-by = szerző: <a data-l10n-name="author-name">{ $author-name }</a>
abuse-report-learnmore =
    Nem tudja, hogy melyik problémát válassza ki?
    <a data-l10n-name="learnmore-link">További információk a bővítmények és témák jelentéséről</a>
abuse-report-submit-description = Írja le a problémát (nem kötelező)

## Panel buttons.

abuse-report-cancel-button = Mégse
abuse-report-next-button = Tovább
abuse-report-goback-button = Ugrás vissza
abuse-report-submit-button = Elküldés

## Message bars descriptions.


## Variables:
##   $addon-name (string) - Name of the add-on


## Message bars actions.

abuse-report-messagebar-action-remove = Igen, távolítsa el
abuse-report-messagebar-action-keep = Nem, megtartom
abuse-report-messagebar-action-retry = Újra
abuse-report-messagebar-action-cancel = Mégse

## Abuse report reasons (optionally paired with related examples and/or suggestions)

abuse-report-spam-example = Példa: Hirdetések beszúrása weboldalakba
abuse-report-settings-reason = Módosította a keresőszolgáltatásomat, kezdőlapomat vagy az új lap oldalamat a tájékoztatásom vagy megkérdezésem nélkül
abuse-report-settings-suggestions = A kiegészítő bejelentése előtt megpróbálhatja megváltoztatni a beállításait:
abuse-report-policy-reason = Gyűlölködő, erőszakos vagy illegális tartalom
abuse-report-other-reason = Valami más

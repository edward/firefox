# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Variables:
#   $name (String) - Display name or URL for the DNS over HTTPS provider
connection-dns-over-https-url-item-default =
    .label = { $name } (privzet)
    .tooltiptext = Uporabi privzeti URL za razreševanje DNS preko HTTPS
connection-dns-over-https-url-custom =
    .label = Po meri
    .accesskey = P
    .tooltiptext = Vnesite želeni URL za razreševanje DNS preko HTTPS
connection-dns-over-https-custom-label = Po meri

<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY aboutPrivateBrowsing.notPrivate                 "Práve nie ste v režime Súkromné prehliadanie">
<!ENTITY privatebrowsingpage.openPrivateWindow.label     "Otvoriť Súkromné okno">
<!ENTITY privatebrowsingpage.openPrivateWindow.accesskey "S">

<!ENTITY privateBrowsing.title                           "Súkromné prehliadanie">
<!ENTITY aboutPrivateBrowsing.info.notsaved.before       "Pri prehliadaní v Súkromnom okne Firefox ">
<!ENTITY aboutPrivateBrowsing.info.notsaved.emphasize    "neukladá">
<!ENTITY aboutPrivateBrowsing.info.notsaved.after        ":">
<!ENTITY aboutPrivateBrowsing.info.visited               "navštívené stránky">
<!ENTITY aboutPrivateBrowsing.info.searches              "vyhľadávania">
<!ENTITY aboutPrivateBrowsing.info.cookies               "súbory cookies">
<!ENTITY aboutPrivateBrowsing.info.temporaryFiles        "dočasné súbory">
<!ENTITY aboutPrivateBrowsing.info.saved.before          "Firefox ">
<!ENTITY aboutPrivateBrowsing.info.saved.emphasize       "bude ukladať">
<!ENTITY aboutPrivateBrowsing.info.saved.after2          " vaše:">
<!ENTITY aboutPrivateBrowsing.info.downloads             "prevzaté súbory">
<!ENTITY aboutPrivateBrowsing.info.bookmarks             "záložky">
<!ENTITY aboutPrivateBrowsing.info.clipboard             "skopírovaný text">
<!ENTITY aboutPrivateBrowsing.note.before                "Režim Súkromné prehliadanie ">
<!ENTITY aboutPrivateBrowsing.note.emphasize             "vás neanonymizuje">
<!ENTITY aboutPrivateBrowsing.note.after                 " na internete. Váš zamestnávateľ alebo poskytovateľ internetového pripojenia môžu stále zistiť, aké stránky ste navštívili.">
<!ENTITY aboutPrivateBrowsing.learnMore3.before          "Ďalšie informácie o ">
<!ENTITY aboutPrivateBrowsing.learnMore3.title           "režime Súkromné prehliadanie">
<!ENTITY aboutPrivateBrowsing.learnMore3.after           ".">

<!ENTITY trackingProtection.startTour1                   "Pozrite sa ako to funguje">

<!ENTITY contentBlocking.title                           "Blokovanie obsahu">
<!ENTITY contentBlocking.description                     "Niektoré webové stránky používajú sledovacie prvky, ktoré sledujú vašu aktivitu na internete. Blokovanie obsahu vo Firefoxe automaticky blokuje veľké množstvo týchto sledovacích prvkov, ktoré môžu zbierať informácie o vašom prehliadaní.">

<!-- Strings for new Private Browsing with Search -->
<!-- LOCALIZATION NOTE (aboutPrivateBrowsing.search.placeholder): This is the placeholder
                       text for the search box.   -->
<!ENTITY aboutPrivateBrowsing.search.placeholder         "Hľadať">
<!ENTITY aboutPrivateBrowsing.info.title                 "Ste v súkromnom okne">
<!ENTITY aboutPrivateBrowsing.info.description           "&brandShortName; vymaže históriu vyhľadávania a navštívených stránok po zavretí aplikácie alebo všetkých súkromných kariet a okien. S touto funkciou nie ste na internete neviditeľní a napríklad váš poskytovateľ pripojenia na internet môže stále zistiť, aké stránky navštevujete. Vaša aktivita na internete ale zostane utajená pred ďalšími používateľmi na tomto počítači.">
<!-- LOCALIZATION NOTE (aboutPrivateBrowsing.info.myths): This is a link to a SUMO article
                       about private browsing myths.   -->
<!ENTITY aboutPrivateBrowsing.info.myths                 "Časté omyly o súkromnom prehliadaní">

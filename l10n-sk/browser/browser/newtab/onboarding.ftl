# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = Vyskúšajte to
onboarding-button-label-get-started = Začíname
onboarding-welcome-header = Víta vás { -brand-short-name }
onboarding-start-browsing-button-label = Začať prehliadať

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = Súkromné prehliadanie
onboarding-private-browsing-text = Buďte na internete v súkromí. Súkromné prehliadanie vám spolu s funkciou blokovania obsahu zabezpečia ochranu pred sledovacími prvkami.
onboarding-screenshots-title = Snímky obrazovky
onboarding-screenshots-text = Tvorte, ukladajte a zdieľajte snímky obrazovky priamo v aplikácii { -brand-short-name }. Vyberte celú stránku alebo jej časť v priebehu prehliadania a uložte si jej snímku na web pre jednoduché zdieľanie a neskoršie zobrazenie.
onboarding-addons-title = Doplnky
onboarding-addons-text = Pridajte si do aplikácie { -brand-short-name } ďalšie funkcie. Porovnávajte ceny, pozrite si predpoveď počasia alebo si upravte prehliadač pomocou témy vzhľadu.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Prehliadajte rýchlejšie, inteligentnejšie a bezpečnejšie s doplnkami, ako je Ghostery, ktoré vám umožňujú blokovať otravné reklamy.
# Note: "Sync" in this case is a generic verb, as in "to synchronize"
onboarding-fxa-title = Synchronizácia
onboarding-fxa-text = Prihláste sa ku svojmu účtu Firefox a synchronizujte svoje záložky, heslá a otvorené karty kdekoľvek používate aplikáciu { -brand-short-name }.

## Message strings belonging to the Return to AMO flow

return-to-amo-sub-header = Skvelé, odteraz máte { -brand-short-name }
# <icon></icon> will be replaced with the icon belonging to the extension
#
# Variables:
#   $addon-name (String) - Name of the add-on
return-to-amo-addon-header = Teraz naspäť k doplnku <icon></icon><b>{ $addon-name }.</b>
return-to-amo-extension-button = Pridať rozšírenie
return-to-amo-get-started-button = Začíname s aplikáciou { -brand-short-name }

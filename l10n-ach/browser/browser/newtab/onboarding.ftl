# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = Tem kombedi
onboarding-button-label-get-started = Caki
onboarding-welcome-header = Wajoli i { -brand-short-name }
onboarding-start-browsing-button-label = Cak yeny

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = Yeny i mung
onboarding-private-browsing-text = Yeny piri keni. Yeny i mung ki Lageng Jami gengo lulub kor me iwiyamo ma lubo kor in kaweng i kakube.
onboarding-addons-title = Med-ikome
onboarding-addons-text = Med jami mapol ma weko { -brand-short-name } tiyo matek piri. Po wel, rot piny kit ma tye kwede onyo nyut kiti ki theme ma iyubo piri.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Yeny oyot, ki ngec, onyo maber ki lamed calo Ghostery, ma weko i gengo kwena cato wil ma cwero yii
onboarding-fxa-text = Coone pi { -fxaccount-brand-name } ka i rib alamabuk, mung me donyo, ki yab dirica matino i kabedo mo keken ma itiyo ki { -brand-short-name }.

## Message strings belonging to the Return to AMO flow

return-to-amo-sub-header = Ber matek, itye ki { -brand-short-name }
# <icon></icon> will be replaced with the icon belonging to the extension
#
# Variables:
#   $addon-name (String) - Name of the add-on
return-to-amo-addon-header = Kombedi dong wek wanongi <icon></icon><b>{ $addon-name }.</b>
return-to-amo-extension-button = Med Lamed
return-to-amo-get-started-button = Cak ki { -brand-short-name }

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-private-browsing-learn-more = Saznajte više o <a data-l10n-name="learn-more">Privatnom surfanju</a>.
about-private-browsing-info-visited = posjećene stranice
privatebrowsingpage-open-private-window-label = Otvori Privatni prozor
    .accesskey = r
about-private-browsing-info-notsaved = Kada surfate u privatnom prozoru, { -brand-short-name } <strong>ne spašava</strong>:
about-private-browsing-info-bookmarks = zabilješke
about-private-browsing-info-searches = pretrage
about-private-browsing-info-downloads = preuzimanja
private-browsing-title = Privatno surfanje
about-private-browsing-info-saved = { -brand-short-name } <strong>će spasiti</strong> vaša:
about-private-browsing-info-temporary-files = privremene fajlove
about-private-browsing-info-cookies = kolačiće
tracking-protection-start-tour = Pogledajte kako ovo radi
about-private-browsing-note = Privatno surfanje <strong>vas ne čini anonimnim</strong> na internetu. Vaš poslodavac ili pružatelj usluge interneta i dalje mogu znati koje stranice posjećujete.
about-private-browsing-not-private = Trenutno niste u privatnom prozoru.

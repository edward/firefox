<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "Firefox Sync">
<!ENTITY syncBrand.shortName.label "Sync">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label 'Conectar a &syncBrand.shortName.label;'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'Para activar tu nuevo dispositivo, selecciona \&quot;Configurar &syncBrand.shortName.label;\&quot; en el dispositivo.'>
<!ENTITY sync.subtitle.pair.label 'Para activar, selecciona “Emparejar un dispositivo” en tu otro dispositivo.'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'No tengo el dispositivo conmigo…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'Inicios de sesión'>
<!ENTITY sync.configure.engines.title.history 'Historial'>
<!ENTITY sync.configure.engines.title.tabs 'Pestañas'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS1; sobre &formatS2;'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'Menú de marcadores'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'Etiquetas'>
<!ENTITY bookmarks.folder.toolbar.label 'Barra de herramientas'>
<!ENTITY bookmarks.folder.other.label 'Otros marcadores'>
<!ENTITY bookmarks.folder.desktop.label 'Marcadores de escritorio'>
<!ENTITY bookmarks.folder.mobile.label 'Marcadores móviles'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'Fijado'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'Regresar a la navegación'>

<!ENTITY fxaccount_getting_started_welcome_to_sync 'Bienvenido a &syncBrand.shortName.label;'>
<!ENTITY fxaccount_getting_started_description2 'Inicia sesión para sincronizar tus pestañas, marcadores, contraseñas y más.'>
<!ENTITY fxaccount_getting_started_get_started 'Iniciar'>
<!ENTITY fxaccount_getting_started_old_firefox '¿Usas una versión antigua de &syncBrand.shortName.label;?'>

<!ENTITY fxaccount_status_auth_server 'Cuenta del servidor'>
<!ENTITY fxaccount_status_sync_now 'Sincronizar ahora'>
<!ENTITY fxaccount_status_syncing2 'Sincronizando…'>
<!ENTITY fxaccount_status_device_name 'Nombre del dispositivo'>
<!ENTITY fxaccount_status_sync_server 'Servidor de Sync'>
<!ENTITY fxaccount_status_needs_verification2 'Tu cuenta necesita ser verificada. Toca para volver a enviar el mensaje de verificación.'>
<!ENTITY fxaccount_status_needs_credentials 'No se puede conectar. Toca para iniciar sesión.'>
<!ENTITY fxaccount_status_needs_upgrade 'Necesitas actualizar &brandShortName; para iniciar sesión.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled '&syncBrand.shortName.label; está configurado, pero no está sincronizado automáticamente. Marca \&quot;sincronizar datos automáticamente\&quot; en la configuración de Android &gt; Uso de datos.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 '&syncBrand.shortName.label; está configurado, pero no está sincronizado automáticamente. Marca “Sincronizar datos automáticamente” en la configuración de Android &gt; Cuentas.'>
<!ENTITY fxaccount_status_needs_finish_migrating 'Presiona para ingresar a tu nueva cuenta de Firefox.'>
<!ENTITY fxaccount_status_choose_what 'Elige que sincronizar'>
<!ENTITY fxaccount_status_bookmarks 'Marcadores'>
<!ENTITY fxaccount_status_history 'Historial'>
<!ENTITY fxaccount_status_passwords2 'Inicios de sesión'>
<!ENTITY fxaccount_status_tabs 'Abrir pestañas'>
<!ENTITY fxaccount_status_additional_settings 'Ajustes adicionales'>
<!ENTITY fxaccount_pref_sync_use_metered2 'Sincronizar sólo con Wi-Fi'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 'Evita que &brandShortName; se sincronice en redes móviles con plan de datos limitado'>
<!ENTITY fxaccount_status_legal 'Aviso legal' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'Términos de servicio'>
<!ENTITY fxaccount_status_linkprivacy2 'Política de privacidad'>
<!ENTITY fxaccount_remove_account 'Desconectar&ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 '¿Desconectar de Sync?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'Tus datos de navegación se mantendrán en este dispositivo, pero ya no se sincronizarán más con tu cuenta.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 'Cuenta de Firefox &formatS; desconectada.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'Desconectar'>

<!ENTITY fxaccount_enable_debug_mode 'Habilitar Modo de Depuración'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title 'Opciones de &syncBrand.shortName.label;'>
<!ENTITY fxaccount_options_configure_title 'Configura &syncBrand.shortName.label;'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 '&syncBrand.shortName.label; no está conectado'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 'Toca para iniciar sesión como &formatS;'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title '¿Finalizar la actualización &syncBrand.shortName.label;?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text 'Presiona para ingresar como &formatS;'>

<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY itemGeneral.label       "General">
<!ENTITY dataChoicesTab.label    "Împărtășire de date">
<!ENTITY itemUpdate.label        "Actualizare">
<!ENTITY itemNetworking.label    "Rețea și spațiu pe disc">
<!ENTITY itemCertificates.label  "Certificate">

<!-- General Settings -->
<!ENTITY enableGlodaSearch.label       "Activează căutarea globală și indexarea">
<!ENTITY enableGlodaSearch.accesskey   "i">
<!ENTITY dateTimeFormatting.label      "Formatarea datei și a orei">
<!ENTITY languageSelector.label        "Limba">
<!ENTITY allowHWAccel.label            "Folosește accelerarea hardware când este disponibilă">
<!ENTITY allowHWAccel.accesskey        "h">
<!ENTITY storeType.label               "Tipul de stocare al mesajelor pentru conturile noi:">
<!ENTITY storeType.accesskey           "T">
<!ENTITY mboxStore2.label              "Fișier per dosar (mbox)">
<!ENTITY maildirStore.label            "Fișiere per mesaj (maildir)">

<!ENTITY scrolling.label               "Derulare">
<!ENTITY useAutoScroll.label           "Utilizează derularea automată">
<!ENTITY useAutoScroll.accesskey       "U">
<!ENTITY useSmoothScrolling.label      "Folosește derularea lină">
<!ENTITY useSmoothScrolling.accesskey  "o">

<!ENTITY systemIntegration.label       "Integrare cu sistemul">
<!ENTITY alwaysCheckDefault.label      "Verifică întotdeauna la pornire dacă &brandShortName; este clientul implicit de e-mail">
<!ENTITY alwaysCheckDefault.accesskey  "a">
<!ENTITY searchIntegration.label       "Permite la &searchIntegration.engineName; să caute în mesaje">
<!ENTITY searchIntegration.accesskey   "P">
<!ENTITY checkDefaultsNow.label        "Verifică acum…">
<!ENTITY checkDefaultsNow.accesskey    "V">
<!ENTITY configEditDesc.label          "Configurare avansată">
<!ENTITY configEdit.label              "Editor de configurație…">
<!ENTITY configEdit.accesskey          "g">
<!ENTITY returnReceiptsInfo.label      "Configurează cum gestionează &brandShortName; confirmările de primire">
<!ENTITY showReturnReceipts.label      "Confirmări de primire…">
<!ENTITY showReturnReceipts.accesskey  "r">

<!-- Data Choices -->
<!ENTITY telemetrySection.label          "Telemetrie">
<!ENTITY telemetryDesc.label             "Împărtășește date privind performanța, consumul, hardware-ul și personalizările browserului tău cu &vendorShortName; pentru a ne ajuta să facem &brandShortName; mai bun">
<!ENTITY enableTelemetry.label           "Activează telemetria">
<!ENTITY enableTelemetry.accesskey       "t">
<!ENTITY telemetryLearnMore.label        "Află mai multe">

<!ENTITY crashReporterSection.label      "Raportor de defecțiuni">
<!ENTITY crashReporterDesc.label         "&brandShortName; trimite rapoarte de probleme pentru a ajuta &vendorShortName; să facă clientul tău de e-mail mai stabil și sigur">
<!ENTITY enableCrashReporter.label       "Activează raportorul de defecțiuni">
<!ENTITY enableCrashReporter.accesskey   "C">
<!ENTITY crashReporterLearnMore.label    "Află mai multe">

<!-- Update -->
<!-- LOCALIZATION NOTE (updateApp.label):
  Strings from aboutDialog.dtd are displayed in this section of the preferences.
  Please check for possible accesskey conflicts.
-->
<!ENTITY updateApp2.label                "Actualizări &brandShortName;">
<!-- LOCALIZATION NOTE (updateApp.version.*): updateApp.version.pre is
  followed by a version number, keep the trailing space or replace it with
  a different character as needed. updateApp.version.post is displayed after
  the version number, and is empty on purpose for English. You can use it
  if required by your language.
 -->
<!ENTITY updateApp.version.pre           "Versiunea ">
<!ENTITY updateApp.version.post          "">
<!ENTITY updateAppAllow.description      "Permite ca &brandShortName; să">
<!ENTITY updateAuto.label                "Instalează actualizări automat (recomandat: securitate sporită)">
<!ENTITY updateAuto.accesskey            "a">
<!ENTITY updateCheck.label               "Caută actualizări, dar lasă-mă pe mine să decid dacă să le instalez">
<!ENTITY updateCheck.accesskey           "C">
<!ENTITY updateHistory.label             "Afișează istoricul actualizărilor">
<!ENTITY updateHistory.accesskey         "p">

<!ENTITY useService.label                "Folosește un serviciu în fundal pentru a instala actualizări">
<!ENTITY useService.accesskey            "s">

<!ENTITY updateCrossUserSettingWarning.description "Această setare se va aplica tuturor conturilor Windows și profilurilor &brandShortName; care folosesc această instalare &brandShortName;.">

<!-- Networking and Disk Space -->
<!ENTITY showSettings.label            "Setări…">
<!ENTITY showSettings.accesskey        "S">
<!ENTITY proxiesConfigure.label        "Configurează modul în care &brandShortName; se conectează la internet">
<!ENTITY connectionsInfo.caption       "Conexiune">
<!ENTITY offlineInfo.caption           "Offline">
<!ENTITY offlineInfo.label             "Configurează setările pentru modul offline">
<!ENTITY showOffline.label             "Offline…">
<!ENTITY showOffline.accesskey         "O">

<!ENTITY Diskspace                       "Spațiu pe disc">
<!ENTITY offlineCompactFolders.label     "Compactează toate dosarele pentru a salva peste">
<!ENTITY offlineCompactFolders.accesskey "C">
<!ENTITY offlineCompactFoldersMB.label   "MB în total">

<!-- LOCALIZATION NOTE:
  The entities useCacheBefore.label and useCacheAfter.label appear on a single
  line in preferences as follows:

  &useCacheBefore.label  [ textbox for cache size in MB ]   &useCacheAfter.label;
-->
<!ENTITY useCacheBefore.label            "Utilizează până la">
<!ENTITY useCacheBefore.accesskey        "U">
<!ENTITY useCacheAfter.label             "MB de spațiu pentru cache">
<!ENTITY overrideSmartCacheSize.label    "Înlocuiește gestionarea automată a cache-ului">
<!ENTITY overrideSmartCacheSize.accesskey "v">
<!ENTITY clearCacheNow.label             "Șterge acum">
<!ENTITY clearCacheNow.accesskey         "c">

<!-- Certificates -->
<!ENTITY certSelection.description       "Când un server cere certificatul meu personal:">
<!ENTITY certs.auto                      "Selectează automat unul">
<!ENTITY certs.auto.accesskey            "S">
<!ENTITY certs.ask                       "Întreabă-mă de fiecare dată">
<!ENTITY certs.ask.accesskey             "A">
<!ENTITY enableOCSP.label                "Interoghează serverele de răspuns OCSP pentru confirmarea valabilității actuale a certificatelor">
<!ENTITY enableOCSP.accesskey            "e">

<!ENTITY manageCertificates.label "Gestionează certificatele">
<!ENTITY manageCertificates.accesskey "M">
<!ENTITY viewSecurityDevices.label "Dispozitive de securitate">
<!ENTITY viewSecurityDevices.accesskey "u">

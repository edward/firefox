# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = Încearcă-l acum
onboarding-button-label-get-started = Începe
onboarding-welcome-header = Bine ai venit la { -brand-short-name }
onboarding-start-browsing-button-label = Începe să navighezi

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = Navigare privată
onboarding-private-browsing-text = Navighează singur. Navigarea privată cu blocarea conținutului blochează elementele de urmărire care te urmăresc peste tot pe web.
onboarding-screenshots-title = Capturi de ecran
onboarding-screenshots-text = Realizează, salvează și partajează capturi de ecran - fără să ieși din { -brand-short-name }. Capturează o regiune sau o întreagă pagină în timp ce navighezi. Apoi salveaz-o pe web pentru acces și partajare ușoare.
onboarding-addons-title = Suplimente
onboarding-addons-text = Adaugă chiar și mai multe funcționalități pentru ca { -brand-short-name } să lucreze și mai mult pentru tine. Compară prețuri, verifică starea vremii sau exprimă-ți personalitatea cu o temă personalizată.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Navighează mai rapid, mai inteligent sau mai sigur cu extensii precum Ghostery, care permite blocarea reclamelor sâcâitoare.
# Note: "Sync" in this case is a generic verb, as in "to synchronize"
onboarding-fxa-title = Sincronizare
onboarding-fxa-text = Înregistrează-te pentru un { -fxaccount-brand-name } și sincronizează-ți marcajele, parolele și filele deschise oriunde folosești { -brand-short-name }.

## Message strings belonging to the Return to AMO flow

return-to-amo-sub-header = Perfect, ai { -brand-short-name }
# <icon></icon> will be replaced with the icon belonging to the extension
#
# Variables:
#   $addon-name (String) - Name of the add-on
return-to-amo-addon-header = Acum, să obținem și <icon></icon><b>{ $addon-name }.</b>
return-to-amo-extension-button = Adaugă extensia
return-to-amo-get-started-button = Începe cu { -brand-short-name }

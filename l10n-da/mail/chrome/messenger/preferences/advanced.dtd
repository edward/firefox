<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY itemGeneral.label       "Generelt">
<!ENTITY dataChoicesTab.label    "Datadeling">
<!ENTITY itemUpdate.label        "Opdateringer">
<!ENTITY itemNetworking.label    "Netværk &amp; diskplads">
<!ENTITY itemCertificates.label  "Certifikater">

<!-- General Settings -->
<!ENTITY enableGlodaSearch.label       "Aktiver global søgning og indeksering">
<!ENTITY enableGlodaSearch.accesskey   "A">
<!ENTITY dateTimeFormatting.label      "Dato- og tidsformat">
<!ENTITY languageSelector.label        "Sprog">
<!ENTITY allowHWAccel.label            "Brug hardware-acceleration hvor muligt">
<!ENTITY allowHWAccel.accesskey        "h">
<!ENTITY storeType.label               "Lagertype for meddelelser for nye konti:">
<!ENTITY storeType.accesskey           "L">
<!ENTITY mboxStore2.label              "En fil pr. mappe (mbox)">
<!ENTITY maildirStore.label            "En fil pr. meddelelse (maildir)">

<!ENTITY scrolling.label               "Scrolling">
<!ENTITY useAutoScroll.label           "Brug autoscrolling">
<!ENTITY useAutoScroll.accesskey       "B">
<!ENTITY useSmoothScrolling.label      "Brug blød scrolling">
<!ENTITY useSmoothScrolling.accesskey  "r">

<!ENTITY systemIntegration.label       "Systemintegration">
<!ENTITY alwaysCheckDefault.label      "Undersøg altid om &brandShortName; er standardmailprogrammet, når det startes">
<!ENTITY alwaysCheckDefault.accesskey  "U">
<!ENTITY searchIntegration.label       "Tillad &searchIntegration.engineName; at søge efter meddelelser">
<!ENTITY searchIntegration.accesskey   "T">
<!ENTITY checkDefaultsNow.label        "Undersøg nu…">
<!ENTITY checkDefaultsNow.accesskey    "n">
<!ENTITY configEditDesc.label          "Avanceret konfiguration">
<!ENTITY configEdit.label              "Avancerede indstillinger…">
<!ENTITY configEdit.accesskey          "r">
<!ENTITY returnReceiptsInfo.label      "Vælg hvordan &brandShortName; skal håndtere kvitteringer">
<!ENTITY showReturnReceipts.label      "Kvitteringer…">
<!ENTITY showReturnReceipts.accesskey  "v">

<!-- Data Choices -->
<!ENTITY telemetrySection.label          "Telemetry">
<!ENTITY telemetryDesc.label             "Deler data om ydelse, brug, hardware og tilpasninger i mailprogrammet med &vendorShortName; for at hjælpe os med at gøre &brandShortName; bedre">
<!ENTITY enableTelemetry.label           "Aktiver Telemetry">
<!ENTITY enableTelemetry.accesskey       "A">
<!ENTITY telemetryLearnMore.label        "Læs mere">

<!ENTITY crashReporterSection.label      "Fejlrapportering">
<!ENTITY crashReporterDesc.label         "&brandShortName; sender fejlrapporter til &vendorShortName; for at gøre dit mailprogram mere stabilt og sikkert">
<!ENTITY enableCrashReporter.label       "Aktiver fejlrapportering">
<!ENTITY enableCrashReporter.accesskey   "k">
<!ENTITY crashReporterLearnMore.label    "Læs mere">

<!-- Update -->
<!-- LOCALIZATION NOTE (updateApp.label):
  Strings from aboutDialog.dtd are displayed in this section of the preferences.
  Please check for possible accesskey conflicts.
-->
<!ENTITY updateApp2.label                "&brandShortName;-opdateringer">
<!-- LOCALIZATION NOTE (updateApp.version.*): updateApp.version.pre is
  followed by a version number, keep the trailing space or replace it with
  a different character as needed. updateApp.version.post is displayed after
  the version number, and is empty on purpose for English. You can use it
  if required by your language.
 -->
<!ENTITY updateApp.version.pre           "Version ">
<!ENTITY updateApp.version.post          "">
<!ENTITY updateAppAllow.description      "Giv &brandShortName; tilladelse til at">
<!ENTITY updateAuto.label                "Installere opdateringer automatisk (anbefalet, forbedrer sikkerheden)">
<!ENTITY updateAuto.accesskey            "I">
<!ENTITY updateCheck.label               "Søge efter opdateringer, men lade mig vælge om de skal installeres">
<!ENTITY updateCheck.accesskey           "ø">
<!ENTITY updateHistory.label             "Vis opdateringshistorik">
<!ENTITY updateHistory.accesskey         "V">

<!ENTITY useService.label                "Brug en baggrundsservice til at installere opdateringer">
<!ENTITY useService.accesskey            "b">

<!ENTITY updateCrossUserSettingWarning.description "Denne indstilling gælder for alle Windows-konti og &brandShortName;-profiler der bruger denne installation af &brandShortName;.">

<!-- Networking and Disk Space -->
<!ENTITY showSettings.label            "Indstillinger…">
<!ENTITY showSettings.accesskey        "I">
<!ENTITY proxiesConfigure.label        "Konfigurer hvordan &brandShortName; forbinder til internettet">
<!ENTITY connectionsInfo.caption       "Forbindelse">
<!ENTITY offlineInfo.caption           "Offline">
<!ENTITY offlineInfo.label             "Rediger offline-indstillinger">
<!ENTITY showOffline.label             "Offline…">
<!ENTITY showOffline.accesskey         "O">

<!ENTITY Diskspace                       "Diskplads">
<!ENTITY offlineCompactFolders.label     "Optimer mapper, når det kan spare mere end">
<!ENTITY offlineCompactFolders.accesskey "k">
<!ENTITY offlineCompactFoldersMB.label   "MB">

<!-- LOCALIZATION NOTE:
  The entities useCacheBefore.label and useCacheAfter.label appear on a single
  line in preferences as follows:

  &useCacheBefore.label  [ textbox for cache size in MB ]   &useCacheAfter.label;
-->
<!ENTITY useCacheBefore.label            "Benyt op til">
<!ENTITY useCacheBefore.accesskey        "B">
<!ENTITY useCacheAfter.label             "MB til mellemlageret">
<!ENTITY overrideSmartCacheSize.label    "Tilsidesæt automatisk cachehåndtering">
<!ENTITY overrideSmartCacheSize.accesskey "s">
<!ENTITY clearCacheNow.label             "Ryd nu">
<!ENTITY clearCacheNow.accesskey         "R">

<!-- Certificates -->
<!ENTITY certSelection.description       "Når en server forespørger mit personlige certifikat:">
<!ENTITY certs.auto                      "Vælg et automatisk">
<!ENTITY certs.auto.accesskey            "a">
<!ENTITY certs.ask                       "Spørg mig hver gang">
<!ENTITY certs.ask.accesskey             "ø">
<!ENTITY enableOCSP.label                "Send forespørgsel til OCSP responder-servere for at bekræfte certifikaters aktuelle gyldighed">
<!ENTITY enableOCSP.accesskey            "e">

<!ENTITY manageCertificates.label "Håndter certifikater">
<!ENTITY manageCertificates.accesskey "c">
<!ENTITY viewSecurityDevices.label "Sikkerhedsmoduler">
<!ENTITY viewSecurityDevices.accesskey "S">

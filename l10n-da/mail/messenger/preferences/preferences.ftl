# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

choose-messenger-language-description = Vælg det sprog, der skal bruges i brugerfladen i { -brand-short-name }.
manage-messenger-languages-button =
    .label = Vælg alternativer…
    .accesskey = l
confirm-messenger-language-change-description = Genstart { -brand-short-name } for at anvende ændringerne
confirm-messenger-language-change-button = Anvend og genstart
update-pref-write-failure-title = Skrivefejl
# Variables:
#   $path (String) - Path to the configuration file
update-pref-write-failure-message = Kunne ikke gemme indstillingerne. Kan ikke skrive til filen: { $path }

<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "Gå tilbage">
<!ENTITY safeb.palm.seedetails.label "Se detaljer">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "Dette er ikke et vildledende websted…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "v">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "Rådgivning leveres af <a id='advisory_provider'/>">


<!ENTITY safeb.blocked.malwarePage.title2 "Det kan skade din computer at besøge dette websted">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "&brandShortName; blokerede denne side, fordi den måske vil forsøge at installere ondsindet software, der kan stjæle eller slette personlig information på din computer.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "<span id='malware_sitename'/> er blevet <a id='error_desc_link'>rapporteret til at indeholde ondsindet software</a>. Du kan <a id='report_detection'>indrapportere, at dette er sket ved en fejl</a> eller <a id='ignore_warning_link'>ignorere risikoen</a> og fortsætte til det usikre websted.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "<span id='malware_sitename'/> er blevet <a id='error_desc_link'>rapporteret til at indeholde ondsindet software</a>. Du kan <a id='report_detection'>indrapportere, at dette er sket ved en fejl</a>.">

<!ENTITY safeb.blocked.malwarePage.learnMore "Læs mere om skadelig indhold på nettet inklusiv vira og anden skadelig software samt hvordan, du kan beskytte din computer på <a id='learn_more_link'>StopBadware.org</a>. Læs mere om beskyttelse mod phishing og skadelig software i &brandShortName; på <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.unwantedPage.title2 "Dette websted kan indeholde skadelige programmer">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "&brandShortName; blokerede denne side, fordi den måske vil forsøge at snyde dig til at installere programmer, der gør dine oplevelse op nettet dårligere (fx ved at ændre din startside eller vise ekstra reklamer på websteder, du besøger).">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "<span id='unwanted_sitename'/> er blevet <a id='error_desc_link'>rapporteret til at indeholde skadelig software</a>. Du kan <a id='ignore_warning_link'>ignorere risikoen</a> og fortsætte til det usikre websted.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "<span id='unwanted_sitename'/> er blevet <a id='error_desc_link'>er blevet rapporteret til at indeholde skadelig software</a>.">

<!ENTITY safeb.blocked.unwantedPage.learnMore "Læs mere om skadelig og uønsket software ved at læse vores <a id='learn_more_link'>retningslinjer om uønsket software</a>. Læs mere om beskyttelse mod phishing og skadelig software i &brandShortName; på <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.phishingPage.title3 "Vildledende websted forude">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "&brandShortName; blokerede denne side, fordi den måske vil snyde dig til at gøre noget farlig, fx at installerede skadelig software eller afsløre personlig information som  dine adgangskoder eller oplysninger om betalingskort.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "<span id='phishing_sitename'/> er blevet <a id='error_desc_link'>rapporteret som et vildledende websted</a>. Du kan <a id='report_detection'>rapportere, at dette er sket ved en fejl</a> eller <a id='ignore_warning_link'>ignorere risikoen</a> og fortsætte til det usikre websted.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "<span id='phishing_sitename'/> er blevet <a id='error_desc_link'>rapporteret som et vildledende websted</a>. Du kan <a id='report_detection'>rapportere, at dette er sket ved en fejl</a>.">

<!ENTITY safeb.blocked.phishingPage.learnMore "Læs mere om vildledende websteder og phishing på <a id='learn_more_link'>www.antiphishing.org</a>. Læs mere om beskyttelse mod phishing og skadelig software i &brandShortName; på <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.harmfulPage.title "Webstedet kan indeholde skadelig software">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "&brandShortName; blokerede denne side, fordi den måske vil forsøge at installere skadelig software, der stjæler eller sletter dine informationer (eksempelvis billeder, adgangskoder, beskeder eller oplysninger om betalingskort).">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "<span id='harmful_sitename'/> er blevet <a id='error_desc_link'>rapporteret til at indehold en potentielt skadelig app</a>. Du kan <a id='ignore_warning_link'>ignorere risikoen</a> og fortsætte til det usikre websted.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "<span id='harmful_sitename'/> er blevet <a id='error_desc_link'>rapporteret til at indholde en potentiel skadelig app</a>.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "Læs mere om beskyttelse mod phishing og skadelig software i &brandShortName; på <a id='firefox_support'>support.mozilla.org</a>.">

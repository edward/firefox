<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "Firefox Sync">
<!ENTITY syncBrand.shortName.label "Sync">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label 'Forbind til &syncBrand.shortName.label;'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'For at aktivere din nye enhed, vælg \&quot;Opsæt &syncBrand.shortName.label;\&quot; på enheden.'>
<!ENTITY sync.subtitle.pair.label 'For at aktivere, vælg \&quot;Tilføj enhed\&quot; på din anden enhed.'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'Jeg har ikke enheden med mig…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'Logins'>
<!ENTITY sync.configure.engines.title.history 'Historik'>
<!ENTITY sync.configure.engines.title.tabs 'Faneblade'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS1; på &formatS2;'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'Bogmærke-menuen'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'Mærkater'>
<!ENTITY bookmarks.folder.toolbar.label 'Bogmærkelinjen'>
<!ENTITY bookmarks.folder.other.label 'Andre bogmærker'>
<!ENTITY bookmarks.folder.desktop.label 'Bogmærker på desktoppen'>
<!ENTITY bookmarks.folder.mobile.label 'Mobil-bogmærker'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'Fastgjort'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'Tilbage til browsing'>

<!ENTITY fxaccount_getting_started_welcome_to_sync 'Velkommen til &syncBrand.shortName.label;'>
<!ENTITY fxaccount_getting_started_description2 'Log ind for at synkronisere dine faneblade, bogmærker, logins med mere.'>
<!ENTITY fxaccount_getting_started_get_started 'Start'>
<!ENTITY fxaccount_getting_started_old_firefox 'Bruger du en ældre version af &syncBrand.shortName.label;?'>

<!ENTITY fxaccount_status_auth_server 'Kontoserver'>
<!ENTITY fxaccount_status_sync_now 'Synkroniser nu'>
<!ENTITY fxaccount_status_syncing2 'Synkroniserer…'>
<!ENTITY fxaccount_status_device_name 'Enhedens navn'>
<!ENTITY fxaccount_status_sync_server 'Synkroniseringsserver'>
<!ENTITY fxaccount_status_needs_verification2 'Din konto skal bekræftes. Tryk for at få sendt en bekræftelsesmail.'>
<!ENTITY fxaccount_status_needs_credentials 'Kan ikke få forbindelse. Tryk for at logge ind.'>
<!ENTITY fxaccount_status_needs_upgrade 'Du skal opgradere &brandShortName; for at kunne logge ind.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled '&syncBrand.shortName.label; er konfigureret, men synkroniserer ikke automatisk. Du kan skifte indstilling for synkronisering i Androids Indstillinger &gt; Dataforbrug.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 '&syncBrand.shortName.label; er sat op, men synkroniserer ikke automatisk. Du kan skifte indstillinger for synkronisering i Androids indstillinger under Konti.'>
<!ENTITY fxaccount_status_needs_finish_migrating 'Tryk for at logge ind på din nye Firefox-konto.'>
<!ENTITY fxaccount_status_choose_what 'Vælg hvad der skal synkroniseres'>
<!ENTITY fxaccount_status_bookmarks 'Bogmærker'>
<!ENTITY fxaccount_status_history 'Historik'>
<!ENTITY fxaccount_status_passwords2 'Logins'>
<!ENTITY fxaccount_status_tabs 'Åbne faneblade'>
<!ENTITY fxaccount_status_additional_settings 'Flere indstillinger'>
<!ENTITY fxaccount_pref_sync_use_metered2 'Synkroniser kun over wi-fi'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 'Lad kun &brandShortName; synkronisere, når du er tilkoblet wi-fi'>
<!ENTITY fxaccount_status_legal 'Juridisk' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'Tjenestevilkår'>
<!ENTITY fxaccount_status_linkprivacy2 'Privatlivspolitik'>
<!ENTITY fxaccount_remove_account 'Afbryd&ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 'Afbryd synkronisering?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'Dine data forbliver på denne enhed, men vil ikke længere blive synkroniseret med din Firefox-konto.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 'Firefox-kontoen &formatS; synkroniseres ikke længere med denne enhed.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'Afbryd synkronisering'>

<!ENTITY fxaccount_enable_debug_mode 'Aktiver debug-tilstand'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title 'Indstillinger for &syncBrand.shortName.label;'>
<!ENTITY fxaccount_options_configure_title 'Indstil &syncBrand.shortName.label;'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 '&syncBrand.shortName.label; har ikke forbindelse'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 'Tryk for at logge ind på kontoen som &formatS;'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title 'Afslut opdatering af &syncBrand.shortName.label;?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text 'Tryk for at logge ind som &formatS;'>

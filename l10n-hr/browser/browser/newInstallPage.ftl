# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### For this feature, "installation" is used to mean "this discrete download of
### Firefox" and "version" is used to mean "the specific revision number of a
### given Firefox channel". These terms are not synonymous.

title = Važne vijesti
heading = Izmijene vašeg { -brand-short-name } profila
changed-title = Što se promijenilo?
changed-desc-profiles = Ova { -brand-short-name } instalacija ima novi profil. Profil je skup datoteka gdje Firefox sprema informacije kao što su zabilješke, lozinke i vaše korisničke postavke.
changed-desc-dedicated = Kako bismo olakšali i učinili sigurnijim prebacivanje između Firefox instalacija (uključujući Firefox, Firefox ESR, Firefox Beta, Firefox Developer Edition i Firefox Nightly), ova instalacija sada ima posebni profil. Ne dijeli automatski vaše spremljene informacije s drugim Firefox instalacijama.
lost = <b>Niste izgubili osobne podatke ili prilagodbe.</b> Ukoliko ste već spremili neke informacije u Firefox na ovom računalu, još je uvijek dostupno u drugoj Firefox instalaciji.
options-title = Koje su moje mogućnosti?
options-do-nothing = Ukoliko ne učinite ništa, podaci profila u { -brand-short-name } biti će različiti od podataka profila u drugim instalacijama Firefoxa.
options-use-sync = Ukoliko želite da sve Firefox instalacije koriste isti profil, možete koristiti { -fxaccount-brand-name } kako biste ih držali sinkroniziranima.
resources = Izvori:
support-link = Koristeći upravitelja profila - članak podrške
sync-header = Prijavite se ili izradite { -fxaccount-brand-name }
sync-label = Unesite vašu e-poštu
sync-input =
    .placeholder = E-pošta
sync-button = Nastavi
sync-terms = Ukoliko nastavite, slažete se s <a data-l10n-name="terms">Uvjetima pružanja usluge</a> i <a data-l10n-name="privacy">Politikom privatnosti</a>.
sync-first = Prvi put koristite { -sync-brand-name }? Trebat ćete se prijaviti na svaku Firefox instalaciju kako biste sinkronizirali svoje podatke.
sync-learn = Saznajte više

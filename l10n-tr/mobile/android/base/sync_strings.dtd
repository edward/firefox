<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "Firefox Sync">
<!ENTITY syncBrand.shortName.label "Sync">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label '&syncBrand.shortName.label;’e bağlan'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'Yeni aygıtınızı etkinleştirmek için, aygıttan “&syncBrand.shortName.label;\&apos;i kur”u seçin.'>
<!ENTITY sync.subtitle.pair.label 'Etkinleştirmek için, diğer aygıtınızdan “Aygıt eşleştir”i seçin.'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'Aygıt yanımda değil…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'Hesaplar'>
<!ENTITY sync.configure.engines.title.history 'Geçmiş'>
<!ENTITY sync.configure.engines.title.tabs 'Sekmeler'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS1; - &formatS2;'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'Yer imleri menüsü'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'Etiketler'>
<!ENTITY bookmarks.folder.toolbar.label 'Yer imi araç çubuğu'>
<!ENTITY bookmarks.folder.other.label 'Diğer yer imleri'>
<!ENTITY bookmarks.folder.desktop.label 'Masaüstü yer imleri'>
<!ENTITY bookmarks.folder.mobile.label 'Mobil yer imleri'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'Sabitlenmiş'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'Tarayıcıya dön'>

<!ENTITY fxaccount_getting_started_welcome_to_sync '&syncBrand.shortName.label;’e hoş geldiniz'>
<!ENTITY fxaccount_getting_started_description2 'Sekmelerinizi, yer imlerinizi, hesaplarınızı ve dahasını eşitlemek için giriş yapın.'>
<!ENTITY fxaccount_getting_started_get_started 'Başlayın'>
<!ENTITY fxaccount_getting_started_old_firefox '&syncBrand.shortName.label;’in eski bir sürümünü mü kullanıyorsunuz?'>

<!ENTITY fxaccount_status_auth_server 'Hesap sunucusu'>
<!ENTITY fxaccount_status_sync_now 'Şimdi eşitle'>
<!ENTITY fxaccount_status_syncing2 'Eşitleniyor…'>
<!ENTITY fxaccount_status_device_name 'Cihaz adı'>
<!ENTITY fxaccount_status_sync_server 'Sync sunucusu'>
<!ENTITY fxaccount_status_needs_verification2 'Hesabınızın doğrulanması gerekiyor. Doğrulama e-postasını tekrar göndermek için dokunun.'>
<!ENTITY fxaccount_status_needs_credentials 'Bağlanılamadı. Oturum açmak için dokunun.'>
<!ENTITY fxaccount_status_needs_upgrade 'Oturum açmak için &brandShortName; tarayıcınızı yükseltmelisiniz.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled '&syncBrand.shortName.label; kuruldu ama otomatik olarak eşitleme yapmıyor. Android Ayarları &gt; Veri Kullanımı menüsünden “Verileri otomatik eşitle”yi açın.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 '&syncBrand.shortName.label; kuruldu ama otomatik olarak eşitleme yapmıyor. Android Ayarları &gt; Hesaplar menüsünden “Verileri otomatik eşitle”yi açın.'>
<!ENTITY fxaccount_status_needs_finish_migrating 'Yeni Firefox Hesabınıza giriş yapmak için dokunun.'>
<!ENTITY fxaccount_status_choose_what 'Nelerin eşitleneceğini seçin'>
<!ENTITY fxaccount_status_bookmarks 'Yer imleri'>
<!ENTITY fxaccount_status_history 'Geçmiş'>
<!ENTITY fxaccount_status_passwords2 'Hesaplar'>
<!ENTITY fxaccount_status_tabs 'Açık sekmeler'>
<!ENTITY fxaccount_status_additional_settings 'Ek ayarlar'>
<!ENTITY fxaccount_pref_sync_use_metered2 'Sadece Wi-Fi\&apos;ye bağlıyken eşitle'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 'Mobil şebeke internetine veya tarifeli ağa bağlıysam &brandShortName; eşitlenmesin'>
<!ENTITY fxaccount_status_legal 'Yasal' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'Hizmet koşulları'>
<!ENTITY fxaccount_status_linkprivacy2 'Gizlilik bildirimi'>
<!ENTITY fxaccount_remove_account 'Bağlantıyı kes&ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 'Sync bağlantısı kesilsin mi?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'Gezinti verileriniz bu cihazda korunacak ama artık hesabınızla eşitlenmeyecek.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 '&formatS; Firefox Hesabının bağlantısı kesildi.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'Bağlantıyı kes'>

<!ENTITY fxaccount_enable_debug_mode 'Hata ayıklama modunu etkinleştir'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title '&syncBrand.shortName.label; Seçenekleri'>
<!ENTITY fxaccount_options_configure_title '&syncBrand.shortName.label;’i yapılandır'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 '&syncBrand.shortName.label; bağlı değil'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 '&formatS; olarak giriş yapmak için dokunun'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title '&syncBrand.shortName.label; yükseltme tamamlansın mı?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text '&formatS; olarak giriş yapmak için dokunun'>

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

title = نیٹ ورکنگ کے بارے میں
warning = یہ محض تجرباتی ہے- صرف کسی بڑے کی موجودگی میں استعمال کریں-
show-next-time-checkbox = اگلی بار یہ تنبیہ نمائش کریں
ok = ٹھیک ہے
http = HTTP
sockets = ساکٹس
dns = ڈی این ایس
websockets = ویب ساکٹس
refresh = تازہ کریں
auto-refresh = خودبخود, ہر ٣ سیکنڈ کے بعد تازہ کریں
hostname = ہوسٹ نیم
port = پورٹ
http2 = HTTP/2
ssl = ایس ایس ایل
active = فعال
idle = بیکار
host = ہوسٹ
tcp = ٹی سی پی
sent = بھیجا گیا
received = وصولا گیا
family = خاندان
trr = TRR
addresses = پتے
expires = مدت ختم ہوتی ہے (سیکنڈ)
messages-sent = پیغامات بھیج دئیے گیے
messages-received = پیغامات وصولے گیے
bytes-sent = بائیٹس بھیج دئیے گیے
bytes-received = بائیٹس وصولے گیے
logging = لاگینگ
log-tutorial = اس ٹول کو کس طرح استعمال کرنا ہے اسکے لئے دیکھیں <a data-l10n-name="logging">HTTP Logging</a>۔
current-log-file = موجودہ لاگ مسل
current-log-modules = موجودہ لاگ ماڈیول
set-log-file = لاگ مسل سیٹ کریں
set-log-modules = لاگ ماڈیول سیٹ کریں
start-logging = لاگنگ شروع کریں
stop-logging = لاگنگ رکیں
dns-lookup = DNS تلاش کریں
dns-lookup-button = حل کریں
dns-domain = عمل داری:
dns-lookup-table-column = IPs
rcwn-perf-open = کھولیں
rcwn-perf-read = پڑھيں
rcwn-perf-write = لکھیں

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### For this feature, "installation" is used to mean "this discrete download of
### Firefox" and "version" is used to mean "the specific revision number of a
### given Firefox channel". These terms are not synonymous.

title = Isallen s wazal
heading = Bedel ɣer umaɣnu { -brand-short-name }
changed-title = Acu Ibedlen?
options-title = D acu-tent tixtirin-inu?
resources = Tiɣbula:
sync-header = Kcem neɣ snulfu-d { -fxaccount-brand-name }
sync-label = Sekcem imayl inek
sync-input =
    .placeholder = Imayl
sync-button = Kemmel
sync-learn = Issin ugar

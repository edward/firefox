# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Asenker tikkelt-nniḍen yettwasra
restart-required-header = Suref-aɣ. Mazal kan ciṭ ad t-nseggem uqble ad tkemmleḍ.
restart-required-intro = Aql-aɣ akken i nsebded lqem deg ugilal. Ales tanekra n { -brand-short-name } akken ad yeddu.
restart-required-description = Ad d-nerr akk isebtar-ik/im, isfuyla akked waccaren ticki yemmed akken ad tuɣaleḍ s zreb ar wanida telliḍ.
restart-button-label = Ales asenker n { -brand-short-name }

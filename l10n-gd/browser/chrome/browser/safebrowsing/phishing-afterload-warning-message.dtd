<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "Air ais">
<!ENTITY safeb.palm.seedetails.label "Faic am mion-fhiosrachadh">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "Chan e làrach foill a tha seo…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "d">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "Comhairle air a sholar le <a id='advisory_provider'/>.">


<!ENTITY safeb.blocked.malwarePage.title2 "Dh’fhaoidte gun èirich cron air a’ choimpiutair agad ma thadhlas tu air an làrach-lìn seo">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "Bhac &brandShortName; an duilleag seo airson ’s nach feuch e ri bathar-bog droch-rùnach a stàladh a bhiodh airson dàta pearsanta a ghoid no a sguabadh às a’ choimpiutair agad.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "Chaidh <a id='error_desc_link'>aithris</a> a dhèanamh gu bheil bathar-bog droch-rùnach air <span id='malware_sitename'/>. ’S urrainn dhut <a id='report_detection'>aithris a dhèanamh thu fhèin gu bheil rudeigin cearr a thaobh sin</a> no <a id='ignore_warning_link'>an cunnart a leigeil seachad</a> agus tadhal air an làrach mhì-shàbhailte co-dhiù.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "Chaidh <a id='error_desc_link'>aithris</a> a dhèanamh gu bheil bathar-bog droch-rùnach air <span id='malware_sitename'/>. ’S urrainn dhut <a id='report_detection'>aithris a dhèanamh thu fhèin gu bheil rudeigin cearr a thaobh sin</a>.">

<!ENTITY safeb.blocked.malwarePage.learnMore "Faigh barrachd fiosrachaidh mu shusbaint-lìn dhochainneach, a’ gabhail a-staigh bhìorasan agus bathar droch-rùnach eile agus mar a dhìonas tu an coimpiutair agad aig <a id='learn_more_link'>StopBadware.org</a>. Faigh barrachd fiosrachaidh mun dìon aig &brandShortName;’s ro bhathar droch-rùnach agus fiasgach aig <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.unwantedPage.title2 "Dh’fhaoidte gu bheil prògraman dochainneach air an làrach seo">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "Bhac &brandShortName; an duilleag seo oir tha cunnart gum bi e ri foill is e a’ feuchainn ri prògraman a stàladh air an uidheam agad a nì cron air do bhrabhsadh (mar eisimpleir, le bhith ag atharrachadh na duilleige-dachaidh agad no a’ sealltainn sanasachd air làraichean air an tadhail thu).">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "Chaidh <a id='error_desc_link'>aithris a dhèanamh gu bheil bathar-bog dochainneach</a> air <span id='unwanted_sitename'/>. ’S urrainn dhut <a id='ignore_warning_link'>an cunnart a leigeil seachad</a> agus tadhail air an làrach mhì-shàbhailte seo a dh’aindeoin sin.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "Chaidh <a id='error_desc_link'>aithris a dhèanamh gu bheil bathar-bog dochainneach</a> air <span id='unwanted_sitename'/>.">

<!ENTITY safeb.blocked.unwantedPage.learnMore "Faigh barrachd fiosrachaidh air bathar-bog gun iarraidh no bathar dochainneach sa <a id='learn_more_link'>phoileasaidh mu bhathar-bog gun iarraidh</a>. Faigh barrachd fiosrachaidh air an dìon a bheir &brandShortName;’s dhut o bhathar droch-rùnach is fiasgach air <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.phishingPage.title3 "’S e làrach foille a tha seo">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "Bhac &brandShortName; an duilleag seo air dh’fhaoidte gum bi e ri foill chunnartach, mar eisimpleir a bhith a’ stàladh bathar-bog no a’ goid fiosrachadh pearsanta ort mar fhaclan-faire no cairtean-creideis.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "Chaidh <a id='error_desc_link'>aithris a dhèanamh gur e làrach foille</a> a tha ann an <span id='phishing_sitename'/>. ’S urrainn dhut <a id='report_detection'>aithris a dhèanamh gu bheil seo cearr</a> no <a id='ignore_warning_link'>an cunnart a leigeil seachad</a> agus tadhal air an làrach mhì-shàbhailte seo co-dhiù.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "Chaidh <a id='error_desc_link'>aithris a dhèanamh</a> gur e làrach foille a tha ann an <span id='phishing_sitename'/>. ’S urrainn dhut <a id='report_detection'>aithris a dhèanamh thu fhèin gu bheil rudeigin cearr a thaobh sin</a>.">

<!ENTITY safeb.blocked.phishingPage.learnMore "Faigh barrachd fiosrachaidh mu làraichean foille agus fiasgach aig <a id='learn_more_link'>www.antiphishing.org</a>. Faigh barrachd fiosrachaidh mun dìon a tha &brandShortName;’s a’ toirt dhut o bhathar-bog droch-rùnach agus fiasgach aig <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.harmfulPage.title "Dh’fhaoidte gu bheil bathar-bog droch rùnach air an làrach seo">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "Bhac &brandShortName; an duilleag seo oir tha teans gum feuch e ri aplacaidean cunnartach a stàladh a ghoideas no a sguabas às fiosrachadh agad (mar eisimpleir, dealbhan, faclan-faire, teachdaireachdan is cairtean-creideis).">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "Chaidh <a id='error_desc_link'>aithris a dhèanamh gu bheul aplacaid chunnartach</a> air <span id='harmful_sitename'/>. ’S urrainn dhut <a id='ignore_warning_link'>an cunnart a leigeil seachad</a> agus tadhal air an làrach mhì-shàbhailte co-dhiù.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "Chaidh <a id='error_desc_link'>aithris a dhèanamh gu bheul aplacaid chunnartach</a> air <span id='harmful_sitename'/>.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "Faigh barrachd fiosrachaidh mun dìon a tha &brandShortName;’s a’ toirt dhut o bhathar-bog droch-rùnach agus fiasgach aig <a id='firefox_support'>support.mozilla.org</a>.">

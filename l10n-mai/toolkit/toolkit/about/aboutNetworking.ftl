# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

title = संजालनक परिचय
warning = ई बहुत प्रायोगिक अछि. बिनु व्यस्कक संग एकर उपयोग नहि करू.
show-next-time-checkbox = Show this warning next time
ok = बेस
sockets = साकेट
dns = DNS
websockets = वेबसाकेट
refresh = ताजा करू
auto-refresh = हर 3 सेकेंडमे फेर ताजा करू
hostname = मेजबाननाम
port = पोर्ट
ssl = SSL
active = सक्रिय करू
idle = निष्क्रिय
host = मेजबान
tcp = TCP
sent = भेजल-डाक
received = भेटल
family = परिवार
addresses = पता
expires = खतम होइछ (सेकंड)
messages-sent = भेजल गेल सन्देश
messages-received = संदेश प्राप्त
bytes-sent = बाइट भेजल
bytes-received = पाओल बाइट

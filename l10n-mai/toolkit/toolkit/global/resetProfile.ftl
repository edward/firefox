# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

refresh-profile-dialog =
    .title = { -brand-short-name } फेर सेट करू
refresh-profile-dialog-button =
    .label = { -brand-short-name } फेर सेट करू
refresh-profile-description = समस्या केँ सही करए आओर प्रदर्शन केँ पुनर्स्थापित करबाक लेल नवीन तरीका सँ आरंभ करू.
refresh-profile-description-details = ई हाएत:
refresh-profile-remove = अपन ऐड-ऑन्स आओर अनुकूलन निकालू
refresh-profile-restore = अपन ब्राउजर सेटिंग्स डिफ़ॉल्ट रूप मे पुनर्स्थापित करू
refresh-profile = { -brand-short-name } एकटा धुन द' दिअ
refresh-profile-button = { -brand-short-name } ताजा करू…

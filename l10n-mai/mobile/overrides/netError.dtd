<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY % brandDTD SYSTEM "chrome://branding/locale/brand.dtd">
%brandDTD;

<!ENTITY loadError.label "पृष्ठक लोडिंगमे समस्या">
<!ENTITY retry.label "फेर कोशिश करू">

<!-- Specific error messages -->

<!ENTITY connectionFailure.title "कनेक्ट करबामे असमर्थ">
<!ENTITY connectionFailure.longDesc2 "&sharedLongDesc3;">

<!ENTITY deniedPortAccess.title "ई पता प्रतिबंधित अछि ">
<!ENTITY deniedPortAccess.longDesc "">

<!ENTITY dnsNotFound.title "सर्वर नहि भेटल">
<!-- LOCALIZATION NOTE (dnsNotFound.longDesc4) This string contains markup including widgets for searching
     or enabling wifi connections. The text inside tags should be localized.  Do not change the ids. -->
<!ENTITY dnsNotFound.longDesc4 "<ul> <li>टंकण त्रुटि हेतु पता जाँचू जेना कि <strong>ww</strong>.example.com क बदला <strong>www</strong>.example.com</li> <div id='searchbox'> <input id='searchtext' type='search'></input> <button id='searchbutton'>खोज</button> </div> <li>जँ अहाँ कोनो पृष्ठ केँ लोड क पाबै मे असमर्थ ची, तँ अपन उपकरण केर डेटा या वाई फाई कनेक्शन केँ जाँचू. <button id='wifi'>वाई फाई सक्रिय करू</button> </li> </ul>">

<!ENTITY fileNotFound.title "फाइल नहि भेटल">
<!ENTITY fileNotFound.longDesc "<ul> <li>कैपिटलाइजेशन अथवा दूसरी टाइपिंग त्रुटि क लेल फाइल नाम जाँचू.</li> <li>जाँचे ई देखब क लेल जे फाइल खिसकलया, फेर नामकरण कएल अथवा मेटायल गेल छल.</li> </ul>">

<!ENTITY fileAccessDenied.title "फ़ाइल तक पहुँच रद्द कएल गेल">
<!ENTITY fileAccessDenied.longDesc "<ul><li>एकरा हटाओल, घसकाओल जाए सकैत अछि या फाइल अनुमति पहुँच रोकि सकैत अछि.</li> </ul>">

<!ENTITY generic.title "ओफ्फ.">
<!ENTITY generic.longDesc "<p>&brandShortName; किछु कारणक लेल एहि पृष्ठ केँ लोड नहि कए सकैत अछि .</p>">

<!ENTITY malformedURI.title "पता वैध नहि अछि">
<!-- LOCALIZATION NOTE (malformedURI.longDesc2) This string contains markup including widgets for searching
     or enabling wifi connections. The text inside the tags should be localized. Do not touch the ids. -->
<!ENTITY malformedURI.longDesc2 "<ul> <li>वेब पता सामान्यतः एहि प्रकार लिखल जाएत अछि <strong>http://www.example.com/</strong></li> <div id='searchbox'> <input id='searchtext' type='search'></input> <button id='searchbutton'>खोज</button> </div> <li>सुनिश्चित करू जे अहाँ फॉर्वर्ड शीर्षक केर उपयोग कए रहल छी (i.e. <strong>/</strong>).</li> </ul>">

<!ENTITY netInterrupt.title "कनेक्शन बाधित कएल गेल छल">
<!ENTITY netInterrupt.longDesc2 "&sharedLongDesc3;">

<!ENTITY notCached.title "दस्ताबेज क समय समाप्त">
<!ENTITY notCached.longDesc "<p>अनुरोध कएल गेल दस्तावेज़ &brandShortName; क अस्थाई भंडार मे उपलब्ध नहि अछि.</p><ul><li>सुरक्षा सावधानी क रूप मे, &brandShortName; स्वतः संवेदनशील दस्तावेज़ कलेल पुनः आग्रह नहि करैत अछि.</li><li>वेबसाइट सँ दस्तावेज़ केँ पुनः आग्रह करए लेली पुनः प्रयास करू केँ दाबू.</li></ul>">

<!ENTITY netOffline.title "Offline mode">
<!-- LOCALIZATION NOTE (netOffline.longDesc3) This string contains markup including widgets enabling wifi connections.
     The text inside the tags should be localized. Do not touch the ids. -->
<!ENTITY netOffline.longDesc3 "<ul> <li>फेर सँ प्रयास करू. &brandShortName; कनेक्शन खोलए आओर पृष्ठ रिलोड करए केर प्रयास करताह. <button id='wifi'>वाई-फाई आरंभ करू</button> </li> </ul>">

<!ENTITY contentEncodingError.title "अंतर्वस्तु एन्कोडिंग त्रुटि">
<!ENTITY contentEncodingError.longDesc "<ul> <li>कृपया वेबसाइट मालिक सँ हुनका एहि समस्याक संबंधमे बताबैक लेल संपर्क करू.</li> </ul>">

<!ENTITY unsafeContentType.title "असुरक्षित फाइल प्रकार">
<!ENTITY unsafeContentType.longDesc "<ul> <li>कृपया वेबसाइट मालिक सँ हुनका एहि समस्याक संबंधमे बताबैक लेल संपर्क करू.</li> </ul>">

<!ENTITY netReset.title "कनेक्शन रिसेट कएल गेल छल">
<!ENTITY netReset.longDesc2 "&sharedLongDesc3;">

<!ENTITY netTimeout.title "कनेक्शनक समय समाप्त भए गेल">
<!ENTITY netTimeout.longDesc2 "&sharedLongDesc3;">

<!ENTITY unknownProtocolFound.title "पता बुझल नहि गेल छला">
<!ENTITY unknownProtocolFound.longDesc "<ul> <li>अहाँक ई पताकेँ खोलबाक लेल दोसर साफ्टवेयरकेँ संस्थापित करै पड़त.</li> </ul>">

<!ENTITY proxyConnectFailure.title "प्राक्सी सर्वर कनेक्शन अस्वीकार कए रहल अछि ">
<!ENTITY proxyConnectFailure.longDesc "<ul> <li>प्रॉक्सी सेटिंग्स केर जाँच सुनिश्चित करए कलेल करू कि ओ सही अछि.</li> <li>अपन संजाल प्रशासक केँ जाँचू सुनिश्चित करबाक लेल प्रॉक्सी सर्वर काज कए रहल अछि.</li> </ul>">

<!ENTITY proxyResolveFailure.title "प्राक्सी सर्वर ढ़ूढ़वमे असमर्थ">
<!-- LOCALIZATION NOTE (proxyResolveFailure.longDesc3) This string contains markup including widgets for enabling wifi connections.
     The text inside the tags should be localized. Do not touch the ids. -->
<!ENTITY proxyResolveFailure.longDesc3 "<ul> <li>जाँचू आओर ई सुनिश्चत करू कि प्रॉक्सी सेटिंग्स सही अछि.</li> <li>जाँचू आओर ई सुनिश्चत करू कि अहाँक डिवाइस केर डेटा या वाई-फाई कनेक्शन शुरू अछि. <button id='wifi'>वाई-फाई शुरू करू</button> </li> </ul>">

<!ENTITY redirectLoop.title "पृष्ठ ठीक सँ पुनर्निर्देशित नहि क रहल अछि">
<!ENTITY redirectLoop.longDesc "<ul> <li>ई समस्या कहियो कहियो कुकी केँ स्वीकार करए या निष्क्रिय करए केर कारण हाएत अछि.</li> </ul>">

<!ENTITY unknownSocketType.title "सर्वर सँ अप्रत्याशित अनुक्रिया">
<!ENTITY unknownSocketType.longDesc "<ul> <li>सुनिश्चित करबाक लेल जाँचू कि अहाँक सिस्टम निजी सुरक्षा प्रबंधक संस्थापित रखने अछि.</li> <li>ई सर्वर पर कोनो गैर मानक विन्यास केर कारण भ सकैत अछि.</li> </ul>">

<!ENTITY nssFailure2.title "सुरक्षित कनेक्शन विफल">
<!ENTITY nssFailure2.longDesc2 "<ul> <li>पृष्ठ जकरा अहाँ देखबाक कोशिश कए रहल छी नहि देखाओल जाए सकैत अछि किएक प्राप्त आंकड़ाक प्रमाणिकता जांचल नहि जाए सकल.</li> <li>कृपया वेब साइट मालिक केँ एहि समस्याक संबंध मे सूचित करबाक लेल संपर्क  करू .</li> </ul>">

<!ENTITY nssBadCert.title "सुरक्षित कनेक्शन विफल">
<!ENTITY nssBadCert.longDesc2 "<ul><li>ई सर्वर क विन्यास सँग समस्या भ सकैत अछि, या ई कोनो एहन सर्वर भ सकैत अछि जे सर्वर क रूप केँ बदलए केर कोसिस कए रहल अछि.</li><li>जँ अहाँ पहिले ई सर्वर सँ सफलतापूर्वक सम्पर्क स्थापित क चुकल छी, तँ त्रुटि अस्थायी भ सकैत अछि, आओर अहाँ बाद मे पुनः कोसिस कए सकैत छी.</li></ul>">

<!-- LOCALIZATION NOTE (sharedLongDesc3) This string contains markup including widgets for enabling wifi connections.
     The text inside the tags should be localized. Do not touch the ids. -->
<!ENTITY sharedLongDesc3 "<ul> <li>ई साइट अस्थाई रूप सँ अनुपलब्ध या अत्यंत व्यस्त भ सकैत अछि.किछु क्षण मे पुनः प्रयास करू.</li> <li>जँ अहाँ कोनो पृष्ठ लोड क पाबै मे असमर्थ छी, तँ अपन मोबाइल उपकरण क डेटा या वाई फाई कनेक्शन केर जाँच करू. <button id='wifi'>वाई फाई सक्रिय करू</button> </li> </ul>">

<!ENTITY cspBlocked.title "Blocked by Content Security Policy">
<!ENTITY cspBlocked.longDesc "<p>&brandShortName; prevented this page from loading in this way because the page has a content security policy that disallows it.</p>">

<!ENTITY corruptedContentErrorv2.title "खराब अंतर्वस्तु त्रुटि">
<!ENTITY corruptedContentErrorv2.longDesc "<p>पृष्ठ जकरा अहाँ देखए केर कोसिस क रहल छी नहि देखाएल जाए सकैत अछि किएक आंकड़ा संचारण मे त्रुटि भेटल.</p><ul><li>कृपया वेबसाइट मालिक केँ ई समस्या क संबंध मे सूचित करए लेल संपर्क करू.</li></ul>">

<!ENTITY securityOverride.linkText "अथवा अहाँ एकटा अपवाद जोड़ि सकैत अछि …">
<!ENTITY securityOverride.getMeOutOfHereButton "एतय सँ हमरा बाहर लए जाउ!">
<!ENTITY securityOverride.exceptionButtonLabel "अपवाद जोड़ू…">

<!-- LOCALIZATION NOTE (securityOverride.warningContent) - Do not translate the
contents of the <xul:button> tags.  The only language content is the label= field,
which uses strings already defined above. The button is included here (instead of
netError.xhtml) because it exposes functionality specific to firefox. -->

<!ENTITY securityOverride.warningContent "<p>You should not add an exception if you are using an internet connection that you do not trust completely or if you are not used to seeing a warning for this server.</p>  <button id='getMeOutOfHereButton'>&securityOverride.getMeOutOfHereButton;</button> <button id='exceptionDialogButton'>&securityOverride.exceptionButtonLabel;</button>">

<!ENTITY remoteXUL.title "दूरस्थ XUL">
<!ENTITY remoteXUL.longDesc "<p><ul><li>कृपया वेबसाइट मालिककेँ ई समस्याक संबंधमे सूचित करबाक लेल संपर्क करू.</li></ul></p>">

<!ENTITY sslv3Used.title "सुरक्षित रूप सँ जुड़ए मे असमर्थ">
<!-- LOCALIZATION NOTE (sslv3Used.longDesc) - Do not translate
     "SSL_ERROR_UNSUPPORTED_VERSION". -->
<!ENTITY sslv3Used.longDesc "विस्तृत जानकारी: SSL_ERROR_UNSUPPORTED_VERSION">

<!ENTITY weakCryptoUsed.title "अहाँक कनेक्शन असुरक्षित अछि">
<!-- LOCALIZATION NOTE (weakCryptoUsed.longDesc) - Do not translate
     "SSL_ERROR_NO_CYPHER_OVERLAP". -->
<!ENTITY weakCryptoUsed.longDesc "विस्तृत जानकारी: SSL_ERROR_NO_CYPHER_OVERLAP">

<!ENTITY inadequateSecurityError.title "अहाँक कनेक्शन सुरक्षित नहि अछि">
<!-- LOCALIZATION NOTE (inadequateSecurityError.longDesc) - Do not translate
     "NS_ERROR_NET_INADEQUATE_SECURITY". -->
<!ENTITY inadequateSecurityError.longDesc "<p><span class='hostname'></span>सुरक्षा तकनीक उपयोग करैत अछि जे आब पुरान भ गेल अछि आओर चपेट मे आबि सकैत अछि. एकटा भेदी आसानी सँ ओ सूचना देखि सकैत अछि. जकरा अहाँ सुरक्षित बुझैत छी. वेबसाइट प्रशासक केँ अहाँक साइट देखए सँ पहिले सर्वर ठीक करए पड़त.</p><p>त्रुटि कोड: NS_ERROR_NET_INADEQUATE_SECURITY</p>">

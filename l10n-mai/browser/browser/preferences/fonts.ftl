# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

fonts-window =
    .title = फ़ॉन्ट
fonts-window-close =
    .key = w

## Font groups by language

fonts-langgroup-arabic =
    .label = अरबी
fonts-langgroup-armenian =
    .label = आर्मिनियन
fonts-langgroup-bengali =
    .label = बंगाली
fonts-langgroup-simpl-chinese =
    .label = सरल चीनी
fonts-langgroup-trad-chinese-hk =
    .label = पारंपरिक चीनी (हाँग काँग)
fonts-langgroup-trad-chinese =
    .label = पारंपरिक चीनी (ताईवान)
fonts-langgroup-cyrillic =
    .label = सिरिलिक
fonts-langgroup-devanagari =
    .label = देवनागरी
fonts-langgroup-ethiopic =
    .label = इथियोपिक
fonts-langgroup-georgian =
    .label = ज्यार्जियन
fonts-langgroup-el =
    .label = ग्रीक
fonts-langgroup-gujarati =
    .label = गुजराती
fonts-langgroup-gurmukhi =
    .label = गुरूमुखी
fonts-langgroup-japanese =
    .label = जापानी
fonts-langgroup-hebrew =
    .label = हिब्रू
fonts-langgroup-kannada =
    .label = कन्नड़
fonts-langgroup-khmer =
    .label = ख्मेर
fonts-langgroup-korean =
    .label = कोरियाई
# Translate "Latin" as the name of Latin (Roman) script, not as the name of the Latin language.
fonts-langgroup-latin =
    .label = लैटिन
fonts-langgroup-malayalam =
    .label = मलयालम
fonts-langgroup-math =
    .label = गणित
fonts-langgroup-sinhala =
    .label = सिंहाला
fonts-langgroup-tamil =
    .label = तमिल
fonts-langgroup-telugu =
    .label = तेलुगु
fonts-langgroup-thai =
    .label = थाई
fonts-langgroup-tibetan =
    .label = तिब्बती
fonts-langgroup-canadian =
    .label = यूनीफाइल कैनाडियन सिलबरी
fonts-langgroup-other =
    .label = अन्य लेखन सिस्टम

## Default fonts and their sizes

fonts-default-serif =
    .label = सेरिफ़
fonts-default-sans-serif =
    .label = सैंस सेरिफ़
fonts-minsize-none =
    .label = कोनो नहि

## Text Encodings
##
## Translate the encoding names as adjectives for an encoding, not as the name
## of the language.

fonts-languages-fallback-header = पुरातन कंटेंटक लेल वर्ण एनकोडिंग
fonts-languages-fallback-desc = पुरातन कंटेंटक लेल वर्ण एनकोडिंग जे अपन एन्कोडिंग कए घोषित करबामे विफल रहैत अछि.
fonts-languages-fallback-name-auto =
    .label = वर्तमान लोकेलक लेल पूर्वनिर्धारित
fonts-languages-fallback-name-arabic =
    .label = अरबी
fonts-languages-fallback-name-baltic =
    .label = बाल्टिक
fonts-languages-fallback-name-ceiso =
    .label = केंद्रीय यूरोपियन
fonts-languages-fallback-name-cewindows =
    .label = मध्य यूरोपीय, माइक्रोसॉफ्ट
fonts-languages-fallback-name-simplified =
    .label = चीनी, सरलीकृत
fonts-languages-fallback-name-traditional =
    .label = चीनी, पारम्परिक
fonts-languages-fallback-name-cyrillic =
    .label = सिरिलिक
fonts-languages-fallback-name-greek =
    .label = ग्रीक
fonts-languages-fallback-name-hebrew =
    .label = हिब्रू
fonts-languages-fallback-name-japanese =
    .label = जापानी
fonts-languages-fallback-name-korean =
    .label = कोरियाई
fonts-languages-fallback-name-thai =
    .label = थाई
fonts-languages-fallback-name-turkish =
    .label = तुर्किश
fonts-languages-fallback-name-vietnamese =
    .label = वियतनामी
fonts-languages-fallback-name-other =
    .label = आन (पश्चिमी यूरोपीय सहित)
fonts-very-large-warning-title = पैघ न्यूनतम फाँट आकार
fonts-very-large-warning-message = अहाँ बहुत पैध न्यूनतम फॉन्ट साइज चुनने छी (24 पिक्सेल सँ बेसी). एहन कएला सँ ई काज कठिन अथवा किछु महत्वपूर्ण विन्यास पृष्ठ एकरे जेहन केर उपयोग असंभव भ सकैछ.
fonts-very-large-warning-accept = हमर परिवर्तन बनओने राखू
# Variables:
#   $name {string, "Arial"} - Name of the default font
fonts-label-default =
    .label = पूर्वनिर्धारित ({ $name })

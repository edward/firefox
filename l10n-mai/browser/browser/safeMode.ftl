# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

safe-mode-window =
    .title = { -brand-short-name } सुरक्षित मोड
    .style = max-width: 400px
start-safe-mode =
    .label = सुरक्षित विधि मे आरंभ करू
refresh-profile =
    .label = ताज़ा करू { -brand-short-name }
safe-mode-description = सुरक्षित विधि { -brand-short-name } केर विशेष विधि अछि जे कोनो समस्या केर समाधान कलेल प्रयोग कएल जाए सकैत अछि.
refresh-profile-instead = अहाँ विघ्ननिवारण आओर { -brand-short-name } केँ ताजा करि क छोड़ि सकैत छी.
# Shown on the safe mode dialog after multiple startup crashes. 
auto-safe-mode-description = { -brand-short-name } आरंभ करबाक बेला मे अचानक बन्न भ गेल. इ सहयुक्ति वा कोनो आन समस्या केर कारण भ सकैत अछि. अहाँ ई समस्या क समाधानक कोश‍िश एकरा तयशुदा स्थिति मे सेट कए वा सुरक्षित विधि मे विघ्ननिवारणक द्वारा कए सकैत छी.

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = No probearje
onboarding-button-label-get-started = Begjinne
onboarding-welcome-header = Wolkom by { -brand-short-name }
onboarding-start-browsing-button-label = Begjinne mei sneupen

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = Priveenavigaasje
onboarding-private-browsing-text = Sneup op jo eigen. Priveenavigaasje mei Ynhâldsblokkearring blokkearret online trackers dy't jo op it web folgje.
onboarding-screenshots-title = Screenshots
onboarding-screenshots-text = Meitsje, bewarje en diel skermôfbyldingen - sûnder { -brand-short-name } te ferlitten. Lis in gebied of in hiele side fêst wylst jo sneupe. Bewarje it dêrnei foar maklike tagong en diele.
onboarding-addons-title = Add-ons
onboarding-addons-text = Foegje sels mear funksjes ta dy't { -brand-short-name } hurder foar jo wurkje lit. Fergelykje prizen, besjoch it waarberjocht of druk jo persoanlikheid út mei in oanpast tema.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Sneup flugger, tûker of feiliger mei útwreidingen lykas Ghostery, wêrmei't jo ferfelende advertinsjes blokkearje kinne.
# Note: "Sync" in this case is a generic verb, as in "to synchronize"
onboarding-fxa-title = Syngronisearje
onboarding-fxa-text = Meitsje in { -fxaccount-brand-name } oan en syngronisearje jo blêdwizers, wachtwurden en iepen ljepblêden, oeral wêr't jo { -brand-short-name } brûke.

## Message strings belonging to the Return to AMO flow

return-to-amo-sub-header = Geweldich, jo hawwe { -brand-short-name }
# <icon></icon> will be replaced with the icon belonging to the extension
#
# Variables:
#   $addon-name (String) - Name of the add-on
return-to-amo-addon-header = Litte wy no <icon></icon><b>{ $addon-name }</b> ophelje.
return-to-amo-extension-button = De útwreiding tafoegje
return-to-amo-get-started-button = Begjinne mei { -brand-short-name }

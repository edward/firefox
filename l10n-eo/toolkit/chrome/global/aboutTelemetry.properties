# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE(pageSubtitle):
# - %1$S is replaced by the value of the toolkit.telemetry.server_owner preference
# - %2$S is replaced by brandFullName
pageSubtitle = Tiu ĉi paĝo montras informon pri efikeco, aparataro, uzado kaj personaj agordoj kolektitaj de Telemetry. Tiu ĉi informo estos sendita al %1$S por helpi plibonigi %2$S.

# LOCALIZATION NOTE(homeExplanation):
# - %1$S is either telemetryEnabled or telemetryDisabled
# - %2$S is either extendedTelemetryEnabled or extendedTelemetryDisabled
homeExplanation = Telemezuro estas %1$S kaj etendita telemezuro estas %2$S.
telemetryEnabled = ŝaltita
telemetryDisabled = malŝaltita
extendedTelemetryEnabled = ŝaltita
extendedTelemetryDisabled = malŝaltita

# LOCALIZATION NOTE(settingsExplanation):
# - %1$S is either releaseData or prereleaseData
# - %2$S is either telemetryUploadEnabled or telemetryUploadDisabled
settingsExplanation = Telemezuro kolektas nun %1$S kaj la alŝuto estas %2$S.
releaseData = datumojn de stabila versio
prereleaseData = datumojn de antaŭstabila versio
telemetryUploadEnabled = ŝaltita
telemetryUploadDisabled = malŝaltita

# LOCALIZATION NOTE(pingDetails):
# - %1$S is replaced by a link with pingExplanationLink as text
# - %2$S is replaced by namedPing
pingDetails = Ĉiu informpeco estas sendita en pako ene de “%1$S”. Vi nun vidas la ping %2$S.
# LOCALIZATION NOTE(namedPing):
# - %1$S is replaced by the ping localized timestamp, e.g. “2017/07/08 10:40:46”
# - %2$S is replaced by the ping name, e.g. “saved-session”
namedPing = %1$S, %2$S
# LOCALIZATION NOTE(pingDetailsCurrent):
# - %1$S is replaced by a link with pingExplanationLink as text
# - %2$S is replaced by currentPing
pingDetailsCurrent = Ĉiu informpeco estas sendita en pako ene de “%1$S“. Vi nun vidas la ping %2$S.
pingExplanationLink = ping-oj
currentPing = nuna

# LOCALIZATION NOTE(filterPlaceholder): string used as a placeholder for the
# search field, %1$S is replaced by the section name from the structure of the
# ping. More info about it can be found here:
# https://firefox-source-docs.mozilla.org/toolkit/components/telemetry/telemetry/data/main-ping.html
filterPlaceholder = Serĉi en %1$S
filterAllPlaceholder = Serĉi en ĉiuj sekcioj

# LOCALIZATION NOTE(resultsForSearch): %1$S is replaced by the searched terms
resultsForSearch = Rezultoj por “%1$S”
# LOCALIZATION NOTE(noSearchResults):
# - %1$S is replaced by the section name from the structure of the ping.
# More info about it can be found here: https://firefox-source-docs.mozilla.org/toolkit/components/telemetry/telemetry/data/main-ping.html
# - %2$S is replaced by the current text in the search input
noSearchResults = Bedaŭrinde ne estis rezultoj por “%2$S” en %1$S
# LOCALIZATION NOTE(noSearchResultsAll): %S is replaced by the searched terms
noSearchResultsAll = Bedaŭrinde ne estis rezultoj por “%S” en iu ajn sekcio
# LOCALIZATION NOTE(noDataToDisplay): %S is replaced by the section name.
# This message is displayed when a section is empty.
noDataToDisplay = Bedaŭrinde nuntempe ne estas datumoj disponeblaj en “%S”
# LOCALIZATION NOTE(currentPingSidebar): used as a tooltip for the “current”
# ping title in the sidebar
currentPingSidebar = nuna ping
# LOCALIZATION NOTE(telemetryPingTypeAll): used in the “Ping Type” select
telemetryPingTypeAll = ĉiuj

# LOCALIZATION NOTE(histogram*): these strings are used in the “Histograms” section
histogramSamples = specimenoj
histogramAverage = meznombro
histogramSum = adicio
# LOCALIZATION NOTE(histogramCopy): button label to copy the histogram
histogramCopy = Kopii

# LOCALIZATION NOTE(telemetryLog*): these strings are used in the “Telemetry Log” section
telemetryLogTitle = Registro de telemezuro
telemetryLogHeadingId = Identigilo
telemetryLogHeadingTimestamp = Tempindiko
telemetryLogHeadingData = Datumoj

# LOCALIZATION NOTE(slowSql*): these strings are used in the “Slow SQL Statements” section
slowSqlMain = Malrapidaj instrukcioj de SQL en la ĉefa fadeno
slowSqlOther = Malrapidaj instrukcioj de SQL en helpaj fadenoj
slowSqlHits = Trafoj
slowSqlAverage = Meznombra tempo (ms)
slowSqlStatement = Instrukcio

# LOCALIZATION NOTE(histogram*): these strings are used in the “Add-on Details” section
addonTableID = Identigilo de aldonaĵo
addonTableDetails = Detaloj
# LOCALIZATION NOTE(addonProvider):
# - %1$S is replaced by the name of an Add-on Provider (e.g. “XPI”, “Plugin”)
addonProvider = Provizanto de %1$S

keysHeader = Atributo
namesHeader = Nomo
valuesHeader = Valoro

# LOCALIZATION NOTE(chrome-hangs-title):
# - %1$S is replaced by the number of the hang
# - %2$S is replaced by the duration of the hang
chrome-hangs-title = Raporto pri blokado #%S (%S sekundoj)
# LOCALIZATION NOTE(captured-stacks-title):
# - %1$S is replaced by the string key for this stack
# - %2$S is replaced by the number of times this stack was captured
captured-stacks-title = %1$S (nombro de kaptoj: %2$S)
# LOCALIZATION NOTE(late-writes-title):
# - %1$S is replaced by the number of the late write
late-writes-title = Malfruaj skriboj #%1$S

stackTitle = Stako:
memoryMapTitle = Mapo de memoro:

errorFetchingSymbols = Eraro okazis dum la akirado de simboloj. Kontrolu ĉu vi estas konektita al la Interreto kaj klopodu denove.

parentPayload = Gepatra utilŝarĝo
# LOCALIZATION NOTE(childPayloadN):
# - %1$S is replaced by the number of the child payload (e.g. “1”, “2”)
childPayloadN = Ida utilŝarĝo %1$S
timestampHeader = tempindiko
categoryHeader = kategorio
methodHeader = metodo
objectHeader = objekto
extraHeader = cetero

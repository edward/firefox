<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "Kthehuni mbrapsht">
<!ENTITY safeb.palm.seedetails.label "Shihni hollësitë">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "Ky s&apos;është sajt i rremë…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "r">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "Këshillimi u dha nga <a id='advisory_provider'/>.">


<!ENTITY safeb.blocked.malwarePage.title2 "Vizita në këtë sajt mund të dëmtojë kompjuterin tuaj">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "&brandShortName; e bllokoi këtë faqe, ngaqë mund të rreket të instalojë software dashakeq që mund të vjedhë ose fshijë të dhëna personale në kompjuterin tuaj.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "<span id='malware_sitename'/> është <a id='error_desc_link'>raportuar se përmban software dashakeq</a>. Mund <a id='report_detection'>të raportoni një problem zbulimi</a> ose <a id='ignore_warning_link'>ta shpërfillni rrezikun</a> dhe të shkoni te ky sajt jo i sigurt.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "<span id='malware_sitename'/> është <a id='error_desc_link'>raportuar se përmban software dashakeq</a>. Mund <a id='report_detection'>të raportoni një problem zbulimi</a>.">

<!ENTITY safeb.blocked.malwarePage.learnMore "Mësoni më tepër rreth lënde web të dëmshme, përfshi viruse dhe tjetër malware, dhe se si ta mbroni kompjuterin tuaj, te <a id='learn_more_link'>StopBadware.org</a>. Mësoni më tepër rreth Mbrojtjes nga Karremëzime dhe Malware nën &brandShortName;, te <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.unwantedPage.title2 "Sajti ku po shkoni mund të përmbajë programe të dëmshëm">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "&brandShortName;-i e bllokoi këtë faqe ngaqë, mund të provojë t’ju mashtrojë dhe të instaloni programe që dëmtojnë shfletimin tuaj (për shembull, duke ndryshuar faqen tuaj hyrëse ose duke shfaqur reklama shtesë në sajte që vizitoni).">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "<span id='unwanted_sitename'/> është <a id='error_desc_link'>raportuar se përmban software të dëmshëm</a>. Mund <a id='ignore_warning_link'>ta shpërfillni rrezikun</a> dhe të shkoni te ky sajt i pasigurt.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "<span id='unwanted_sitename'/> është <a id='error_desc_link'>raportuar se përmban software të dëmshëm</a>. ">

<!ENTITY safeb.blocked.unwantedPage.learnMore "Mësoni më tepër rreth software-i të dëmshëm dhe të padëshiruar, te <a id='learn_more_link'>Rregulla Software-i të Padëshiruar</a>.Mësoni më tepër rreth Mbrojtjes Nga Karremëzimet dhe Malware në &brandShortName;, te <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.phishingPage.title3 "Sajt i rremë përpara">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "&brandShortName;-i e bllokoi këtë faqe, ngaqë mund t’jua hedhë dhe të bëni diçka të rrezikshme, të tillë si instalim software-i pse nxjerrje sheshit të dhënash personale, të tilla si fjalëkalime ose karta krediti.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "<span id='phishing_sitename'/> është <a id='error_desc_link'>raportuar si sajt i rrejshëm</a>. Mundeni <a id='report_detection'>të raportoni një problem me këtë vlerësim</a> ose <a id='ignore_warning_link'>ta shpërfillni rrezikun</a> dhe të shkoni te ky sajt jo i parrezik.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "<span id='phishing_sitename'/> është <a id='error_desc_link'>raportuar si sajt i rrejshëm</a>. Mundeni <a id='report_detection'>të raportoni një problem me këtë vlerësim</a>.">

<!ENTITY safeb.blocked.phishingPage.learnMore "Mësoni më tepër rreth sajtesh të rrejshëm dhe karremëzimesh, te <a id='learn_more_link'>www.antiphishing.org</a>. Mësoni më tepër rreth Mbrojtjes Nga Karremëzimet dhe Malware në &brandShortName;, te <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.harmfulPage.title "Sajti ku po shkoni mund të përmbajë malware">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "&brandShortName;-i e bllokoi këtë faqe, ngaqë mund të përpiqet të instalojë aplikacione të rrezikshme që vjedhin ose fshijnë të dhëna tuajat (për shembull, foto, fjalëkalime, mesazhe dhe karta kreditit).">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "<span id='harmful_sitename'/> është <a id='error_desc_link'>raportuar se përmban një aplikacion potencialisht të dëmshëm</a>. Mund ta <a id='ignore_warning_link'>shpërfillni rrezikun</a> dhe të shkoni te ky sajt jo i parrezikshëm.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "<span id='harmful_sitename'/> është <a id='error_desc_link'>raportuar se përmban një aplikacion potencialisht të dëmshëm</a>.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "Mësoni më tepër rreth Mbrojtjes Nga Karremëzimet dhe Malware në &brandShortName;, te <a id='firefox_support'>support.mozilla.org</a>.">

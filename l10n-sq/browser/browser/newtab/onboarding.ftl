# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = Provojeni Që Tani
onboarding-button-label-get-started = Fillojani
onboarding-welcome-header = Mirë se vini te { -brand-short-name }
onboarding-start-browsing-button-label = Filloni të Shfletoni

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = Shfletim Privat
onboarding-private-browsing-text = Shfletësoni vetëm. Shfletimi Privat me Bllokim Lënde bllokon gjurmuesit internetorë që ju ndjekin hap pas hapi në web.
onboarding-screenshots-title = Foto ekrani
onboarding-screenshots-text = Bëni, ruani dhe ndani me të tjerët foto ekrani — pa dalë nga { -brand-short-name }. Të një pjese ose të një faqeje të tërë, teksa shfletoni. Mandej ruajini në web për përdorim dhe ndarje të lehtë me të tjerët.
onboarding-addons-title = Shtesa
onboarding-addons-text = Shtojini madje edhe më tepër veçori që e bëjnë { -brand-short-name } të punojë më fort për ju. Krahasoni çmime, shihni motin ose shprehni personalitetin tuaj me një temë grafike të përshtatur.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Shfletoni më shpejt, me më mençuri, ose me më siguri, përmes zgjerimesh të tillë si Ghostery, që ju lejon të bllokoni reklama të bezdishme.
# Note: "Sync" in this case is a generic verb, as in "to synchronize"
onboarding-fxa-title = Njëkohësoni
onboarding-fxa-text = Regjistrohuni për një  { -fxaccount-brand-name } dhe njëkohësoni fjalëkalimet tuaj, faqerojtësit dhe skedat e hapura nga kudo që përdorni { -brand-short-name }.

## Message strings belonging to the Return to AMO flow

return-to-amo-sub-header = Bukur, e morët { -brand-short-name }-in
# <icon></icon> will be replaced with the icon belonging to the extension
#
# Variables:
#   $addon-name (String) - Name of the add-on
return-to-amo-addon-header = Tani le të marrim për ju <icon></icon><b>{ $addon-name }.</b>
return-to-amo-extension-button = Shtoje Zgjerimin
return-to-amo-get-started-button = Fillojani me { -brand-short-name }

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

choose-messenger-language-description = Zgjidhni gjuhët e përdorura për shfaqje menush, mesazhesh, dhe njoftimesh nga { -brand-short-name }-i.
manage-messenger-languages-button =
    .label = Caktoni Alternativa…
    .accesskey = C
confirm-messenger-language-change-description = Që të hyjnë në fuqi këto ndryshime, rinisni { -brand-short-name }-in
confirm-messenger-language-change-button = Zbatoji dhe Rinisu
update-pref-write-failure-title = Dështim Shkrimi
# Variables:
#   $path (String) - Path to the configuration file
update-pref-write-failure-message = S’arrihet të ruhen parapëlqimet. S"u shkrua dot te kartelë: { $path }

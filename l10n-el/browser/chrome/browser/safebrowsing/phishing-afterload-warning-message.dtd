<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "Επιστροφή">
<!ENTITY safeb.palm.seedetails.label "Δείτε λεπτομέρειες">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "Αυτή δεν είναι μια παραπλανητική ιστοσελίδα…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "δ">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "Παροχή υποδείξεων από <a id='advisory_provider'/>.">


<!ENTITY safeb.blocked.malwarePage.title2 "Η επίσκεψη σε αυτή την ιστοσελίδα μπορεί να βλάψει τον υπολογιστή σας">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "Το &brandShortName; απέκλεισε αυτή τη σελίδα, επειδή ενδέχεται να επιχειρήσει να εγκαταστήσει κακόβουλο λογισμικό που μπορεί να υποκλέψει ή να διαγράψει προσωπικές πληροφορίες στον υπολογιστή σας.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "Το <span id='malware_sitename'/> έχει <a id='error_desc_link'> αναφερθεί ότι περιέχει κακόβουλο λογισμικό</a>. Μπορείτε να <a id='report_detection'>αναφέρετε το πρόβλημα</a> ή να <a id='ignore_warning_link'> αγνοήσετε τον κίνδυνο</a> και να μεταβείτε σε αυτή την επισφαλή ιστοσελίδα.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "Το <span id='malware_sitename'/> έχει <a id='error_desc_link'> αναφερθεί ότι περιέχει κακόβουλο λογισμικό</a>. Μπορείτε να <a id='report_detection'>αναφέρετε το πρόβλημα</a>.">

<!ENTITY safeb.blocked.malwarePage.learnMore "Μάθετε περισσότερα σχετικά με το επιβλαβές διαδικτυακό περιεχόμενο, όπως ιοί και άλλο κακόβουλο λογισμικό, καθώς και το πώς να προστατέψετε τον υπολογιστή σας στο <a id='learn_more_link'>StopBadware.org</a>. Μάθετε περισσότερα σχετικά με την προστασία από ηλεκτρονικό ψάρεμα και κακόβουλο λογισμικό του &brandShortName; στο <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.unwantedPage.title2 "Η ακόλουθη ιστοσελίδα ενδέχεται να περιέχει επιβλαβή προγράμματα">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "Το &brandShortName; απέκλεισε αυτή τη σελίδα, επειδή ενδέχεται να προσπαθήσει να σάς παραπλανήσει ώστε να εγκαταστήσετε προγράμματα που βλάπτουν την εμπειρία περιήγησής σας (για παράδειγμα, αλλάζοντας την αρχική σας σελίδα ή εμφανίζοντας επιπλέον διαφημίσεις στις ιστοσελίδες που επισκέπτεστε).">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "Το <span id='unwanted_sitename'/> έχει <a id='error_desc_link'>αναφερθεί ότι περιέχει επιβλαβές λογισμικό</a>. Μπορείτε να <a id='ignore_warning_link'>αγνοήσετε τον κίνδυνο</a> και να συνεχίσετε σε αυτή την επισφαλή ιστοσελίδα.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "Το <span id='unwanted_sitename'/> έχει <a id='error_desc_link'>αναφερθεί ότι περιέχει επιβλαβές λογισμικό</a>.">

<!ENTITY safeb.blocked.unwantedPage.learnMore "Μάθετε περισσότερα σχετικά με το επιβλαβές και ανεπιθύμητο λογισμικό στην <a id='learn_more_link'>Πολιτική ανεπιθύμητου λογισμικού</a>. Μάθετε περισσότερα σχετικά με την προστασία από ηλεκτρονικό ψάρεμα και κακόβουλο λογισμικό του &brandShortName; στο <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.phishingPage.title3 "Παραπλανητική ιστοσελίδα">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "Το &brandShortName; απέκλεισε αυτή τη σελίδα, επειδή ενδέχεται να σάς παραπλανήσει ώστε να κάνετε κάτι επικίνδυνο, όπως να εγκαταστήσετε λογισμικό ή να αποκαλύψετε προσωπικά δεδομένα, όπως κωδικούς πρόσβασης ή πιστωτικές κάρτες.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "Το <span id='phishing_sitename'/> έχει <a id='error_desc_link'>αναφερθεί ως παραπλανητική ιστοσελίδα</a>. Μπορείτε να <a id='report_detection'>αναφέρετε το πρόβλημα</a> ή να <a id='ignore_warning_link'>αγνοήσετε τον κίνδυνο</a> και να συνεχίσετε σε αυτή την επισφαλή ιστοσελίδα.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "Το <span id='phishing_sitename'/> έχει <a id='error_desc_link'>αναφερθεί ως παραπλανητική ιστοσελίδα</a>. Μπορείτε να <a id='report_detection'>αναφέρετε το πρόβλημα</a>.">

<!ENTITY safeb.blocked.phishingPage.learnMore "Μάθετε περισσότερα σχετικά με τις παραπλανητικές ιστοσελίδες και το ηλεκτρονικό ψάρεμα στο <a id='learn_more_link'>www.antiphishing.org</a>. Μάθετε περισσότερα σχετικά με την προστασία από ηλεκτρονικό ψάρεμα και κακόβουλο λογισμικό του &brandShortName; στο <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.harmfulPage.title "Η ιστοσελίδα αυτή ενδέχεται να περιέχει κακόβουλο λογισμικό">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "Το &brandShortName; απέκλεισε αυτή τη σελίδα, επειδή ενδέχεται να αποπειραθεί να εγκαταστήσει επικίνδυνες εφαρμογές, που υποκλέπτουν ή διαγράφουν τις πληροφορίες σας (για παράδειγμα, φωτογραφίες, κωδικοί πρόσβασης, μηνύματα και πιστωτικές κάρτες).">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "Το <span id='harmful_sitename'/> έχει <a id='error_desc_link'>αναφερθεί ότι περιέχει πιθανώς επιβλαβή εφαρμογή</a>. Μπορείτε να <a id='ignore_warning_link'>αγνοήσετε τον κίνδυνο</a> και να συνεχίσετε σε αυτή την επισφαλή ιστοσελίδα.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "Το <span id='harmful_sitename'/> έχει <a id='error_desc_link'>αναφερθεί ότι περιέχει πιθανώς επιβλαβή εφαρμογή</a>.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "Μάθετε περισσότερα σχετικά με την προστασία από ηλεκτρονικό ψάρεμα και κακόβουλο λογισμικό του &brandShortName; στο <a id='firefox_support'>support.mozilla.org</a>.">

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-learn-more = Meer info
onboarding-button-label-try-now = Nu proberen
onboarding-button-label-get-started = Beginnen
onboarding-welcome-header = Welkom bij { -brand-short-name }
onboarding-welcome-body = U hebt de browser.<br/>Maak kennis met de rest van { -brand-product-name }.
onboarding-welcome-learn-more = Meer info over de voordelen.
onboarding-join-form-header = Doe mee met { -brand-product-name }
onboarding-join-form-body = Voer uw e-mailadres in om te beginnen.
onboarding-join-form-email =
    .placeholder = Voer e-mailadres in
onboarding-join-form-email-error = Geldig e-mailadres vereist
onboarding-join-form-legal = Door verder te gaan, gaat u akkoord met de <a data-l10n-name="terms">Servicevoorwaarden</a> en <a data-l10n-name="privacy">Privacyverklaring</a>.
onboarding-join-form-continue = Doorgaan
onboarding-start-browsing-button-label = Beginnen met browsen

## These are individual benefit messages shown with an image, title and
## description.

onboarding-benefit-products-title = Nuttige producten

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = Privénavigatie
onboarding-private-browsing-text = Surf in uw eentje. Privénavigatie met Inhoudsblokkering blokkeert online trackers die u op het web volgen.
onboarding-screenshots-title = Screenshots
onboarding-screenshots-text = Maak, bewaar en deel schermafbeeldingen - zonder { -brand-short-name } te verlaten. Leg een gebied of een hele pagina vast terwijl u surft. Sla het daarna op voor makkelijke toegang en delen.
onboarding-addons-title = Add-ons
onboarding-addons-text = Voeg nog meer functies toe die { -brand-short-name } harder voor u laten werken. Vergelijk prijzen, bekijk het weerbericht of druk uw persoonlijkheid uit met een aangepast thema.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Surf sneller, slimmer of veiliger met extensies zoals Ghostery, waarmee u vervelende advertenties kunt blokkeren.
# Note: "Sync" in this case is a generic verb, as in "to synchronize"
onboarding-fxa-title = Synchroniseren
onboarding-fxa-text = Maak een { -fxaccount-brand-name } aan en synchroniseer uw bladwijzers, wachtwoorden en open tabbladen, overal waar u { -brand-short-name } gebruikt.

## Message strings belonging to the Return to AMO flow

return-to-amo-sub-header = Geweldig, u hebt { -brand-short-name }
# <icon></icon> will be replaced with the icon belonging to the extension
#
# Variables:
#   $addon-name (String) - Name of the add-on
return-to-amo-addon-header = Laten we nu <icon></icon><b>{ $addon-name }</b> ophalen.
return-to-amo-extension-button = De extensie toevoegen
return-to-amo-get-started-button = Beginnen met { -brand-short-name }

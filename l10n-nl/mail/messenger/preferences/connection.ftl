# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

connection-dns-over-https-url-resolver = Provider gebruiken
    .accesskey = r

# Variables:
#   $name (String) - Display name or URL for the DNS over HTTPS provider
connection-dns-over-https-url-item-default =
    .label = { $name } (standaard)
    .tooltiptext = De standaard URL gebruiken om DNS over HTTPS om te zetten

connection-dns-over-https-url-custom =
    .label = Aangepast
    .accesskey = A
    .tooltiptext = Voer uw gewenste URL in om DNS over HTTPS om te zetten

connection-dns-over-https-custom-label = Aangepast

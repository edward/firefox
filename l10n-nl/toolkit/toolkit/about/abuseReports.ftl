# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

abuse-report-title-extension = Deze extensie rapporteren aan { -vendor-short-name }
abuse-report-title-theme = Dit thema rapporteren aan { -vendor-short-name }
abuse-report-subtitle = Wat is het probleem?
# Variables:
#   $author-name (string) - Name of the add-on author
abuse-report-addon-authored-by = van <a data-l10n-name="author-name">{ $author-name }</a>
abuse-report-learnmore =
    Weet u niet zeker welk probleem u moet selecteren?
    <a data-l10n-name="learnmore-link">Meer info over het rapporteren van extensies en thema’s</a>
abuse-report-submit-description = Beschrijf het probleem (optioneel)
abuse-report-textarea =
    .placeholder = Het is makkelijker voor ons om een probleem te behandelen als we details hebben. Beschrijf het probleem dat u ondervindt. Bedankt voor uw hulp bij het gezond houden van het web.
abuse-report-submit-note =
    Noot: voeg geen persoonlijke gegevens toe (zoals naam, e-mailadres, telefoonnummer, fysiek adres).
    { -vendor-short-name } bewaart deze rapporten permanent.

## Panel buttons.

abuse-report-cancel-button = Annuleren
abuse-report-next-button = Volgende
abuse-report-goback-button = Teruggaan
abuse-report-submit-button = Verzenden

## Message bars descriptions.


## Variables:
##   $addon-name (string) - Name of the add-on

abuse-report-messagebar-aborted = Rapport voor <span data-l10n-name="addon-name">{ $addon-name }</span> geannuleerd.
abuse-report-messagebar-submitting = Rapport verzenden voor <span data-l10n-name="addon-name">{ $addon-name }</span>.
abuse-report-messagebar-submitted = Bedankt voor het indienen van een rapport. Wilt u <span data-l10n-name="addon-name">{ $addon-name }</span> verwijderen?
abuse-report-messagebar-removed-extension = Bedankt voor het indienen van een rapport. U heeft de extensie <span data-l10n-name="addon-name">{ $addon-name }</span> verwijderd.
abuse-report-messagebar-removed-theme = Bedankt voor het indienen van een rapport. U heeft het thema <span data-l10n-name="addon-name">{ $addon-name }</span> verwijderd.
abuse-report-messagebar-error = Er is een fout opgetreden bij het verzenden van een rapport voor <span data-l10n-name="addon-name">{ $addon-name }</span>.
abuse-report-messagebar-error-recent-submit = Het rapport voor <span data-l10n-name="addon-name">{ $addon-name }</span> is niet verzonden, omdat u recent een ander rapport heeft verzonden.

## Message bars actions.

abuse-report-messagebar-action-remove = Ja, verwijderen
abuse-report-messagebar-action-keep = Nee, behouden
abuse-report-messagebar-action-retry = Opnieuw proberen
abuse-report-messagebar-action-cancel = Annuleren

## Abuse report reasons (optionally paired with related examples and/or suggestions)

abuse-report-damage-reason = Beschadigt mijn computer en gegevens

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = Liiguta üles
    .accesskey = L
languages-customize-movedown =
    .label = Liiguta alla
    .accesskey = a
languages-customize-remove =
    .label = Eemalda
    .accesskey = E
languages-customize-select-language =
    .placeholder = Vali lisatav keel…
languages-customize-add =
    .label = Lisa
    .accesskey = i
messenger-languages-window =
    .title = { -brand-short-name }i keelesätted
    .style = width: 40em
messenger-languages-description = { -brand-short-name } kuvab esimest keelt vaikeväärtusena ja teisi keeli vastavalt vajadusele nende esinemise järjekorras.

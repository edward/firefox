<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "Mine tagasi">
<!ENTITY safeb.palm.seedetails.label "Vaata üksikasju">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "See ei ole veebivõltsing…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "b">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "Nõuande andja: <a id='advisory_provider'/>.">


<!ENTITY safeb.blocked.malwarePage.title2 "Selle saidi külastamine võib kahjustada sinu arvutit">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "&brandShortName; blokkis selle lehe, kuna see võib üritada paigaldada ohtlikke rakendusi, mis varastavad või kustutavad sinu isiklikke andmeid.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "<span id='malware_sitename'/> on raporteeritud <a id='error_desc_link'>pahavara sisaldava saidina</a>. Sa võid <a id='report_detection'>raporteerida tuvastamise probleemist</a> või <a id='ignore_warning_link'>riski ignoreerida</a> ja jätkata selle ebaturvalise saidi külastamist.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "<span id='malware_sitename'/> on raporteeritud <a id='error_desc_link'>pahavara sisaldava saidina</a>. Sa võid <a id='report_detection'>raporteerida tuvastamise probleemist</a>.">

<!ENTITY safeb.blocked.malwarePage.learnMore "Rohkem teavet kahjuliku veebisisu ja muu pahavara, ning selle kohta, kuidas oma arvutit kaitsta, leiab aadressilt <a id='learn_more_link'>StopBadware.org</a>. Rohkem teavet &brandShortName;i veebivõltsingute ja pahavara vastase kaitse kohta leiab aadressilt <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.unwantedPage.title2 "Külastatav sait võib sisaldada kahjulikke rakendusi">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "&brandShortName; blokkis selle lehe, sest see võib üritada sind üle kavaldada, et paigaldaksid tarkvara, mis kahjustab sinu veebilehitsemise kogemust (nt vahetab välja brauseri avalehe või kuvab reklaame lehtedel, mida külastad).">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "<span id='unwanted_sitename'/> on raporteeritud <a id='error_desc_link'>kahjulikku tarkvara sisaldava saidina</a>. Sa võid <a id='ignore_warning_link'>riski ignoreerida</a> ja jätkata selle ebaturvalise saidi külastamist.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "<span id='unwanted_sitename'/> on raporteeritud <a id='error_desc_link'>kahjulikku tarkvara sisaldava saidina</a>.">

<!ENTITY safeb.blocked.unwantedPage.learnMore "Rohkem teavet kahjuliku ja soovimatu tarkvara kohta leiab lehelt <a id='learn_more_link'>soovimatu tarkvara reeglid</a>. Rohkem teavet &brandShortName;i veebivõltsingute ja pahavara vastase kaitse kohta leiab aadressilt <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.phishingPage.title3 "Veebivõltsingu hoiatus">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "&brandShortName; blokkis selle lehe, sest see võib üritada sind üle kavaldada, et teeksid midagi ohtlikku, nagu tarkvara paigaldamine või isiklike andmete (paroolid või krediitkaardi andmed) avalikustamine.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "<span id='phishing_sitename'/> on raporteeritud <a id='error_desc_link'>veebivõltsingut sisaldava saidina</a>. Sa võid <a id='report_detection'>raporteerida tuvastamise probleemist</a> või <a id='ignore_warning_link'>riski ignoreerida</a> ja jätkata selle ebaturvalise saidi külastamist.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "<span id='phishing_sitename'/> on raporteeritud <a id='error_desc_link'>veebivõltsingut sisaldava saidina</a>. Sa võid <a id='report_detection'>raporteerida tuvastamise probleemist</a>.">

<!ENTITY safeb.blocked.phishingPage.learnMore "Rohkem teavet veebivõltsingute ja õngitsemise kohta leiab aadressilt <a id='learn_more_link'>www.antiphishing.org</a>. Rohkem teavet &brandShortName;i veebivõltsingute ja pahavara vastase kaitse kohta leiab aadressilt <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.harmfulPage.title "Külastatav sait võib sisaldada pahavara">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "&brandShortName; blokkis selle lehe, kuna see võib üritada paigaldada ohtlikke rakendusi, mis varastavad või kustutavad sinu andmeid (nt fotosid, paroole, sõnumeid või krediitkaartide infot).">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "<span id='harmful_sitename'/> on raporteeritud <a id='error_desc_link'>potentsiaalselt kahjulikku rakendust sisaldava saidina</a>. Sa võid <a id='ignore_warning_link'>riski ignoreerida</a> ja jätkata selle ebaturvalise saidi külastamist.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "<span id='harmful_sitename'/> on raporteeritud <a id='error_desc_link'>potentsiaalselt kahjulikku rakendust sisaldava saidina</a>.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "Rohkem teavet &brandShortName;i veebivõltsingute ja pahavara vastase kaitse kohta leiab aadressilt <a id='firefox_support'>support.mozilla.org</a>.">

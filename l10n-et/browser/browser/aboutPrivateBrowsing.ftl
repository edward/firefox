# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-private-browsing-learn-more = Rohkem teavet <a data-l10n-name="learn-more">privaatse veebilehitsemise kohta</a>.
about-private-browsing-info-visited = külastatud lehti
privatebrowsingpage-open-private-window-label = Ava privaatne aken
    .accesskey = p
about-private-browsing-info-notsaved = Kui sa lehitsed veebi privaatses aknas, siis { -brand-short-name } <strong>ei salvesta</strong>:
about-private-browsing-info-bookmarks = järjehoidjad
about-private-browsing-info-searches = otsinguid
about-private-browsing-info-downloads = allalaadimised
private-browsing-title = Privaatne veebilehitsemine
about-private-browsing-info-saved = { -brand-short-name } <strong>salvestab</strong>:
about-private-browsing-info-clipboard = kopeeritud tekstid
about-private-browsing-info-temporary-files = ajutisi faile
about-private-browsing-info-cookies = küpsiseid
tracking-protection-start-tour = Vaata, kuidas see töötab
about-private-browsing-note = Privaatne veebilehitsemine ei muuda sind internetis <strong>anonüümseks</strong>. Sinu interneti teenusepakkuja või tööandja võib siiski sinu poolt külastatavaid lehti jälgida.
about-private-browsing-not-private = Sa pole praegu privaatses aknas.
content-blocking-title = Sisu blokkimine
content-blocking-description = Mõned saidid kasutavad jälitamistehnoloogiaid, mis võivad jälitada sinu tegevust üle terve interneti. { -brand-short-name }i sisu blokkija blokib automaatselt paljud sellised jälitajad, kes võivad koguda andmeid sinu lehitsemiseelistuste kohta.

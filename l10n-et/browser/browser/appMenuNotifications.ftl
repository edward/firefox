# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

appmenu-update-available =
    .label = { -brand-shorter-name }ile on saadaval uuendus.
    .buttonlabel = Laadi uuendus alla
    .buttonaccesskey = L
    .secondarybuttonlabel = Mitte praegu
    .secondarybuttonaccesskey = M
appmenu-update-available-message = Uuenda oma { -brand-shorter-name }, et saada osa paremast kiirusest ja privaatsusest.
appmenu-update-manual =
    .label = { -brand-shorter-name }il pole võimalik uuendada uusima versiooni peale.
    .buttonlabel = Laadi alla { -brand-shorter-name }
    .buttonaccesskey = d
    .secondarybuttonlabel = Mitte praegu
    .secondarybuttonaccesskey = M
appmenu-update-manual-message = Laadi alla { -brand-shorter-name }i värske versioon ja me aitame sul selle paigaldada.
appmenu-update-whats-new =
    .value = Vaata uuendusi.
appmenu-update-restart =
    .label = Uuendamiseks taaskäivita { -brand-shorter-name }.
    .buttonlabel = Taaskäivita ja taasta
    .buttonaccesskey = T
    .secondarybuttonlabel = Mitte praegu
    .secondarybuttonaccesskey = M
appmenu-update-restart-message = Pärast kiiret taaskäivitamist taastab { -brand-shorter-name } kõik avatud kaardid ja aknad, mis pole privaatse veebilehitsemise režiimis.
appmenu-addon-post-install-message = Halda oma laiendusi, klõpsates nupul <image data-l10n-name='addon-install-icon'></image> menüüs <image data-l10n-name='addon-menu-icon'></image>.

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = Proovi kohe
onboarding-button-label-get-started = Tee algust
onboarding-welcome-header = Tere tulemast { -brand-short-name }i
onboarding-start-browsing-button-label = Alusta veebilehitsemist

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = Privaatne veebilehitsemine
onboarding-private-browsing-text = Lehitse veebi privaatselt. Sisu blokkimisega privaatse veebilehitsemise režiimis blokitakse jälitajad, kes järgnevad sulle igal pool veebis.
onboarding-screenshots-title = Ekraanipildid
onboarding-screenshots-text = Tee, salvesta ja jaga ekraanipilte - ilma { -brand-short-name }ist lahkumata. Pildista ühte piirkonda või tervet veebilehte. Siis salvesta see veebis kiireks ligipääsemiseks ja jagamiseks.
onboarding-addons-title = Lisad
onboarding-addons-text = Lisa { -brand-short-name }ile rohkem funktsionaalsust, et ta töötaks sinu jaoks efektiivsemalt. Võrdle hindu, kontrolli ilma või väljenda oma isiksust kohandatud teemaga.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Lehitse veebi kiiremini, targemini või ohutumalt laiendustega nagu Ghostery, mis lubavad sul blokkida tüütuid reklaame.
# Note: "Sync" in this case is a generic verb, as in "to synchronize"
onboarding-fxa-title = Sync
onboarding-fxa-text = Loo { -fxaccount-brand-name } konto ja sünkroniseeri oma järjehoidjad, paroolid ning avatud kaardid kõikjale, kus kasutad { -brand-short-name }i.

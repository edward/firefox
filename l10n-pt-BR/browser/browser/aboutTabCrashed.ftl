# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

crashed-title = Relator de travamento em abas
crashed-close-tab-button = Fechar aba
crashed-restore-tab-button = Restaurar esta aba
crashed-restore-all-button = Restaurar todas as abas travadas
crashed-header = Aff! Sua aba travou.
crashed-offer-help = Podemos ajudar!
crashed-single-offer-help-message = Escolha { crashed-restore-tab-button } para recarregar a página.
crashed-multiple-offer-help-message = Escolha { crashed-restore-tab-button } ou { crashed-restore-all-button } para recarregar a(s) página(s).
crashed-request-help = Você nos ajudará?
crashed-request-help-message = Os relatórios de travamento nos ajudam a diagnosticar problemas e a fazer o { -brand-short-name } melhor.
crashed-request-report-title = Relatar esta aba
crashed-send-report = Enviar um relatório automatizado de travamento para que possamos corrigir problemas como este.
crashed-comment =
    .placeholder = Comentários opcionais (comentários são visíveis publicamente)
crashed-include-URL = Incluir as URLs dos sites em que você estava quando { -brand-short-name } travou.
crashed-email-placeholder = Digite aqui seu endereço de e-mail
crashed-email-me = Enviar um e-mail quando mais informações estiverem disponíveis
crashed-report-sent = Relatório do travamento enviado; obrigado por ajudar a fazer o { -brand-short-name } melhor!
crashed-request-auto-submit-title = Informar abas em segundo plano
crashed-auto-submit-checkbox = Atualizar as preferências para enviar relatórios automaticamente quando o { -brand-short-name } travar.

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-private-browsing-info-visited = visited pages
privatebrowsingpage-open-private-window-label = Open a Private Window
    .accesskey = P
about-private-browsing-info-notsaved = When you browse in a Private Window, { -brand-short-name } <strong>does not save</strong>:
about-private-browsing-info-bookmarks = bookmarks
about-private-browsing-info-searches = searches
about-private-browsing-info-downloads = downloads
private-browsing-title = Private Browsing
about-private-browsing-info-saved = { -brand-short-name } <strong>will save</strong> your:
about-private-browsing-info-temporary-files = temporary files
about-private-browsing-info-cookies = cookies
tracking-protection-start-tour = See how it works
about-private-browsing-note = Private Browsing <strong>doesn’t make you anonymous</strong> on the Internet. Your employer or Internet service provider can still know what page you visit.
about-private-browsing-not-private = You are currently not in a private window.

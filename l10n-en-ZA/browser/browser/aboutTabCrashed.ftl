# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

crashed-restore-tab-button = Restore This Tab
crashed-restore-all-button = Restore All Crashed Tabs
crashed-email-placeholder = Enter your email address here
crashed-email-me = Email me when more information is available
crashed-report-sent = Crash report already submitted; thank you for helping make { -brand-short-name } better!

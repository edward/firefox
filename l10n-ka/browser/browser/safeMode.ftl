# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

safe-mode-window =
    .title = { -brand-short-name } უსაფრთხო რეჟიმი
    .style = max-width: 400px
start-safe-mode =
    .label = უსაფრთხო რეჟიმით გაშვება
refresh-profile =
    .label = { -brand-short-name } – შეკეთება
safe-mode-description = უსაფრთხო რეჟიმი გამოიყენება { -brand-short-name }-ის გაუმართაობის აღმოსაჩენად.
safe-mode-description-details = თქვენი დამატებები და მითითებული პარამეტრები, დროებით შეიზღუდება, შესაბამისად { -brand-short-name }-ის ცალკეულმა შესაძლებლობებმა, შესაძლოა სათანადოდ ვერ იმუშაოს.
refresh-profile-instead = შეგიძლიათ გამოტოვოთ ხარვეზების აღმოჩენა და პირდაპირ შეაკეთოთ { -brand-short-name }.
# Shown on the safe mode dialog after multiple startup crashes. 
auto-safe-mode-description = { -brand-short-name } გაშვებისას მოულოდნელად დაიხურა. ეს შეიძლება გამოწვეული იყოს დამატებების მიერ ან სხვა გაუმართაობით. ხარვეზების აღმოფხვრა შეგიძლიათ უსაფრთხო რეჟიმის დახმარებით.

<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY itemGeneral.label       "ზოგადი">
<!ENTITY dataChoicesTab.label    "მონაცემთა შერჩევა">
<!ENTITY itemUpdate.label        "განახლება">
<!ENTITY itemNetworking.label    "ქსელი და ადგილი დისკზე">
<!ENTITY itemCertificates.label  "სერტიფიკატები">

<!-- General Settings -->
<!ENTITY enableGlodaSearch.label       "გლობალური ძიებისა და გზავნილთა ინდექსაციის ჩართვა">
<!ENTITY enableGlodaSearch.accesskey   "E">
<!ENTITY dateTimeFormatting.label      "რიცხვისა და დროის გაფორმება">
<!ENTITY languageSelector.label        "ენა">
<!ENTITY allowHWAccel.label            "ხელმისაწვდომობის შემთხვევაში აპარატული აჩქარების გამოყენება">
<!ENTITY allowHWAccel.accesskey        "პ">
<!ENTITY storeType.label               "შეტყობინების შენახვის სახე ახალი ანაგირშებისთვის:">
<!ENTITY storeType.accesskey           "ხ">
<!ENTITY mboxStore2.label              "თითოეული საქაღალდე ფაილში (mbox)">
<!ENTITY maildirStore.label            "თითოეული წერილი ფაილში (maildir)">

<!ENTITY scrolling.label               "გადახვევა">
<!ENTITY useAutoScroll.label           "თვითგადაადგილების გამოყენება">
<!ENTITY useAutoScroll.accesskey       "თ">
<!ENTITY useSmoothScrolling.label      "გლუვი გადაადგილების გამოყენება">
<!ENTITY useSmoothScrolling.accesskey  "გ">

<!ENTITY systemIntegration.label       "სისტემური ინტეგრაცია">
<!ENTITY alwaysCheckDefault.label      "გახსნისას &brandShortName; პროგრამის ყოველთვის შემოწმება ნაგულისხმებ საფოსტო პროგრამად">
<!ENTITY alwaysCheckDefault.accesskey  "A">
<!ENTITY searchIntegration.label       "&searchIntegration.engineName; სისტემისთვის წერილებში ძიების ნებართვა">
<!ENTITY searchIntegration.accesskey   "S">
<!ENTITY checkDefaultsNow.label        "შემოწმება ახლავე…">
<!ENTITY checkDefaultsNow.accesskey    "N">
<!ENTITY configEditDesc.label          "დეტალური გამართვა">
<!ENTITY configEdit.label              "გამართვის რედაქტორი…">
<!ENTITY configEdit.accesskey          "C">
<!ENTITY returnReceiptsInfo.label      "როგორ მოეპყრას &brandShortName; მიღების დასტურებს">
<!ENTITY showReturnReceipts.label      "მიღების დასტურები…">
<!ENTITY showReturnReceipts.accesskey  "რ">

<!-- Data Choices -->
<!ENTITY telemetrySection.label          "მონაცემთა აღრიცხვა">
<!ENTITY telemetryDesc.label             "თქვენი ელფოსტის პროგრამის წარმადობის, გამოყენების, აპრატურისა და მორგების მონაცემებს უზიარებს &vendorShortName;-ს , რაც &brandShortName;-ის გაუმჯობესებაში გვეხმარება">
<!ENTITY enableTelemetry.label           "აღრიცხვის ჩართვა">
<!ENTITY enableTelemetry.accesskey       "ღ">
<!ENTITY telemetryLearnMore.label        "ვრცლად">

<!ENTITY crashReporterSection.label      "უეცარი გათიშვების მომხსენებელი">
<!ENTITY crashReporterDesc.label         "&brandShortName; აგზავნის უეცარი გათიშვების მოხსენებებს, რაც &vendorShortName;-ს თქვენი ელფოსტის პროგრამის მდგრადობისა და უსაფრთხოების გაუმჯობესებაში ეხმარება">
<!ENTITY enableCrashReporter.label       "უეცარი გათიშვების მომხსენებლის ჩართვა">
<!ENTITY enableCrashReporter.accesskey   "რ">
<!ENTITY crashReporterLearnMore.label    "ვრცლად">

<!-- Update -->
<!-- LOCALIZATION NOTE (updateApp.label):
  Strings from aboutDialog.dtd are displayed in this section of the preferences.
  Please check for possible accesskey conflicts.
-->
<!ENTITY updateApp2.label                "&brandShortName; – განახლებები">
<!-- LOCALIZATION NOTE (updateApp.version.*): updateApp.version.pre is
  followed by a version number, keep the trailing space or replace it with
  a different character as needed. updateApp.version.post is displayed after
  the version number, and is empty on purpose for English. You can use it
  if required by your language.
 -->
<!ENTITY updateApp.version.pre           "ვერსია ">
<!ENTITY updateApp.version.post          "">
<!ENTITY updateAppAllow.description      "ნებართვა, რომ &brandShortName; შეძლებს,">
<!ENTITY updateAuto.label                "თავად დააყენებს განახლებებს (სასურველია უსაფრთხოების გასაუმჯობესებლად)">
<!ENTITY updateAuto.accesskey            "თ">
<!ENTITY updateCheck.label               "შემოწმდეს განახლებებზე, მაგრამ თავად გადაწყვეტთ, დააყენებთ თუ არა">
<!ENTITY updateCheck.accesskey           "შ">
<!ENTITY updateHistory.label             "განახლების ისტორიის ჩვენება">
<!ENTITY updateHistory.accesskey         "ნ">

<!ENTITY useService.label                "ფონური მოსახურებით სარგებლობა განახლებათა ჩადგმისას">
<!ENTITY useService.accesskey            "b">

<!ENTITY updateCrossUserSettingWarning.description "ეს პარამეტრები აისახება Windows-ის ყველა ანგარიშზე და &brandShortName;-ის ყველა პროფილზე, რომელიც &brandShortName;-ის ამ დაყენებულ ვერსიას ექვემდებარება.">

<!-- Networking and Disk Space -->
<!ENTITY showSettings.label            "პარამეტრები…">
<!ENTITY showSettings.accesskey        "S">
<!ENTITY proxiesConfigure.label        "როგორ დაუკავშირდეს &brandShortName; პროგრამა ინტერნეტს.">
<!ENTITY connectionsInfo.caption       "კავშირი">
<!ENTITY offlineInfo.caption           "კავშირგარეშე">
<!ENTITY offlineInfo.label             "კავშირგარეშე რეჟიმის პარამეტრები">
<!ENTITY showOffline.label             "კავშირგარეშე…">
<!ENTITY showOffline.accesskey         "O">

<!ENTITY Diskspace                       "ადგილი დისკზე">
<!ENTITY offlineCompactFolders.label     "ყველა საქაღალდის შეკუმშვა, თუ მეტ სივრცეს გამოათავისუფლებს">
<!ENTITY offlineCompactFolders.accesskey "a">
<!ENTITY offlineCompactFoldersMB.label   "მბ მაინც">

<!-- LOCALIZATION NOTE:
  The entities useCacheBefore.label and useCacheAfter.label appear on a single
  line in preferences as follows:

  &useCacheBefore.label  [ textbox for cache size in MB ]   &useCacheAfter.label;
-->
<!ENTITY useCacheBefore.label            "გამოყენება, მანამ">
<!ENTITY useCacheBefore.accesskey        "U">
<!ENTITY useCacheAfter.label             "მბ ბუფერისთვის">
<!ENTITY overrideSmartCacheSize.label    "კეშის ზომის თვითგანსაზღვრის უგულებელყოფა">
<!ENTITY overrideSmartCacheSize.accesskey "ვ">
<!ENTITY clearCacheNow.label             "გასუფთავება">
<!ENTITY clearCacheNow.accesskey         "C">

<!-- Certificates -->
<!ENTITY certSelection.description       "როცა სერვერი ჩემს პირად სერტიფიკატს ითხოვს:">
<!ENTITY certs.auto                      "ავტომატური შერჩევა">
<!ENTITY certs.auto.accesskey            "m">
<!ENTITY certs.ask                       "შეკითხვა ყოველ ჯერზე">
<!ENTITY certs.ask.accesskey             "A">
<!ENTITY enableOCSP.label                "OCSP სერვერებისთვის სერტიფიკატების მიმდინარე მდგომარეობის კითხვა">
<!ENTITY enableOCSP.accesskey            "ს">

<!ENTITY manageCertificates.label "სერტიფიკატების მართვა">
<!ENTITY manageCertificates.accesskey "მ">
<!ENTITY viewSecurityDevices.label "უსაფრთხოების მოწყობილობები">
<!ENTITY viewSecurityDevices.accesskey "S">

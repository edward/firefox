# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = Flytt opp
    .accesskey = o
languages-customize-movedown =
    .label = Flytt ned
    .accesskey = d
languages-customize-remove =
    .label = Fjern
    .accesskey = r
languages-customize-select-language =
    .placeholder = Vel eit språk for å leggja til…
languages-customize-add =
    .label = Legg til
    .accesskey = L
messenger-languages-window =
    .title = { -brand-short-name } språkinnstillingar
    .style = width: 40em
messenger-languages-description = { -brand-short-name } vil visa det første språket som standard og vil visa alternative språk om nødvendig i den rekkjefølgja dei i vert viste i.

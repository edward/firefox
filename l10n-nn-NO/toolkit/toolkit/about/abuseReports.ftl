# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

abuse-report-title-extension = Rapporter denne utvidinga til { -vendor-short-name }
abuse-report-subtitle = Kva er problemet?
# Variables:
#   $author-name (string) - Name of the add-on author
abuse-report-addon-authored-by = av <a data-l10n-name="author-name">{ $author-name }</a>
abuse-report-submit-description = Beskriv problemet (valfritt)

## Panel buttons.

abuse-report-cancel-button = Avbryt
abuse-report-next-button = Neste
abuse-report-goback-button = Gå tilbake
abuse-report-submit-button = Send inn

## Message bars descriptions.


## Variables:
##   $addon-name (string) - Name of the add-on


## Message bars actions.

abuse-report-messagebar-action-retry = Prøv ein gong til
abuse-report-messagebar-action-cancel = Avbryt

## Abuse report reasons (optionally paired with related examples and/or suggestions)

abuse-report-other-reason = Noko anna
